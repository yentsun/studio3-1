<?php

class Application_Plugin_AccessControl extends Zend_Controller_Plugin_Abstract {

	public function preDispatch(Zend_Controller_Request_Abstract $request) {

		$acl = new Zend_Acl();

        //roles
        $acl->addRole(new Zend_Acl_Role('guest'));
        $acl->addRole(new Zend_Acl_Role('client'), 'guest');
        $acl->addRole(new Zend_Acl_Role('administrator'), 'client');

        //resources
        $acl->addResource('index');
        $acl->addResource('page');
        $acl->addResource('admin');
        $acl->addResource('team');
        $acl->addResource('country');
        $acl->addResource('player');
        $acl->addResource('user');
        $acl->addResource('game');
        $acl->addResource('league');

        //rules
        //not registered
        $acl->deny('guest', null);
        $acl->allow('guest', array('page', 'index'));
        $acl->allow('guest', 'user', array('register', 'logged', 'login',
                                           'recover', 'register-success'));


        //registered
        $acl->deny('client', null);
        $acl->allow('client', array('team', 'country', 'player', 'game',
                                    'league'));
        $acl->allow('client', 'user', array('dashboard', 'logout',
                                            'add-favourite',
                                            'favourites', 'delete-favourite'));
        $acl->deny('client', 'user', 'register');


        //admin can do everything
        $acl->allow('administrator', 'admin');

		if ($this->_response->isException()) {
			$this->getResponse()->setHttpResponseCode(404);
			return;
		}

        $controller = $request->controller;
        $action = $request->action;
        $layout = Zend_Layout::getMvcInstance();
        $view = $layout->getView();
        if (!($controller == 'game' && $action == 'stream')) {
            $user = Application_Model_User::get_auth_user();
            $role = $user ? $user->get_status() : 'guest';
            if ($acl->has($controller)) {
                if (!$acl->isAllowed($role, $controller, $action)) {
                    $flashMessenger = Zend_Controller_Action_HelperBroker
                    ::getStaticHelper('FlashMessenger');
                    $flashMessenger->addMessage('<div class="alert alert-error">
                    У вас недостаточно прав. Пожалуйста, пройдите авторизацию.
                    </div>');
                    $request->setControllerName('user')
                        ->setActionName('login')
                        ->setDispatched(true);
                }
            }
            $view->user = $user;
        }
        Zend_Registry::set('acl', $acl);
        $view->acl = $acl;
	}
}
