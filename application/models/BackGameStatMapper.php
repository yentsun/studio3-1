<?php

class Application_Model_BackGameStatMapper
    extends Application_Model_PlayerGameStatMapper {

    protected $_table_name = 'stat_player_2';

    protected $_map = array(
        'id'=> 'id',
        'player_id'=> 'playerid',
        'game_id'=> 'parentid',
        'games_played'=> 'value_66__i',
        'goals_authored'=> 'value_65__i',
        'assists'=> 'value_64__i',
        'points'=> 'value_63__i',
        'plus_minus'=> 'value_62__i',
        'penalty_time'=> 'value_61__i',
        'score_equal'=> 'value_60__i',
        'score_grater'=> 'value_59__i',
        'score_less'=> 'value_58__i',
        'score_overtime'=> 'value_57__i',
        'score_win'=> 'value_56__i',
        'score_win_bullets'=> 'value_55__i',
        'attempts'=> 'value_54__i',
        'performance'=> 'value_53__f',
        'face_offs'=> 'value_52__i',
        'face_offs_won'=> 'value_51__i',
        'face_off_performance'=> 'value_50__f',
        'field_time'=> 'value_49__t',
        'shifts'=> 'value_48__i'
    );
}