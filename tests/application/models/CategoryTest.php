<?php

class CategoryTest extends PHPUnit_Framework_TestCase {

    private $_mapper = null;
    private $_adapter = null;
    const HOCKEY_ID = 1;
    const COUNTRY_ID = 252;
    const LEAGUE_ID = 287;
    const LEAGUE2_ID = 288;

    function setUp() {

        $this->_mapper = new Application_Model_CategoryMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();
    }

    function tearDown() {

        $this->_adapter->rollBack();
    }

    public function test_create_category() {

        $category_data = array(
            'title'=> 'Лапландия',
            'image_file'=> 'lap.jpg',
            'parent_id'=> 1,
            'order_id'=> 1,
            'status_id'=> 0
        );
        $new_category_id = Application_Model_Category::create($category_data);
        $this->assertInternalType('int', $new_category_id);
        $new_category = Application_Model_Category::fetch($new_category_id);
        $this->assertEquals('Лапландия', $new_category->title);
        $this->assertEquals('lap.jpg', $new_category->image_file);
        $this->assertEquals('country', $new_category->get_type());
        $this->assertEquals('Хоккей', $new_category->get_parent_by_type('sport')->title);
    }

    public function test_update_category() {

        $data = array(
            'image_file'=> 'lap1.jpg',
            'rubbish_prop'=> '345gf' //should be ignored
        );
        $updated_category = Application_Model_Category::update($data,
                                                             self::COUNTRY_ID);
        $this->assertInstanceOf('Application_Model_Category', $updated_category);
        $this->assertEquals('lap1.jpg', $updated_category->image_file);
    }

    public function test_delete_category() {

        Application_Model_Category::delete(self::COUNTRY_ID);
        $deleted_category = Application_Model_Category::fetch(self::COUNTRY_ID);
        $this->assertNull($deleted_category);
    }

    public function test_fetch_by_sport() {

        $hockey_countries = Application_Model_Category::fetch_all(self::HOCKEY_ID);
        $this->assertEquals(self::HOCKEY_ID, count($hockey_countries));
    }

    public function test_fetch_children() {

        $category = Application_Model_Category::fetch(self::COUNTRY_ID);
        $child_categories = $category->fetch_children();
        $this->assertEquals(2, count($child_categories));
        foreach ($child_categories as $league)
            $this->assertInstanceOf('Application_Model_Category', $league);
    }

    public function test_fetch_parent() {

        $category = Application_Model_Category::fetch(self::LEAGUE_ID);
        $country = $category->parent();
        $this->assertEquals('Лапландия', $country->title);
    }

    public function test_get_parents() {

        $category = Application_Model_Category::fetch(self::LEAGUE_ID);
        $this->assertEquals(2, count($category->parents()));
        $this->assertEquals(1, count($category->parents(false, 1)));
        $this->assertEquals('Хоккей Лапландия', $category->parents(true));
        $this->assertEquals('Лапландия', $category->parents(true, 1));
    }

    public function test_tree() {

        $tree = Application_Model_Category::get_tree(1);
        $this->assertEquals(1, count($tree));
        foreach ($tree as $node) {
            $this->assertInstanceOf('Application_Model_Category', $node['cat']);
            $this->assertInternalType('array', $node['children']);
        }
        $this->assertEquals(2, count($tree[self::COUNTRY_ID]['children']));

        //test deletion of a category and updated tree
        Application_Model_Category::delete(self::LEAGUE_ID);
        $tree = Application_Model_Category::get_tree(1);
        $this->assertEquals(1, count($tree[self::COUNTRY_ID]['children']));
    }

    public function test_validators() {

        $dummy = new Application_Model_Category();
        $validators = array(
            'order_id'=> $dummy->get_validators('order_id'),
        );
        $data = array();

        $input = new Zend_Filter_Input(null, $validators, $data);
        $this->assertTrue($input->isValid());
        $this->assertEquals(0, $input->order_id);
    }
}