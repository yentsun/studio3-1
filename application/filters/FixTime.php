<?php

class Application_Filter_FixTime implements Zend_Filter_Interface {

    /**
     * Get 'incorrect' time value like '65:13' and return correct '01:05:13'
     * '0' is transformed into '00:00:00'
     * @param mixed $time_string
     * @return mixed|string
     */
    public function filter($time_string) {

        $pieces = explode(':', $time_string);
        if (isset($pieces[0]) && isset($pieces[1])) {
            $time = mktime((int) 0, $pieces[0], $pieces[1]);
            return date('H:i:s', $time);
        }
        if ($time_string == '0')
            return '00:00:00';
        return $time_string;
    }
}