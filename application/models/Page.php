<?php

class Application_Model_Page extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_PageMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>true),
        'slug'=> array(),
        'title'=> array(),
        'body'=> array('allowEmpty'=>true)
    );

    public static function fetch($slug) {

        $mapper = new Application_Model_PageMapper();
        $row = $mapper->get('slug', $slug);
        if ($row)
            return new self($row);
        else
            return NULL;
    }

    public static function delete($slug) {

        $mapper = new Application_Model_PageMapper();
        $mapper->delete('slug', $slug);
    }
}