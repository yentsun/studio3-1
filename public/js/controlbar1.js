(function(jwplayer){

  var template = function(player, config, div) {
    
	zapp.data = '';
	zapp.duration = 0;
	
    player.onReady(this._show1);
	player.onMeta(_onmeta);
	
    this._show = function(data) {
		//div.innerHTML = '<div class="markvideo"><div class="markinner">Test</div></div>';
		zapp.data = data;
		div.innerHTML = data;
		if(zapp.duration) {
			$(div).find('.markvideo').each(function(i, e){
				var w = jwplayer("video_player").getWidth();
				var n = w/zapp.duration;
				var t = $(e).find('a').attr('t');
				$(e).css('left', n * t + 'px');
			});
		}
		else {
			$(div).find('.markvideo').each(function(i, e){
					var w = jwplayer("video_player").getWidth();
					var n = $(div).find('.markvideo').length;
					$(e).css('left', w * (i + 1) / (n + 1) + 'px');
					
			});
		}
		if(zapp.data) $(div).css('display', 'block');
    };
	
	this._show1 = function() {
		if(zapp.data) $(div).css('display', 'block');
    };
	
	this._hide = function() {
		$(div).css('display', 'none');
	}
	
	function _onmeta(meta) {
		zapp.duration = jwplayer("video_player").getDuration();
		if(zapp.data) {
			$(div).find('.markvideo').each(function(i, e){
				var w = jwplayer("video_player").getWidth();
				var n = w/zapp.duration;
				var t = $(e).find('a').attr('t');
				$(e).css('left', n * t + 'px');
			});
		}
	}
    
    this.resize = function(width, height) {
		div.style.position = 'absolute';
		div.style.zIndex = 60000;
		div.style.height = 7 + 'px';
		div.style.width = width + 'px';
		$(div).css('margin-top', height - 7 - 50  + 'px');
		$(div).css('background', '#053706');
		$(div).css('height', '7px');
		$(div).css('cursor', 'auto');
		if(!zapp.data) $(div).css('display', 'none');
		if(zapp.duration) {
			$(div).find('.markvideo').each(function(i, e){
				var w = jwplayer("video_player").getWidth();
				var n = w/zapp.duration;
				var t = $(e).find('a').attr('t');
				$(e).css('left', n * t + 'px');
			});
		}
		else {
			$(div).find('.markvideo').each(function(i, e){
					var w = jwplayer("video_player").getWidth();
					var n = $(div).find('.markvideo').length;
					$(e).css('left', w * (i + 1) / (n + 1) + 'px');
			});
		}
    };
    
  };

  jwplayer().registerPlugin('controlbar1', template);


})(jwplayer);

$('.markvideo').live('mouseover', function(){
	var w = jwplayer("video_player").getWidth();
	var l = $(this).position().left - 95;
	var k = - 95;
	if(l < 0) k =0;
	if(l > (w - 98)) k = -95 + w - l ;
	$(this).find('.markinner').css('left', k + 'px');
	$(this).find('.markinner').css('top', ($(this).find('.markinner').height()+23)  * (-1) + 'px');
	$(this).find('.markinner').css('display', 'block');
});
$('.markvideo').live('mouseout', function(){
	$(this).find('.markinner').css('display', 'none');
})

$('.markvideo').live('click',function(){

		var s = $(this).find('a').attr('t');
		jwplayer("video_player").seek(s);
		
		return false;
});