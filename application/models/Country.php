<?php

class Application_Model_Country extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_CountryMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>True),
        'title'=> array(),
        'image_file'=> array('allowEmpty'=>True),
        'order_id'=> array('Int', 'default'=>0),
    );

    public static function fetch($id=null, $title=null) {

        $mapper = new Application_Model_CountryMapper();
        $data_array = $id
            ? $mapper->get('id', (int) $id)
            : $mapper->get('title', $title);
        if ($data_array)
            return new self($data_array);
        else
            return NULL;
    }
}