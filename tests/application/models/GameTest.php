<?php

class GameTest extends PHPUnit_Framework_TestCase {

    const GAME_ID = 24114;
    const GAME2_ID = 445;

    function setUp() {

        $this->_mapper = new Application_Model_GameMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();

        //delete old index
        $this->_search = new Application_Model_Search('Application_Model_Game');
        $this->_search->delete_index();

        //recreate a new one
        $this->_search = new Application_Model_Search('Application_Model_Game');

        //add all games to index
        $games = Application_Model_Game::fetch_all();
        foreach ($games as $game) {
            $this->_search->save_document($game->to_search_doc());
        }
    }

    function tearDown() {

        $this->_adapter->rollBack();
        $this->_search->delete_index();
    }

    public function test_create_game() {

        $this->assertEquals(2, $this->_search->get_index()->count());

        $game_data = array(
            'datetime'=> '22:00 12.09.2011',
            'arena_title'=> 'Уфа-Арена',
            'team_one_id'=> TeamTest::OMSK_A_ID,
            'team_two_id'=> TeamTest::OMSK_2_ID,
            'category_id'=> CategoryTest::LEAGUE_ID,
            'video_path'=>
            '/videoo0/khl/001_2011.09.12_Salavat_Yulaev_v_Atlant.mp4',
            'number'=> '1',
            'game_score'=> '5:3',
            'protocol_html'=> '<h5 class="center">Голы</h5>
			<ul class="lenta">
				<li class="col"><table class="table2"><tr>
							<td> </td>
							<td><strong>24. Счастливый Пётр</strong><br />&nbsp;(10. Нильссон Роберт, 28. Кольцов Константин)</td>
							<td><img src="/images/ico_sh2.png" alt=""></td>
							<td>1:3</td>
							<td>30:45</td>
					</tr><tr>
							<td> </td>
							<td><strong>11. Трубачёв Юрий</strong><br />&nbsp;(17. Атюшов Виталий, 25. Козлов Виктор)</td>
							<td><img src="/images/ico_sh2.png" alt=""></td>
							<td>2:3</td>
							<td>36:45</td>
					</tr><tr>
							<td> </td>
							<td><strong>75. Стеглик Рихард</strong><br />&nbsp;(10. Нильссон Роберт, 24. Счастливый Пётр)</td>
							<td><img src="/images/ico_sh2.png" alt=""></td>
							<td>3:3</td>
							<td>44:21</td>
					</tr><tr>
							<td> </td>
							<td><strong>27. Григоренко Игорь</strong><br />&nbsp;(42. Зиновьев Сергей, 47. Радулов Александр)</td>
							<td><img src="/images/ico_sh2.png" alt=""></td>
							<td>4:3</td>
							<td>48:56</td>
					</tr><tr>
							<td> </td>
							<td><strong>27. Григоренко Игорь</strong><br />&nbsp;(47. Радулов Александр)</td>
							<td><img src="/images/ico_sh2.png" alt=""></td>
							<td>5:3</td>
							<td>59:36</td>
					</tr></table></li>
				<li class="col"><table class="table2"><tr>
							<td> </td>
							<td><strong>19. Закриссон Патрик</strong><br />&nbsp;</td>
							<td><img src="/images/ico_sh2.png" alt=""></td>
							<td>0:1</td>
							<td>02:58</td>
					</tr><tr>
							<td> </td>
							<td><strong>8. Батыршин Рафаэль</strong><br />&nbsp;(28. Нискала Янне, 36. Уппер Дмитрий)</td>
							<td><img src="/images/ico_sh2.png" alt=""></td>
							<td>0:2</td>
							<td>03:23</td>
					</tr><tr>
							<td> </td>
							<td><strong>92. Радивоевич Бранко</strong><br />&nbsp;(19. Закриссон Патрик, 27. Ковалёв Алексей)</td>
							<td><img src="/images/ico_sh2.png" alt=""></td>
							<td>0:3</td>
							<td>17:30</td>
					</tr></table></li>
			</ul><h5 class="center">Удаления</h5>
			<ul class="lenta">
				<li class="col"><table class="table2"><tr>
							<td> </td>
							<td><strong>98. Картаев Владислав</strong></td>
							<td>2</td>
							<td>16:31</td>
					</tr><tr>
							<td> </td>
							<td><strong>42. Зиновьев Сергей</strong></td>
							<td>2</td>
							<td>38:56</td>
					</tr><tr>
							<td> </td>
							<td><strong>47. Радулов Александр</strong></td>
							<td>2</td>
							<td>46:16</td>
					</tr><tr>
							<td> </td>
							<td><strong>92. Мирнов Игорь</strong></td>
							<td>2</td>
							<td>51:00</td>
					</tr><tr>
							<td> </td>
							<td><strong>40. Эрсберг Эрик</strong></td>
							<td>2</td>
							<td>52:17</td>
					</tr></table></li>
				<li class="col"><table class="table2"><tr>
							<td> </td>
							<td><strong>30. Барулин Константин</strong></td>
							<td>2</td>
							<td>00:48</td>
					</tr><tr>
							<td> </td>
							<td><strong>93. Жердев Николай</strong></td>
							<td>2</td>
							<td>13:26</td>
					</tr><tr>
							<td> </td>
							<td><strong>93. Жердев Николай</strong></td>
							<td>2</td>
							<td>17:43</td>
					</tr><tr>
							<td> </td>
							<td><strong>5. Фернхольм Даниэль</strong></td>
							<td>2</td>
							<td>27:52</td>
					</tr><tr>
							<td> </td>
							<td><strong>5. Фернхольм Даниэль</strong></td>
							<td>2</td>
							<td>36:10</td>
					</tr><tr>
							<td> </td>
							<td><strong>30. Барулин Константин</strong></td>
							<td>2</td>
							<td>44:06</td>
					</tr><tr>
							<td> </td>
							<td><strong>27. Ковалёв Алексей</strong></td>
							<td>2</td>
							<td>47:18</td>
					</tr><tr>
							<td> </td>
							<td><strong>5. Фернхольм Даниэль</strong></td>
							<td>2</td>
							<td>55:35</td>
					</tr></table></li>
			</ul>',
            'scoreboard_html'=> '<table>
						<thead>
							<tr><td>1</td><td>2</td><td>3</td></tr>
						</thead>
						<tbody><tr><td>0</td><td>2</td><td>5</td></tr><tr><td>3</td><td>3</td><td>3</td></tr></tbody>
				</table>',
            'video_time'=> '2011-09-12 15:00:00'
        );
        $new_game_id = Application_Model_Game::create($game_data);
        $this->_created_game_id = $new_game_id;
        $this->assertInternalType('int', $new_game_id);
        $new_game = Application_Model_Game::fetch($new_game_id);
        $this->assertEquals('5:3', $new_game->game_score);
        $this->assertEquals('22:00 12.09.2011',
                            $new_game->datetime(Application_Model_Game::DATETIME_FORMAT));
        $this->assertEquals('2011-09-12T22:00:00+04:00',
                            $new_game->datetime(Zend_Date::ISO_8601));
        $this->assertEquals(TeamTest::OMSK_A_ID, $new_game->team_one_id);

        $this->assertEquals(3, count(Application_Model_Game::fetch_all(null, null, null)));

        //strange thing ahead: if count assertion is called before notnul
        //- it fails
        $this->assertNotNull($this->_search->fetch($new_game_id));
        $this->assertEquals(3, $this->_search->get_index()->count());
    }

    public function test_update_game() {

        $data = array(
            'arena_title'=> 'Витязь-Арена',
            'rubbish_prop'=> '345gf' //should be ignored
        );
        $updated_game = Application_Model_Game::update($data,
                                                          self::GAME_ID);
        $this->assertInstanceOf('Application_Model_Game', $updated_game);
        $this->assertEquals('Витязь-Арена', $updated_game->arena_title);
    }

    public function test_update_with_wrong_data() {

        $data = array(
            'team_one_id'=> 'Команда А',
            'rubbish_prop'=> '345gf' //should be ignored
        );
        $this->setExpectedException('Application_Exception_EntityNotValid');
        $updated_game = Application_Model_Game::update($data,
                                                          self::GAME_ID);
        $this->assertInstanceOf('Application_Model_Game', $updated_game);
        $this->assertEquals('Витязь-Арена', $updated_game->arena_title);
    }

    public function test_delete_game() {

        Application_Model_Game::delete(self::GAME_ID);
        $deleted_game = Application_Model_Game::fetch(self::GAME_ID);
        $this->assertNull($deleted_game);
    }

    public function test_fetch_all_by_team() {

        $team_games = Application_Model_Game
                          ::fetch_all(TeamTest::OMSK_A_ID);
        $this->assertEquals(2, count($team_games));
        foreach ($team_games as $game) {
            $this->assertEquals(CategoryTest::LEAGUE_ID, $game->category()->id);
            $this->assertInstanceOf('Application_Model_Team', $game->team_one());
            $this->assertInstanceOf('Application_Model_Team', $game->team_two());
        }

        $game = Application_Model_Game::fetch(self::GAME_ID);
        $this->assertEquals(TeamTest::OMSK_A_ID, $game->winner()->id);
    }

//    public function test_fetch_by_category() {
//
//        $games = Application_Model_Game::fetch_all(null, null,
//                                                   CategoryTest::HOCKEY_ID);
//        $this->assertEquals(2, count($games));
//    }

    public function test_fetch_paginated() {

        $games = Application_Model_Game::fetch_all(null, null, 1);
        $this->assertNotNull(Application_Model_Game::$paginator);
        $this->assertEquals(1, count($games));
    }

    public function test_process_csv() {

        //with all teams present
//        $games = Application_Model_Game
//                    ::process_csv(dirname(__FILE__).'/../../data/final.csv',
//                                  CategoryTest::LEAGUE_ID);
//        $this->assertEquals(7, count($games));
//        foreach ($games as $game) {
//            $this->assertInstanceOf('Application_Model_Game', $game);
//        }

        //read utf-16le
        $games = Application_Model_Game
                    ::process_csv(dirname(__FILE__).'/../../data/czerasp_bom.txt',
                                  CategoryTest::LEAGUE_ID);
        $this->assertEquals(7, count($games));

        //with all teams absent
        $this->setExpectedException('Application_Exception_TeamNotFound');
        $games = Application_Model_Game
        ::process_csv(
            dirname(__FILE__).'/../../data/final_absent_teams.csv',
            CategoryTest::LEAGUE_ID);
    }

    public function test_process_xls() {

        $result = Application_Model_Game
                        ::process_xls(dirname(__FILE__).'/../../data/002.xls');
        $this->assertEquals(1, count($result));
    }
}