<?php

class Application_Model_PlayerGameStatMapper extends Application_Model_Mapper {

    // has to be overriden by child classes
    protected $_table_name = null;
    protected $_map = array();

    public function fetch_all($game_id=null, $player_id=null) {

        $player_mapper = new Application_Model_PlayerMapper();
        $game_mapper = new Application_Model_GameMapper();

        $select = $this->_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_table_name)
            ->join(array('player'=> $player_mapper->get_table_name()),
                         $this->_map['player_id'].' = player.id',
                          array('player_name'=>'player.'
                                    .$player_mapper->get_map_value('name'),
                                'player_photo'=>'player.'
                                    .$player_mapper->get_map_value('photo')))
        ;
        if ($game_id)
            $select->where($this->_table_name.'.'
                          .$this->_map['game_id'].' = ?', $game_id);
        if ($player_id)
            $select->where($this->_table_name.'.'
                          .$this->_map['player_id'].' = ?', $player_id);
        $rows = $this->_gateway->fetchAll($select);
        return $rows;
    }
}