<?php

class Application_Model_GroupMapper extends Application_Model_Mapper {

    protected $_table_name = 'groupe';

    protected $_map = array(
        'id'=> 'id',
        'title'=> 'ml_title_1',
        'parent_id'=> 'parentid'
    );
}

