//html5
document.createElement("header");
document.createElement("nav");
document.createElement("section");
document.createElement("article");
document.createElement("aside");
document.createElement("footer");
document.createElement("hgroup");
document.createElement("figure");
document.createElement("time");

// классы к первому и последнему элементам списка
$(function(){
   $("ul li:last-child").addClass("last-child");
   $("ul li:first-child").addClass("first-child");
   $("ul li:nth-child(3n+3)").addClass("rd3");
   $("table.table_fav tr td:last-child").addClass("last-child");
   $("table tbody tr:nth-child(2n+2)").addClass("rd2");
   $("table.prices tr td:nth-child(3n+3)").addClass("rd3");
   $("table.prices tr th:nth-child(3n+3)").addClass("rd3")
})

// стили ячейкам таблиц
$(function(){
   $("table tr td:nth-child(1)").addClass("n1");
   $("table tr td:nth-child(2)").addClass("n2");
   $("table tr td:nth-child(3)").addClass("n3");
   $("table tr td:nth-child(4)").addClass("n4");
   $("table tr td:nth-child(5)").addClass("n5");
   $("table tr td:nth-child(6)").addClass("n6");
   $("table tr td:nth-child(7)").addClass("n7");
   $("table tr td:nth-child(8)").addClass("n8");
   $("table tr td:nth-child(9)").addClass("n9");
   $("table tr td:nth-child(10)").addClass("n10");
   $("table tr td:nth-child(11)").addClass("n11");
   $("table tr td:nth-child(12)").addClass("n12");
   $("table tr td:nth-child(13)").addClass("n13");
   $("table tr td:nth-child(14)").addClass("n14")
   
    $(".table2 td").addClass("border_right");
	$(".table2 td:last-child").removeClass("border_right");
	
})

// табы
$(function () {
    var tabContainers = $('div.tabs1 > div');
    tabContainers.hide().filter(':first').show();
    
    $('div.tabs1 ul.tabNavigation a').click(function () {
        tabContainers.hide();
        tabContainers.filter(this.hash).show();
        $('div.tabs1 ul.tabNavigation a').removeClass('selected');
        $(this).addClass('selected');
        return false;
    }).filter(':first').click();
});

$(function () {
    var tabContainers = $('div.tabs2 > div');
    tabContainers.hide().filter(':first').show();
    
    $('div.tabs2 ul.tabNavigation a').click(function () {
        tabContainers.hide();
        tabContainers.filter(this.hash).show();
        $('div.tabs2 ul.tabNavigation a').removeClass('selected');
        $(this).addClass('selected');
        return false;
    }).filter(':first').click();
});

// скрываем label у полей
jQuery(document).ready( function() {
	jQuery("#search input[type='text'], #search input[type='password'], #search textarea").blur(
		function() {
			jQuery("#search label[for='" + jQuery(this).attr('id') + "']").css("textIndent", (((jQuery(this). val(jQuery.trim(jQuery(this).val())).val() == '') || (jQuery(this).val(jQuery. trim(jQuery(this).val())).val() == -1)) ? "0px" : "-9999px"));
		}
	).focus(
		function() {
			jQuery("#search label[for='" + jQuery(this). attr('id') + "']").css("textIndent", "-9999px");
		}
	).trigger("blur");
});

// выпадающее меню
/*$(document).ready(function(){
    $('nav ul li').hover(
        function() {
            $(this).addClass("active");
            $(this).find('ul').stop(true, true);
            $(this).find('ul').slideDown();
        },
        function() {
            $(this).removeClass("active");
            $(this).find('ul').slideUp('slow');
        }
    );
});
*/
$(document).ready(function() { 
        $('ul.sf-menu').superfish();
		$('ul.sf-menu > li > ul').each(function() {
			$(this).css('left', $(this).parent().width()/2 - $(this).width()/2);
		});
}); 

/* Триал */
$(document).ready(function(){
	$('.trial_form select[name=type]').bind('change', function(){
			if($(this).val() == 0) {
				$('.trial_form input[name="club"]').parent().parent().removeAttr('class');
				
				$('.trial_form input[name="num_lic"]').parent().parent().attr('class', 'disabl');
				$('.trial_form input[name="agency"]').parent().parent().attr('class', 'disabl');
			}
			else {
				$('.trial_form input[name="num_lic"]').parent().parent().removeAttr('class');
				$('.trial_form input[name="agency"]').parent().parent().removeAttr('class');
				
				$('.trial_form input[name="club"]').parent().parent().attr('class','disabl');
			}
		});
	$('.trial_form select[name=type]').change();
	$('.trial_form ul.errors').each(function(i, el){ 
		$(el).parent().parent().find('dt').css('color', 'red');
	});
});

zapp = {};
// Выбор сезонов
$(document).ready(function(){
	$('.choose select').change(zapp.select_change);
});

zapp.select_change = function() {
	if($(this).attr('name') == 'url') {
		$('.choose form').attr('action', $(this).val());
	}
	$('.choose input[name=page]').val(0);
	$(this).parent().submit();
};

// Сортировка
$(document).ready(function(){
	$('.order').click(zapp.select_order);
});
zapp.select_order = function() {
	$('.choose form').append('<input type="hidden" value="' + $(this).attr('order') + '" name="order_' + $(this).attr('el') + '" />');
	$('.choose form').submit();
	return false;
};

// Next Back
$(document).ready(function(){
	$('.select_back').click(
		function(){
			$('.choose input[name=page]').val($('.choose input[name=page]').val() * 1 - 1);
			$('.choose form').submit();
			return false;
		}
	);
	$('.select_forward').click(
		function(){
			$('.choose input[name=page]').val($('.choose input[name=page]').val() * 1 + 1);
			$('.choose form').submit();
			return false;
		}
	);
});
//Favorites

$(document).ready(function(){
	$('.ico_fav a').click(function(){
		$.blockUI({message: 'Loading...',
			overlayCSS:{opacity:0.5,cursor:'default'}
		});
	
		$.ajax($(this).attr('href'), {
			type: 'POST',
			data: {
				type: $(this).attr('data-elt'),
				id: $(this).attr('data-el') 
			},
			complete: function() {
				$.unblockUI();
			}
		});
		
		return false;
		
	});
	
	$('a.del').click(function(){
		$.blockUI({message: 'Loading...',
			overlayCSS:{opacity:0.5,cursor:'default'}
		});
	
		$.ajax($(this).attr('href'), {
			type: 'POST',
			data: {
				id: $(this).attr('data-el') 
			},
			complete: function() {
				$.unblockUI();
				var url = window.location.href;
				window.location.href = url;
			}
		});
		
		return false;
		
	});
});


//GMT
$(document).ready(function(){
	$('.gmt').click(zapp.gmt_select);
	$('.gmt_menu div').live('click', zapp.gmt_select_item);
});

zapp.gmt_select = function() {
	$('.gmt_menu').fadeIn();
};

zapp.gmt_select_item = function() {
	$('.gmt_menu').fadeOut();
	
	$.blockUI({message: 'Loading...',
			overlayCSS:{opacity:0.5,cursor:'default'}
	});
	
	$.ajax('/ru/gmt', {
		type: 'POST',
		data: {
			gmt: $(this).attr('gmt')
		},
		complete: function() {
			$.unblockUI();
			var url = window.location.href;
			window.location.href = url;
		}
	});

	var s = $(this).html();
	$('.gmt').html(s);
	
	return false;
};


// всплывающие окна - галереи, видео, формы
	$(document).ready(function(){
		$("li a[rel^='pp']").prettyPhoto({
			animation_speed: 'fast', /* fast/slow/normal */
			autoplay_slideshow: false, /* true/false */
			opacity: 0.80, /* Value between 0 and 1 */
			show_title: true, /* true/false */
			allow_resize: true, /* Resize the photos bigger than viewport. true/false */
			default_width: 500,
			default_height: 400,
			counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
			theme: 'light_rounded', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			horizontal_padding: 20, /* The padding on each side of the picture */
			hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
			wmode: 'opaque', /* Set the flash wmode attribute */
			autoplay: false, /* Automatically start videos: True/False */
			modal: false, /* If set to true, only the close button will close the window */
			deeplinking: true, /* Allow prettyPhoto to update the url to enable deeplinking. */
			overlay_gallery: true, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
			keyboard_shortcuts: true, /* Set to false if you open forms inside prettyPhoto */
			changepicturecallback: function(){}, /* Called everytime an item is shown/changed */
			callback: function(){}, /* Called when prettyPhoto is closed */
			ie6_fallback: true,
			slideshow:10,
			markup: '<div class="pp_pic_holder"> \
						<a class="pp_close" href="#">Close</a> \
						<div class="ppt">&nbsp;</div> \
						<div class="pp_top"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
						<div class="pp_content_container"> \
							<div class="pp_left"> \
							<div class="pp_right"> \
								<div class="pp_content"> \
									<div class="pp_loaderIcon"></div> \
									<div class="pp_fade"> \
										<a href="#" class="pp_expand" title="Expand the image">Expand</a> \
										<div class="pp_hoverContainer"> \
											<a class="pp_next" href="#">next</a> \
											<a class="pp_previous" href="#">previous</a> \
										</div> \
										<div id="pp_full_res"></div> \
										<div class="pp_details"> \
											<div class="pp_nav"> \
												<a href="#" class="pp_arrow_previous">Previous</a> \
												<p class="currentTextHolder">0/0</p> \
												<a href="#" class="pp_arrow_next">Next</a> \
											</div> \
											<p class="pp_description"></p> \
											{pp_social} \
										</div> \
									</div> \
								</div> \
							</div> \
							</div> \
						</div> \
						<div class="pp_bottom"> \
							<div class="pp_left"></div> \
							<div class="pp_middle"></div> \
							<div class="pp_right"></div> \
						</div> \
					</div> \
					<div class="pp_overlay"></div>',
			gallery_markup: '<div class="pp_gallery"> \
								<a href="#" class="pp_arrow_previous">Previous</a> \
								<div> \
									<ul> \
										{gallery} \
									</ul> \
								</div> \
								<a href="#" class="pp_arrow_next">Next</a> \
							</div>',
			image_markup: '<img id="fullResImage" src="{path}" />',
			flash_markup: '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="{width}" height="{height}"><param name="wmode" value="{wmode}" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{path}" /><embed src="{path}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="{width}" height="{height}" wmode="{wmode}"></embed></object>',
			quicktime_markup: '<object classid="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" codebase="http://www.apple.com/qtactivex/qtplugin.cab" height="{height}" width="{width}"><param name="src" value="{path}"><param name="autoplay" value="{autoplay}"><param name="type" value="video/quicktime"><embed src="{path}" height="{height}" width="{width}" autoplay="{autoplay}" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/"></embed></object>',
			iframe_markup: '<iframe src ="{path}" width="{width}" height="{height}" frameborder="no"></iframe>',
			inline_markup: '<div class="pp_inline">{content}</div>',
			custom_markup: '',
			social_tools: false /* html or false to disable */
		});
		$("li a[rel^='pp2']").prettyPhoto({animation_speed:'normal',theme:'dark_rounded',slideshow:6000, autoplay_slideshow: true});
	});

function base64decode(str) {
    var b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg'+
                   'hijklmnopqrstuvwxyz0123456789+/=';
    var b64decoded = '';
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
 
    str = str.replace(/[^a-z0-9+/=]/gi, '');
 
    for (var i=0; i<str.length;) {
        enc1 = b64chars.indexOf(str.charAt(i++));
        enc2 = b64chars.indexOf(str.charAt(i++));
        enc3 = b64chars.indexOf(str.charAt(i++));
        enc4 = b64chars.indexOf(str.charAt(i++));
 
        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
 
        b64decoded = b64decoded + String.fromCharCode(chr1);
 
        if (enc3 < 64) {
            b64decoded += String.fromCharCode(chr2);
        }
        if (enc4 < 64) {
            b64decoded += String.fromCharCode(chr3);
        }
    }
    return b64decoded;
}

function basedecode(str) {
 
    r = '';
	for (var i=0; i<str.length; i++) {
        c = str.charCodeAt(i);
		c = c^138;
		r += String.fromCharCode(c);
    }
    return r;
}

// video
$(document).ready(function(){
	var h = $("#video_player").attr('f');
	zapp.create_player(h);
	$('#but_download').click(function() {
		hh = base64decode($(this).attr('f'));
		hh = basedecode(hh);
		window.location.href=(hh);
	});	
	$('#but_play').click(function() {
		hh = $(this).attr('f');
		if(!hh) return;
		hh = base64decode(hh);
		hh = basedecode(hh);
		jwplayer("video_player").load({file: hh});
		jwplayer("video_player").play();
	});
	$('#but_obzor').click(function() {
		hh = $(this).attr('f');
		if(!hh) return;
		hh = base64decode(hh);
		hh = basedecode(hh);
		jwplayer("video_player").load({file: hh});
		jwplayer("video_player").play();
	});
	
	$('.marktime').live('click',function(){

		var s = $(this).attr('t');
		jwplayer("video_player").seek(s);
		
		return false;
	});
});


zapp.create_player = function(h) {
	
	if(!h) return;
	
	h = base64decode(h);
	h = basedecode(h);
	
	jwplayer("video_player").setup({
		plugins: {
			'/js/controlbar1.js': {}
		},
		/*controlbar: 'bottom',*/
		//autostart: true,
		file: h,
		height: $("#video_player").height(),
		//width: $("#video_player").parent().parent().width() - $("#video_player").parent().parent().width() * 0.25,
		width: $("#video_player").width(),
		provider: "http",
		modes: [
			{ type: "html5" },
			{ type: "flash", src: "/img/player.swf" }
		]
	});
	
	jwplayer("video_player").onPlay(function(state){
		setTimeout(function(){

		}, 500);
	});
	
	jwplayer("video_player").getPlugin("controlbar").onShow(function(){
		jwplayer("video_player").getPlugin("controlbar1")._show1();
	});
	jwplayer("video_player").getPlugin("controlbar").onHide(function(){
		jwplayer("video_player").getPlugin("controlbar1")._hide();
	});

				$.ajax(window.location.href, {
					type: 'POST',
					data: {
						match: $("#video_player").attr('m'),
						type: 1
					},
					success: function(data) {
						jwplayer("video_player").getPlugin("controlbar1")._show(data);
					}
			});
	
	$.ajax(window.location.href, {
		type: 'POST',
		data: {
			match: $("#video_player").attr('m'),
			type: 1,
			getbig: 1
		},
		success: function(data) {
			$('div.video table.mmmm').append(data);
		}
	});
	
	$(window).resize(function(){
		//$("#video_player").height($(window).height() - $(window).height()/10);
		//$("#video_player").width($(window).width() - $(window).width()/10);
		//jwplayer("video_player").resize($(window).width() - $(window).width()/10, $(window).height() - $(window).height()/10);
	});

}