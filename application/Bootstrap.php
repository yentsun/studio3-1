<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _init_settings() {

        $settings = new Zend_Config_Ini(
            APPLICATION_PATH.'/configs/settings.ini',
            APPLICATION_ENV);
        Zend_Registry::set('settings', $settings);
    }

    protected function _init_search_config() {

        Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding('utf-8');
        Zend_Search_Lucene_Analysis_Analyzer::setDefault(
            new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8_CaseInsensitive ()
        );
    }

    protected function _initResourceAutoloader() {

        $autoloader = new Zend_Loader_Autoloader_Resource(
            array('basePath'  => APPLICATION_PATH,
                  'namespace' => 'Application'));
        $autoloader->addResourceType('exception', 'exceptions/', 'Exception');
        $autoloader->addResourceType('filter', 'filters/', 'Filter');
    }
}