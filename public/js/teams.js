var timer;

$(function(){
    $('#team_search').keyup(function(){
        var val = $(this).val();
        if(timer) {
            clearTimeout(timer);
            timer = null;
        }
        timer = setTimeout(
        function(){
            if (val.length >= 3) {
                $.post('/admin/ajax-query-teams/query/'+val, function(response){
                    $('#teams tbody').html(response);
                    $('.pagination').hide();
                });
            }
        },
        500);
    });
});