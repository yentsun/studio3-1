<?php

class PageControllerTest extends Zend_Test_PHPUnit_ControllerTestCase
{

    public function setUp()
    {
        $this->bootstrap = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH.'/configs/application.ini');
        parent::setUp();
        $this->getFrontController()->getRouter()->addDefaultRoutes();
    }

    public function testIndexAction() {
        $params = array('action'=>'index', 'controller'=>'Page',
                        'module'=>'default', 'slug'=>'benefits');
        $url = $this->url($params);
        $this->dispatch($url);
        
        // assertions
        $this->assertModule($params['module']);
        $this->assertController($params['controller']);
        $this->assertAction($params['action']);
        $this->assertQueryContentContains(
            'body',
            'You will get many benefits while working with us');
    }
}