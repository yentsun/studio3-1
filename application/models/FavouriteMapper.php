<?php

class Application_Model_FavouriteMapper extends Application_Model_Mapper {

    protected $_table_name = 'favorites';

    protected $_map = array(
        'id'=> 'id',
        'datetime'=> 'date',
        'type'=> 'type',
        'user_id'=> 'parentid',
        'object_id'=> 'itemid'
    );

    public function fetch_all($type) {

        $user_id = Application_Model_User::get_auth_user()->id;
        $select = $this->_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_table_name)
            ->where($this->_table_name.'.'.$this->_map['user_id'].' = ?', $user_id)
            ->where($this->_table_name.'.'.$this->_map['type'].' = ?', $type)
            ->order($this->_table_name.'.'.$this->_map['datetime']);
        if ($type == 2) {
            $team_mapper = new Application_Model_TeamMapper();
            $team_map = $team_mapper->get_map();
            $select
                ->joinLeft(array('team'=>'team'),
                         $this->_map['object_id'].' = team.id',
                          array('team_title'=>'team.'.$team_map['title'],
                                'team_logo'=>'team.'.$team_map['logo']));
        }
        return $this->_gateway->fetchAll($select);
    }
}