<?php

class UserControllerTest extends Zend_Test_PHPUnit_ControllerTestCase
{

    public function setUp()
    {
        $this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
        parent::setUp();
        $this->getFrontController()->getRouter()->addDefaultRoutes();
    }

    public function tearDown()
    {

        Application_Model_User::delete('ikeee@example.com');
    }

    public function testIndexAction()
    {
        $params = array('action' => 'index', 'controller' => 'User', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);

        // assertions
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertQueryContentContains(
            'div#view-content p',
            'View script for controller <b>' . $params['controller'] . '</b> and script/action name <b>' . $params['action'] . '</b>'
        );
    }

    public function testRegisterAction()
    {

        $params = array('action'=>'register', 'controller'=>'User');
        $url = $this->url($params);
        $this->request->setMethod('POST');
        $this->request->setPost(array(
                'email'=> 'ikeee@example.com',
                'first_name'=> 'Ike',
                'last_name'=> 'Broflowski',
                'phone'=> '+79990000001',
                'locale'=> 'ru_RU',
                'club'=> 'AK Bars',
                'job_title'=> 'scout',
                'sport_id'=> '1'));

        $this->dispatch($url);
//        print_r($this->getResponse()->getBody());
        $this->assertNotController('error');
        $this->assertRedirectTo('/user/register-success');
    }

    public function test_register_invalid()
    {

        $params = array('action'=>'register', 'controller'=>'User');
        $url = $this->url($params);
        $this->request->setMethod('POST');
        $this->request->setPost(array(
                'email'=> 'fgdfgd',
                'first_name'=> 'Ike',
                'last_name'=> 'Broflowski',
                'phone'=> '+79990000001',
                'locale'=> 'ru_RU',
                'club'=> 'AK Bars',
                'job_title'=> 'scout',
                'sport_id'=> '1'));

        $this->dispatch($url);
//        print_r($this->getResponse()->getBody());
        $this->assertNotController('error');
        $this->assertQueryContentContains(
            'div.alert-error',
            'Форма заполнена неверно!'
            );
    }

    public function test_correct_login_logout()
    {

        //login (actually admin login)
        self::login($this);

        $user = Application_Model_User::get_auth_user();
        $this->assertInstanceOf('Application_Model_User', $user);
        $this->assertEquals($user->email, 'yentsun@yandex.ru');
        $this->assertRedirectTo('/user/dashboard');

        //test some acl
        $acl = Zend_Registry::get('acl');
        $this->assertTrue($acl->isAllowed($user->get_status(), 'admin'));
        $this->assertTrue($acl->isAllowed($user->get_status(), 'team'));

        //now logout
        $this->resetRequest()->resetResponse();
        $params = array('action'=>'logout', 'controller'=>'User',
                        'module'=>'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);

//        print_r($this->getResponse()->getBody());
        $this->assertRedirectTo('/');
        $user = Application_Model_User::get_auth_user();
        $this->assertNull($user);


    }

    public function test_invalid_login()
    {

        $params = array('action'=>'login', 'controller'=>'User',
                        'module'=>'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->getRequest()->setMethod('POST')
              ->setPost(array(
                  'email'=> 'yentsun@yandex.ru',
                  'password'=> 'wrong_password'));
        $this->dispatch($url);

        $user = Application_Model_User::get_auth_user();
        $this->assertNull($user);
    }

    public function testDashboardAction()
    {
        $params = array('action' => 'dashboard', 'controller' => 'User', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
//        print_r($this->getResponse()->getBody());
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public static function login(
        Zend_Test_PHPUnit_ControllerTestCase $test_case_instance) {


        $params = array('action'=>'login', 'controller'=>'User',
                        'module'=>'default');
        $urlParams = $test_case_instance->urlizeOptions($params);
        $url = $test_case_instance->url($urlParams);
        $test_case_instance->getRequest()->setMethod('POST')
            ->setPost(array(
                           'email'=> 'yentsun@yandex.ru',
                           'password'=> 'vaehiodadr'));
        $test_case_instance->dispatch($url);
    }
}