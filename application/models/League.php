<?php

class Application_Model_League extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_LeagueMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>True),
        'title'=> array(),
        'logo'=> array(),
        'country_id'=> 'Int',
        'country'=> array('allowEmpty'=>True),
        'order_id'=> 'Int'
    );

    protected function _post_data_population() {

        $this->country = Application_Model_Category::fetch($this->country_id);
    }

    public static function fetch_all_by_country_id($country_id) {

        $mapper = new Application_Model_LeagueMapper();
        $rows = $mapper->fetch_all(array('country_id'=>$country_id));
        $result_set = array();
        foreach ($rows as $row)
            $result_set[strval($row->id)] = new self($mapper->row_to_array($row));
        return $result_set;
    }

    public function fetch_teams() {

        return Application_Model_Team::fetch_all_by_category_id($this->id);
    }
}

