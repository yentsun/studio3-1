<?php

class Application_Model_PlayerMapper extends Application_Model_Mapper {

    protected $_table_name = 'player';
    protected $_lines_table_name = 'line';
    protected $_player_game_table_name = 'recaps';

    protected $_map = array(
        'id'=> 'id',
        'name'=> 'ml_title_1',
        'datetime'=> 'birth',
        'height'=> 'growth',
        'weight'=> 'weight',
        'hand'=> 'hander',
        'sport_id'=> 'parentid',
        'country_id'=> 'nationalityid',
        'line_id'=> 'line',
        'photo'=> 'pic',
        'promo_video'=> 'promo'
    );

    public function fetch_all($sport_id=null, $offset=0, $per_page=0,
                              $games=null, $team_id=null, $order_by=null,
                              $dir=null, $stats=false, $id=null,
                              $current_team=true, $user_id=null,
                              $line_id=null) {

        $team_mapper = new Application_Model_TeamMapper();
        $team_map = $team_mapper->get_map();
        $pt_join_condition = $current_team
            ? ' AND pt.date_end = "0000-00-00"'
            : '';
        $select = $this->_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->limit($per_page, $offset)
            ->from(array('player'=>$this->_table_name))
            ->joinLeft(array('line'=>'line'),
                      'player.'.$this->_map['line_id'].' = line.id',
                       array('line_title'=>'line.ml_title_1',
                             'line_image_file'=>'line.pic'))
            ->joinLeft(array('c'=>'country'),
                      'player.'.$this->_map['country_id'].' = c.id',
                       array('country_title'=>'c.ml_title_1',
                             'country_image_file'=>'c.pic'))
            ->joinLeft(array('pt'=>'player_team'),
                  'player.'.$this->_map['id'].' = pt.parentid'
                  .$pt_join_condition, //join with current team
                   array('team_id'=>'pt.teamid',
                         'team_entry_date'=>'pt.date',
                         'player_number'=>'pt.number'))
            ->joinLeft(array('t'=>'team'),
                      'pt.teamid = t.id',
                       array('team_title'=>'t.'.$team_map['title'],
                             'team_logo'=>'t.'.$team_map['logo']))
        ;
        if ($order_by=='points') {
            $this->_map['points'] = 'points';
        } else {
            $order_by = 'name';
            $dir = 'ASC';
        }
        if ($line_id)
            $select
                ->where('line.id = ?', $line_id);
        if ($sport_id)
            $select = $select
                ->where($this->_table_name.'.'.$this->_map['sport_id'].'= ?',
                        $sport_id);
        if ($team_id)
            $select = $select
                ->where('pt.teamid = ?', $team_id);
        if ($games and !$stats) {
            $games = (array) $games;
            $select = $select
                ->join(array($this->_player_game_table_name),
                       $this->_table_name.'.'.$this->_map['id'].' = '
                      .$this->_player_game_table_name.'.playerid', array())
                ->where($this->_player_game_table_name.'.parentid IN (?)',
                        $games)
            ;
        }
        if ($user_id) {
            $fav_mapper = new Application_Model_FavouriteMapper();
            $select
                ->join(array('fav'=>$fav_mapper->get_table_name()),
                       $this->_table_name.'.'.$this->_map['id'].' = '
                      .'fav.'.$fav_mapper->get_map_value('object_id'),
                       array('id'=>$this->_table_name.'.'.$this->_map['id'],
                             'fav_id'=>'fav.'.$fav_mapper->get_map_value('id')))
                ->where('fav.'.$fav_mapper->get_map_value('user_id').' = ?',
                        $user_id)
                ->where('fav.'.$fav_mapper->get_map_value('type').' = ?',
                         1)
            ;
        }
        if ($stats) {
            $forw_mapper = new Application_Model_ForwardGameStatMapper();
            $def_mapper = new Application_Model_BackGameStatMapper();
            $goalie_mapper = new Application_Model_GoalieGameStatMapper();
            $forw_select = clone($select);
            $def_select = clone($select);
            $goalie_select = clone($select);
            $forw_select
                ->joinLeft(array('forw_stats'=>$forw_mapper->get_table_name()),
                           sprintf('player.%s = forw_stats.%s',
                                   $this->_map['id'],
                                   $forw_mapper->get_map_value('player_id')),
                           array('total_goals'=> new Zend_Db_Expr(
                                     sprintf('SUM(forw_stats.%s)',
                                             $forw_mapper
                                          ->get_map_value('goals_authored'))),
                                 'games_played'=> new Zend_Db_Expr(
                                     sprintf('SUM(forw_stats.%s)',
                                             $forw_mapper
                                             ->get_map_value('games_played'))),
                                 'plus_minus'=> new Zend_Db_Expr(
                                     sprintf('SUM(forw_stats.%s)',
                                             $forw_mapper
                                             ->get_map_value('plus_minus'))),
                                 'points'=> new Zend_Db_Expr(
                                     sprintf('SUM(forw_stats.%s)+SUM(forw_stats.%s)',
                                             $forw_mapper
                                             ->get_map_value('goals_authored'),
                                             $forw_mapper
                                             ->get_map_value('assists')))))
                ->group('player.'.$this->_map['id'])
            ;
            $def_select
                ->joinLeft(array('def_stats'=>$def_mapper->get_table_name()),
                                  sprintf('player.%s = def_stats.%s',
                                         $this->_map['id'],
                                         $def_mapper->get_map_value('player_id')),
                           array('goals'=> new Zend_Db_Expr(
                                  sprintf('SUM(def_stats.%s)',
                                         $def_mapper
                                          ->get_map_value('goals_authored'))),
                                 'games_played'=> new Zend_Db_Expr(
                                     sprintf('SUM(def_stats.%s)',
                                             $def_mapper
                                             ->get_map_value('games_played'))),
                                 'plus_minus'=> new Zend_Db_Expr(
                                     sprintf('SUM(def_stats.%s)',
                                         $def_mapper
                                             ->get_map_value('plus_minus'))),
                                 'points'=> new Zend_Db_Expr(
                                     sprintf('SUM(def_stats.%s)+SUM(def_stats.%s)',
                                             $def_mapper
                                             ->get_map_value('games_played'),
                                             $def_mapper
                                             ->get_map_value('assists')))))
                ->group('player.'.$this->_map['id'])
            ;
            $goalie_select
                ->joinLeft(array('goalie_stats'=>$goalie_mapper->get_table_name()),
                           sprintf('player.%s = goalie_stats.%s',
                                   $this->_map['id'],
                                   $goalie_mapper->get_map_value('player_id')),
                           array('goals'=> new Zend_Db_Expr(
                                 sprintf('SUM(goalie_stats.%s)',
                                         $goalie_mapper
                                          ->get_map_value('goals_authored'))),
                                 'games_played'=> new Zend_Db_Expr(
                                     sprintf('SUM(goalie_stats.%s)',
                                             $goalie_mapper
                                             ->get_map_value('games_played'))),
                                 'games_lost'=> new Zend_Db_Expr(
                                     sprintf('SUM(goalie_stats.%s)',
                                         $goalie_mapper
                                             ->get_map_value('games_lost'))),
                                 'points'=> new Zend_Db_Expr(
                                     sprintf('SUM(goalie_stats.%s)',
                                         $goalie_mapper
                                             ->get_map_value('games_won')))))
                ->group('player.'.$this->_map['id'])
            ;
            //fetch stats for certain games only
            if ($games) {
                $games = (array) $games;
                    $forw_select
                        ->where('forw_stats.'
                               .$forw_mapper->get_map_value('game_id').' IN (?)',
                                $games);
                    $def_select
                        ->where('def_stats.'
                               .$def_mapper->get_map_value('game_id').' IN (?)',
                                $games);
                    $goalie_select
                        ->where('goalie_stats.'
                               .$goalie_mapper->get_map_value('game_id').' IN (?)',
                                $games);
            }
            $select = $this->_gateway
                ->select()
                ->union(array($forw_select, $def_select, $goalie_select))
            ;
        }
        return $id
            ? $this->_gateway->fetchRow(
                $select
                    ->where($this->_table_name.'.'.$this->_map['id'].'= ?', $id)
                    ->orWhere($this->_table_name.'.'.$this->_map['name'].'= ?', $id))
            : $this->_gateway->fetchAll(
                $select
                    ->order(array($this->_map[$order_by].' '.$dir))
            );
    }

    public function fetch_all_lines($id=null) {

        $lines_gateway = new Zend_Db_Table($this->_lines_table_name);
        $select = $lines_gateway->select();
        if ($id)
            $select = $select->where('id = ?', $id);
        return $lines_gateway->fetchAll($select);
    }

    public function fetch_line_id($stitle) {

        $lines_gateway = new Zend_Db_Table($this->_lines_table_name);
        $select = $lines_gateway->select();
        $select = $select->where('stitle = ?', $stitle);
        return $lines_gateway->fetchRow($select)->id;
    }

    public function fetch_all_by_team_id($team_id) {

        return $this->fetch_all(null, null, null, null, $team_id,
                               'line_id', null, true);
    }

    public function insert_game_record($player_id, $game_id) {

        $player_game_gateway = new Zend_Db_Table($this->_player_game_table_name);
        $player_game_gateway->insert(array('parentid'=> $game_id,
                                           'playerid'=> $player_id));
    }

    public function delete_game_records($game_id) {

        $player_game_gateway = new Zend_Db_Table($this->_player_game_table_name);
        $where = $player_game_gateway
            ->getAdapter()
            ->quoteInto('parentid = ?', $game_id);
        $player_game_gateway->delete($where);
    }
}