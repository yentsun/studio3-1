<?php

class Application_Model_Transfer extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_TransferMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>true),
        'player_id'=> 'Int',
        'team_id'=> 'Int',
        'number'=> array('Int', 'allowEmpty'=>true),
        'datetime'=> array(array('Date', 'yyyy-MM-dd'),
                           'allowEmpty'=>True),
        'datetime_end'=> array(array('Date', 'yyyy-MM-dd'),
                           'allowEmpty'=>True),
        'row'=> array('allowEmpty'=>true)
    );

    protected $_filters = array(
        'number'=> 'Int',
        'datetime'=> array(
            array('LocalizedToNormalized',
                  array('date_format'=> 'dd.MM.YYYY')),
            array('NormalizedToLocalized',
                  array('date_format'=> Zend_Date::ISO_8601))
        ),
        'datetime_end'=> array(
            array('LocalizedToNormalized',
                  array('date_format'=> 'dd.MM.YYYY')),
            array('NormalizedToLocalized',
                  array('date_format'=> Zend_Date::ISO_8601))
        )
    );

    /**
     * Fetch player's transfers from latest to earliest
     * @static
     * @param null $player_id
     * @return array
     */
    public static function fetch_all($player_id, $current=false) {

        $mapper = new Application_Model_TransferMapper();
        $rows = $mapper->fetch_all($player_id, $current);
        $result_set = array();
        foreach ($rows as $row) {
            $transfer = new self($mapper->row_to_array($row));
            $transfer->row = $row;
            $result_set[] = $transfer;
        }
        return $result_set;
    }
}
