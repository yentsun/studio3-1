<?php

class TeamController extends Zend_Controller_Action
{

    public function init() {

        $this->id = $this->_getParam('id');
    }

    public function indexAction() {

        $this->view->headScript()
            ->appendFile('/js/jquery.dataTables.min.js')
            ->appendFile('/js/team-index.js')
        ;

        $this->view->team = Application_Model_Team::fetch($this->id);
        $this->view->games = Application_Model_Game::fetch_all($this->id);
    }

    public function rosterAction() {

        $this->view->headScript()
            ->appendFile('http://vjs.zencdn.net/c/video.js')
            ->appendFile('/js/bootstrap-modal.js')
            ->appendFile('/js/roster.js')
        ;
        $this->view->headLink()
            ->appendStylesheet('http://vjs.zencdn.net/c/video-js.css');
        $id = $this->_getParam('team_id');
        $this->view->team = Application_Model_Team::fetch($id);
        $this->view->games = Application_Model_Game::fetch_all($id);
    }

    public function videosAction() {

        $this->view->headScript()
            ->appendFile('/js/jquery.dataTables.min.js')
            ->appendFile('/js/team-index.js')
        ;
        $this->view->team = Application_Model_Team::fetch($this->id);
        $this->view->games = Application_Model_Game::fetch_all($this->id);
    }
}