<?php

class Application_Filter_YearToISO implements Zend_Filter_Interface {

    public function filter($value) {

        $is_year = is_numeric($value);
        if ($is_year)
            $filtered = $value.'-01-01';
        else
            $filtered = $value;
        return $filtered;
    }
}