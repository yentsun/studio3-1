<?php

class PageController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {

        $slug = $this->_getParam('slug');
        $this->_helper->viewRenderer($slug);
    }
}