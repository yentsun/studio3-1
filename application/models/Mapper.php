<?php
/**
 * An abstract class used to back model mappers
 */
abstract class Application_Model_Mapper extends Whyte_Model_Mapper {

    public $paginator = null;

    protected function _paginate(Zend_Db_Select $select, $page, $perPage) {

        Zend_Paginator::setDefaultScrollingStyle('Elastic');
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('_pagination.phtml');
        $paginator = Zend_Paginator::factory($select);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($perPage);
        $paginator->setPageRange(Zend_Registry::get('settings')->pagination->items->range);

        $this->paginator = $paginator;
        $fO = array('lifetime' => 21600, 'automatic_serialization' => true);
        $bO = array('cache_dir'=>APPLICATION_PATH.'/cache/paginator');
        $cache = Zend_cache::factory('Core', 'File', $fO, $bO);
        Zend_Paginator::setCache($cache);

        return $paginator->getItemsByPage($page);
    }
}