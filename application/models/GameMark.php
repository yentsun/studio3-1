<?php

class Application_Model_GameMark extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_GameMarkMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>true),
        'game_id'=> 'Int',
        'team_id'=> 'Int',
        'time_video'=> array(array('Date', 'HH:mm:ss'), 'allowEmpty'=>true),
        'time_protocol'=> array(array('Date', 'HH:mm:ss')),
        'type'=> 'Int',
        'title'=> array('allowEmpty'=>true),
        'description_line1'=> array('allowEmpty'=>true),
        'description_line2'=> array('allowEmpty'=>true),
        'player_id'=> array('Int', 'allowEmpty'=>true),
        'row'=> array('allowEmpty'=>true)
    );

    protected function _pre_data_population() {

        $this->_filters = array_merge(
            $this->_filters,
            array('time_protocol'=> new Application_Filter_FixTime()));
    }

    public $type_map = array(
        0=> 'Начало',
        1=> 'Удаление',
        2=> 'Силовой',
        3=> 'Гол',
        4=> 'Бросок'
    );

    public static function fetch_all($game_id) {

        $mapper = new Application_Model_GameMarkMapper();
        $rows = $mapper->fetch_all($game_id);
        $result_set = array();
        foreach ($rows as $row) {
            $mark = new Application_Model_GameMark(
                $mapper->row_to_array($row));
            $mark->row = $row;
            $result_set[$mark->id] = $mark;
        }
        return $result_set;
    }

    public function seconds() {

        list($hours, $mins, $secs) = explode(':', $this->time_video);
        return ($hours * 3600 ) + ($mins * 60 ) + $secs;
    }
}