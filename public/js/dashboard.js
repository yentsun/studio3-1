var selected_cat_id = null;
var parent_id;

$(function() {
    $('.collapse-toggle').click(function(){
        var icon = $(this);
        if (icon.hasClass('icon-folder-close'))
            icon.removeClass('icon-folder-close').addClass('icon-folder-open');
        else
            icon.removeClass('icon-folder-open').addClass('icon-folder-close');
    });
    $('#tree ul').sortable({
        handle: '.handle',
        update: function(event, ui) {
            var id_array = $(this).sortable('toArray');
            for (var key in id_array) {
                id_array[key] = id_array[key].split('cat_')[1];
            }
            $.post('/admin/ajax-update-category', {id_array: id_array},
                    function(response) {
                response = jQuery.parseJSON(response);
                if (response.status == 'ok') {
                    //TODO do something to indicate success
                }
            });
        }
    });
    $('.show_edit').click(function(){
        $(this).siblings('.edit').toggle();
    });
    var cat_id_to_select = $.cookie('selected_cat_id');
    select_category($('#'+cat_id_to_select), cat_id_to_select);
});

function add_category() {

    if (selected_cat_id)
        window.location.href = '/admin/update-category/parent_id/'+selected_cat_id;
    else
        alert('Элемент не выбран');
}

function update_category(value) {

    if (selected_cat_id)
        window.location.href = '/admin/update-category/id/'+selected_cat_id;
    else
        alert('Элемент не выбран');
}

function import_from_csv() {

    if (selected_cat_id) {
        window.location.href = '/admin/add-games/parent_id/'+selected_cat_id;
    } else
        window.location.href = '/admin/add-games';
}

function delete_category(id) {

    if (selected_cat_id) {
        var really = confirm('Действительно удалить?');
        if (really)
            window.location.href = '/admin/delete-category/id/'+selected_cat_id;
    }
    else
        alert('Элемент не выбран');
}

function teams() {

    window.location.href = '/admin/teams/sport_id/'+selected_cat_id;
}

function teams_to_season() {

    window.location.href = '/admin/teams-to-category/id/'+selected_cat_id;
}

function games() {

    window.location.href = '/admin/games-by-category/id/'+selected_cat_id;
}

function players() {

    window.location.href = '/admin/players/sport_id/'+selected_cat_id;
}

function select_category(cont, cat_id) {

    var cat_li = $('#cat_'+cat_id);
    $('.tool .btn').not('.always').attr('disabled', true);
    $('.item').removeClass('selected');
    cat_li.parents('.collapse').collapse();
    cat_li.find('.item:first').addClass('selected');
    var node_type = cat_li.attr('data-type');
    $('.tool .edit').attr('disabled', false);
    if (node_type == 'sport') {
        $('#teams_button').attr('disabled', false);
        $('#players_button').attr('disabled', false);
    }
    if (node_type == 'season') {
        $('#teams_seasons_button').attr('disabled', false);
    }
    if (node_type == 'stage') {
        $('#games_button').attr('disabled', false);
        $('#scv_import_button').attr('disabled', false);
    }
    selected_cat_id = cat_id;
    $.cookie('selected_cat_id', cat_id);
}

function update_status(cat_id, status_id, cont) {

    var data = {id: cat_id, status_id: status_id};
    $.post('/admin/ajax-update-category', data, function(response) {
        response = jQuery.parseJSON(response);
        if (response.status == 'ok') {
            if (status_id==0) {
                $(cont).addClass('active btn-warning')
                       .attr('onclick',
                             'update_status("'+cat_id+'", "1", this)');
            } else {
                $(cont).removeClass('active btn-warning')
                       .attr('onclick',
                             'update_status("'+cat_id+'", "0", this)');
            }
        } else {
            $('#'+data['id']).text(response.messages);
        }
    });
}