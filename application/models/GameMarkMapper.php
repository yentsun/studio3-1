<?php

class Application_Model_GameMarkMapper extends Application_Model_Mapper {

    protected $_table_name = 'video';

    protected $_map = array(
        'id'=> 'id',
        'game_id'=> 'parentid',
        'team_id'=> 'teamid',
        'time_video'=> 'value_time',
        'time_protocol'=> 'value_time_p',
        'type'=> 'type',
        'title'=> 'ml_title_1',
        'description_line1'=> 'ml_m1_1',
        'description_line2'=> 'ml_m2_1',
        'player_id'=> 'playerid'
    );

    public function fetch_all($game_id) {

        $player_mapper = new Application_Model_PlayerMapper();
        $team_mapper = new Application_Model_TeamMapper();
        $select = $this->_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->from(array('mark'=>$this->_table_name))
            ->order($this->_map['time_protocol'])
            ->joinLeft(array('team'=>'team'),
                         'mark.'.$this->_map['team_id'].'= team.id',
                         array('team_id'=>'team.'
                               .$team_mapper->get_map_value('id'),
                               'team_logo'=>'team.'
                               .$team_mapper->get_map_value('logo'),
                               'team_title'=>'team.'
                                    .$team_mapper->get_map_value('title')))
            ->joinLeft(array('player'=>'player'),
                         'mark.'.$this->_map['player_id'].'= player.id',
                   array('player_name'=>'player.'
                                .$player_mapper->get_map_value('name'),
                         'player_photo'=>'player.'
                                .$player_mapper->get_map_value('photo')))
            ->joinLeft(array('pt'=>'player_team'),
                  'pt.parentid = mark.'.$this->_map['player_id'].' AND
                   pt.teamid = mark.'.$this->_map['team_id'],
                   array('player_number'=>'pt.number'))
        ;
        $where = $this->_gateway
                ->getAdapter()
                ->quoteInto('mark.'.$this->_map['game_id'].' = ?', $game_id);
        $select = $select->where($where);
        $rows = $this->_gateway->fetchAll($select);
        return $rows;
    }
}