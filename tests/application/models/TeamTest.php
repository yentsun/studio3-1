<?php

class TeamTest extends PHPUnit_Framework_TestCase {

    private $_mapper = NULL;
    private $_adapter = NULL;
    const OMSK_A_ID = 447;
    const OMSK_2_ID = 448;

    function setUp() {

        $this->_mapper = new Application_Model_TeamMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();
    }

    function tearDown() {

        $this->_adapter->rollBack();
    }

    public function test_create_team() {

        $team_data = array(
            'title'=> 'Омский Авангардт',
            'logo'=> 'om_av.gif',
            'sport_id'=> '1',
            'datetime'=> '2012',
            'city'=> 'Омск',
            'arena_title'=> 'Арена-Омск',
            'coach_name'=> 'Raimo Summanen (FIN)',
            'email'=> 'avangardsn@mail.ru',
            'website_url'=> 'http://www.hawk.ru',
            'type'=> 'k'
        );
        $new_team_id = Application_Model_Team::create($team_data);
        $this->assertInternalType('int', $new_team_id);
        $new_team = Application_Model_Team::fetch($new_team_id);
        $this->assertInstanceOf('Application_Model_Team', $new_team);
        $this->assertEquals('Омский Авангардт', $new_team->title);
        $this->assertEquals('om_av.gif', $new_team->logo);
        $this->assertEquals(1, $new_team->sport_id);
        $this->assertEquals('01.01.2012', $new_team->datetime());
        $this->assertEquals('2012', $new_team->datetime(Zend_Date::YEAR));
        $this->assertEquals('Raimo Summanen (FIN)', $new_team->coach_name);
    }

    public function test_update_team() {

        $data = array(
            'title'=> 'Омский Газмяс',
            'logo'=> 'om_av.gif',
            'sport_id'=> '1',
            'datetime'=> '2012',
            'category_id'=> CategoryTest::LEAGUE_ID,
            'city'=> 'Омск',
            'arena_title'=> 'Арена-Омск',
            'coach_name'=> 'Raimo Summanen (FIN)',
            'email'=> 'avangardsn@mail.ru',
            'website_url'=> 'http://www.hawk.ru',
            'type'=> 'k'
        );
        $updated_team = Application_Model_Team::update($data,
                                                       self::OMSK_A_ID);
        $this->assertInstanceOf('Application_Model_Team', $updated_team);
        $this->assertEquals('Омский Газмяс', $updated_team->title);
    }

    public function test_delete_team() {

        Application_Model_Team::delete(self::OMSK_A_ID);
        $deleted_team = Application_Model_Team::fetch(self::OMSK_A_ID);
        $this->assertNull($deleted_team);
    }

    public function test_fetch_team() {

        $omsk_a = Application_Model_Team::fetch(self::OMSK_A_ID);
        $this->assertInstanceOf('Application_Model_Team', $omsk_a);
        $this->assertEquals(CategoryTest::LEAGUE_ID, $omsk_a->category()->id);
        $this->assertEquals(CategoryTest::COUNTRY_ID,
                            $omsk_a->country()->id);
    }

    public function test_fetch_all() {

        $all_teams = Application_Model_Team::fetch_all(CategoryTest::HOCKEY_ID, null);
        $this->assertEquals(2, count($all_teams));
    }

    public function test_fetch_games() {

        $team = Application_Model_Team::fetch(self::OMSK_A_ID);
        $games = $team->fetch_games();
        $this->assertEquals(2, count($games));

        //with category set
        $games = $team->fetch_games(CategoryTest::LEAGUE_ID);
        $this->assertEquals(2, count($games));
    }
}