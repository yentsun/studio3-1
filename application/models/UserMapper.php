<?php

class Application_Model_UserMapper extends Application_Model_Mapper {

    protected $_table_name = 'user';
    protected $_map = array(
        'id'=> 'id',
        'email'=> 'login',
        'password_hash'=> 'password',
        'first_name'=> 'name',
        'last_name'=> 'family',
        'phone'=> 'phone',
        'locale'=> 'country',
        'registration_date'=> 'date',
        'club'=> 'club',
        'job_title'=> 'job',
        'agency'=> 'agency',
        'end_date'=> 'date_end',
        'last_login_time'=> 'date_login',
        'sport_id'=> 'type_sport',
        'status_id'=> 'show_it'
    );
}
