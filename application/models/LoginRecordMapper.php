<?php

class Application_Model_LoginRecordMapper extends Application_Model_Mapper {

    protected $_table_name = 'userip';

    protected $_map = array(
        'id'=> 'id',
        'ip'=> 'ip',
        'user_id'=> 'parentid',
        'start_datetime'=> 'start',
        'end_datetime'=> 'end'
    );
}