<?php

class Application_Model_GameMapper extends Application_Model_Mapper {

    protected $_table_name = 'match';


    protected $_map = array(
        'id'=> 'id',
        'datetime'=> 'date',
        'arena_title'=> 'ml_title_1',
        'team_one_id'=> 'clubid',
        'team_two_id'=> 'club2id',
        'category_id'=> 'parentid',
        'video_path'=> 'video',
        'video_hd'=> 'video_hd',
        'review_path'=> 'obzor',
        'number'=> 'number',
        'game_score'=> 'schet',
        'protocol_html'=> 'ml_protocol_1',
        'scoreboard_html'=> 'ml_protocolp_1',
        'video_time'=> 'datevideo'
    );

    public function fetch_all($team_id=null, array $cat_ids=null,
                              $offset=0, $limit=0, $player_id=null,
                              $line_id=null, $video_only=false,
                              $count_only=false, $user_id=null) {

        $category_mapper = new Application_Model_CategoryMapper();
        $team_mapper = new Application_Model_TeamMapper();
        $select = $this->_gateway
            ->select()
            ->setIntegrityCheck(false);
        if ($count_only)
            $select->from(array('game'=>$this->_table_name), 'COUNT(*) as num');
        else {
            $select
            ->from(array('game'=>$this->_table_name))
            ->limit($limit, $offset)
            ->join(array('cat'=>'main'),
                  'game.'.$this->_map['category_id'].'= cat.id',
                   array('category_id'=>'cat.id',
                         'category_title'=>'cat.'
                             .$category_mapper->get_map_value('title'),
                         'category_parent_id'=>'cat.'
                             .$category_mapper->get_map_value('parent_id')))
            ->join(array('team_one'=>'team'),
                  'game.'.$this->_map['team_one_id'].'= team_one.id',
                   array('team_one_id'=>'team_one.'
                             .$team_mapper->get_map_value('id'),
                         'team_one_title'=>'team_one.'
                             .$team_mapper->get_map_value('title')))
            ->join(array('team_two'=>'team'),
                  'game.'.$this->_map['team_two_id'].'= team_two.id',
                   array('team_two_id'=>'team_two.'
                             .$team_mapper->get_map_value('id'),
                         'team_two_title'=>'team_two.'
                             .$team_mapper->get_map_value('title')))
            ->order('game.'.$this->_map['datetime'].' DESC')
            ;
        }
        if ($team_id) {
            $select
                ->where($this->_map['team_one_id'].' = ? OR '
                       .$this->_map['team_two_id'].' = ?', $team_id);
        }
        if ($cat_ids) {
            $select
                ->where('game.'.$this->_map['category_id'].' IN (?)', $cat_ids);
        }

        if ($video_only) {
            $select
                ->where('game.'.$this->_map['video_path'].' <> ?', '');
        }

        if ($user_id) {
            $fav_mapper = new Application_Model_FavouriteMapper();
            $select
                ->join(array('fav'=>$fav_mapper->get_table_name()),
                             'game.'.$this->_map['id'].' = '
                            .'fav.'.$fav_mapper->get_map_value('object_id'),
                       array('id'=>'game.'.$this->_map['id'],
                             'fav_id'=>'fav.'.$fav_mapper->get_map_value('id')))
                ->where('fav.'.$fav_mapper->get_map_value('user_id').' = ?',
                        $user_id)
                ->where('fav.'.$fav_mapper->get_map_value('type').' = ?',
                         3)
            ;
        }

        // get player games with some stats
        if ($player_id && $line_id) {
            $stat_mapper = Application_Model_PlayerGameStat::get_mapper($line_id);
            $stat_fields = array();
            if (in_array($line_id, array(1,2)))
                $stat_fields = array(
                    'goals_authored'=>$stat_mapper->get_map_value('goals_authored'),
                    'penalty_time'=>$stat_mapper->get_map_value('penalty_time'),
                    'attempts'=>$stat_mapper->get_map_value('attempts'),
                    'field_time'=>$stat_mapper->get_map_value('field_time')
                );
            if ($line_id == 3)
                $stat_fields = array(
                    'score_missed'=>$stat_mapper->get_map_value('score_missed'),
                    'saves'=>$stat_mapper->get_map_value('saves'),
                    'field_time'=>$stat_mapper->get_map_value('field_time')
                );
            $select
                ->join(array('stat'=>$stat_mapper->get_table_name()),
                      'game.'.$this->_map['id'].' =
                       stat.'.$stat_mapper->get_map_value('game_id'),
                      $stat_fields
                )
                ->where('stat.'.$stat_mapper->get_map_value('player_id').' = ?',
                        $player_id)
            ;
        }
        return $count_only
            ? $this->_gateway->fetchRow($select)->num
            : $this->_gateway->fetchAll($select);
    }

    /**
     * Fetch game by team ids and the date
     * @param $team_one_id
     * @param $team_two_id
     * @param $date
     * @return null|Zend_Db_Table_Row
     */
    public function fetch_by_composite_key($team_one_id, $team_two_id, $date) {

        $select = $this->_gateway
            ->select()
            ->where($this->_map['team_one_id'].' = ?', $team_one_id)
            ->where($this->_map['team_two_id'].' = ?', $team_two_id)
            ->where('TO_DAYS('.$this->_map['datetime'].') = TO_DAYS(?)', $date)
        ;
        return $this->_gateway->fetchRow($select);
    }

    public function fetch_players($game_id) {

        $player_game_gateway = new Zend_Db_Table($this->_player_game_table_name);
        $player_mapper = new Application_Model_PlayerMapper();
        $select = $player_game_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_player_game_table_name)
            ->join(array('player'=>$player_mapper->get_table_name()),
                         $this->_player_game_table_name.'.playerid = player.'
                        .$player_mapper->get_map_value('id'))
            ->where($this->_player_game_table_name.'.parentid = ?', $game_id)
        ;
        return $player_game_gateway->fetchAll($select);
    }
}