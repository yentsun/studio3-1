<?php

class GameController extends Zend_Controller_Action
{

    public function init() {

        $this->id = $this->_getParam('id');
        $this->view->game = Application_Model_Game::fetch($this->id);
        $this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    public function indexAction() {


    }

    public function lineupsAction() {

    }

    public function videoAction() {

        $this->view->headScript()
            ->appendFile('http://vjs.zencdn.net/c/video.js')
            ->appendFile('/js/video.js')
        ;
        $this->view->headLink()
            ->appendStylesheet('http://vjs.zencdn.net/c/video-js.css');
    }

    public function streamAction() {

        $user_id = $this->_getParam('uid');
        $format = $this->_getParam('format');
        $ip = Application_Model_User::get_ip();
        $session = Application_Model_Session::fetch($user_id);
        if (!$session) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">
                     Вы не авторизовались!</div>');
            $this->_redirect('/');
        }
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $file_info = $this->view->game->get_videofile_info();
        if ($session->ip == $ip) {
            $this->getResponse()
                ->setHeader('Content-type', 'video/'.$format)
                ->setHeader('X-Accel-Redirect',
                            '/media'.$file_info['directory_path'].'/'
                                    .$file_info['name'].'.'.$format)
                ->setHeader('Content-length',
                            abs(filesize(APPLICATION_PATH.'/../data/media'
                                        .$file_info['directory_path'].'/'
                                        .$file_info['name'].'.'.$format)))
            ;
        }
    }

    public function downloadAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $path = APPLICATION_PATH.'/../data/media'.$this->view->game->video_path;
        $this->getResponse()
            ->setHeader('Content-Disposition',
                         sprintf('attachment; filename=%s.mp4',
                                 $this->view->game->id))
            ->setHeader('X-Accel-Redirect',
                        '/media'.$this->view->game->video_path)
            ->setHeader('Content-type', 'video/mp4')
            ->setHeader('Content-length',
                         filesize($path))
        ;
    }
}