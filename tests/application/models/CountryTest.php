<?php

class CountryTest extends PHPUnit_Framework_TestCase {

    private $_mapper = null;
    private $_adapter = null;
    const RUSSIA_ID = 1;

    function setUp() {

        $this->_mapper = new Application_Model_CountryMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();
    }

    function tearDown() {

        $this->_adapter->rollBack();
    }

    public function test_fetch_all() {

        $countries = Application_Model_Country::fetch_all();
        $this->assertEquals(36, count($countries));
    }
}