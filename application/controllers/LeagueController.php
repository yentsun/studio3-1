<?php

class LeagueController extends Zend_Controller_Action {

    public function init() {

        $this->id = $this->_getParam('id');
        $this->view->league = Application_Model_Category::fetch($this->id);
    }

    public function indexAction() {

        $active_season =
            $this->view->league->get_first_season($this->id);
        $ids =
            Application_Model_Team::fetch_all_by_category_id($active_season->id);
        $this->view->teams = !empty($ids) ?
            Application_Model_Team::fetch_all(null, null, $ids) :
            array();
    }

    public function gamesAction() {

        $this->view->headScript()
            ->appendFile('/js/jquery.dataTables.min.js')
            ->appendFile('/js/team-index.js')
        ;
        $this->view->games = Application_Model_Game::fetch_all(null, $this->id);
    }

    public function statsAction() {

        $this->view->headScript()
            ->appendFile('/js/jquery.dataTables.min.js')
            ->appendFile('/js/league-stats.js')
        ;
    }

    public function ajaxStatsAction() {

        $category_id = $this->_getParam('category_id');
        $line_id = $this->_getParam('line_id');
        if (!isset($category_id))
            $category_id = $this->id;

        $games = Application_Model_Game::fetch_all(null, $category_id);
        $game_ids = array_keys($games);
        $this->view->players =
            Application_Model_Player::fetch_all(null, null, null, null, true,
                                               $game_ids, $line_id, 'points',
                                               'DESC');
        $this->_helper->layout->disableLayout();
    }
}