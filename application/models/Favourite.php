<?php

class Application_Model_Favourite extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_FavouriteMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>true),
        'datetime'=> array(array('Date', Zend_Date::ISO_8601), 'allowEmpty'=>true),
        'type'=> 'Int',
        'user_id'=> array('Int', array('GreaterThan', 0)),
        'object_id'=> 'Int',
        'row'=> array('allow_empty'=>true)
    );

    public static function fetch_all($type, $user_id) {

        if ($type == 2)
            return Application_Model_Team::fetch_all(null, null, null, $user_id);
        if ($type == 1)
            return Application_Model_Player::fetch_all(null, null, true,
                                                      $user_id, true);
        if ($type == 3)
            return Application_Model_Game::fetch_all(null, null, null, null,
                                                     null, null, $user_id);
        return array();
    }

    public static function delete($id, $user_id) {

        $favourite = self::fetch($id);
        if ($favourite->user_id == $user_id) {
            $mapper = new Application_Model_FavouriteMapper();
            $rows = $mapper->delete('id', $id);
            if (!$rows)
                throw new Zend_Exception('Now rows were affected!');
            return true;
        }
        return false;
    }
}