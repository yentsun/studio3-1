<?php

class AdminControllerTest extends Zend_Test_PHPUnit_ControllerTestCase
{

    public function setUp()
    {
        $this->bootstrap = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH.'/configs/application.ini');
        parent::setUp();
        $this->getFrontController()->getRouter()->addDefaultRoutes();

        $this->_category_mapper = new Application_Model_CategoryMapper();
        $this->_game_mapper = new Application_Model_GameMapper();
        $this->_category_adapter = $this->_category_mapper
                                        ->get_gateway()
                                        ->getAdapter();
        $this->_game_adapter = $this->_game_mapper
                                        ->get_gateway()
                                        ->getAdapter();
        $this->_category_adapter->beginTransaction();
//        $this->_game_adapter->beginTransaction();


    }

    public function tearDown()
    {

        $this->_category_adapter->rollBack();
//        $this->_game_adapter->rollBack();
    }

    public function testDashboardAction()
    {
        $params = array('action' => 'dashboard', 'controller' => 'Admin', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public function testAddMatchesAction()
    {

        //admin login
        UserControllerTest::login($this);

        $this->resetRequest()->resetResponse();

        $params = array('action' => 'addGames', 'controller' => 'admin');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public function test_update_create_category()
    {
        $params = array('action'=>'update-category',
                        'controller' =>'Admin');
        $url = $this->url($params);

        //update category
        $this->request->setMethod('POST');
        $this->request->setPost(
            array(
                 'id'=> CategoryTest::LEAGUE_ID,
                 'title'=> 'new league title'
            )
        );
        $this->dispatch($url);
//        print_r($this->getResponse()->getBody());
        $this->assertNotController('error');
    }

    public function test_delete_category_action()
    {
        $params = array('action' => 'deleteCategory',
                        'controller' => 'Admin');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);

        //delete existent category
        $this->request->setMethod('POST');
        $this->request->setPost(
            array('id'=> CategoryTest::LEAGUE_ID)
        );
        $this->dispatch($url);
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);

        //delete non-existent category (must throw exception)
        $this->resetRequest()->resetResponse();
        $this->request->setMethod('POST');
        $this->request->setPost(
            array('id'=> '8987978978')
        );
        $this->dispatch($url);
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public function testUpdateGameAction()
    {
        $params = array('action' => 'updateGame', 'controller' => 'Admin',
                        'id' => GameTest::GAME_ID);
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->request->setMethod('POST');
        $this->request->setPost(array(
            'arena_title'=> 'Витязь-Арена'
        ));
        $this->dispatch($url);

        // assertions
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertRedirectTo($url);
    }

    public function test_create_game()
    {
        $params = array('action' => 'updateGame', 'controller' => 'Admin');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->request->setMethod('POST');
        $this->request->setPost(
            array(
                 'datetime'=> '17.09.2011',
                 'arena_title'=> 'Уфа-Арена',
                 'team_one_id'=> TeamTest::OMSK_A_ID,
                 'team_two_id'=> TeamTest::OMSK_2_ID,
                 'category_id'=> CategoryTest::LEAGUE_ID,
                 'number'=> '87',
                 'game_score'=> '1:3'
            ));
        $this->dispatch($url);

        // assertions
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertRedirectTo('/admin/dashboard');
    }

    public function test_create_game_wrong_data()
    {
        $params = array('action' => 'updateGame', 'controller' => 'Admin');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->request->setMethod('POST');
        $this->request->setPost(
            array(
                 'datetime'=> '17.09.2011',
                 'arena_title'=> 'Уфа-Арена',
                 'team_one_id'=> 'Авангард',
                 'team_two_id'=> TeamTest::OMSK_2_ID,
                 'category_id'=> CategoryTest::LEAGUE_ID,
                 'number'=> '87',
                 'game_score'=> '1:3'
            ));
        $this->dispatch($url);

        // assertions
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertQueryContentContains(
            'div.alert-error',
            'Форма заполнена неверно!'
        );
    }

    public function test_update_game_wrond_data()
    {
        $params = array('action' => 'updateGame',
                        'controller' => 'Admin',
                        'id' => GameTest::GAME_ID);
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->request->setMethod('POST');
        $this->request->setPost(array(
            'team_one_id'=> 'Команда А',
        ));
        $this->dispatch($url);

        // assertions
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertQueryContentContains(
            'div.alert-error',
            'Форма заполнена неверно!'
        );
    }

    public function testDeleteGameAction()
    {
        $params = array('action'=> 'deleteGame',
                        'controller'=> 'Admin',
                        'id'=> GameTest::GAME2_ID);
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertRedirectTo('/admin/dashboard');

        $deleted_game = Application_Model_Game::fetch(GameTest::GAME2_ID);
        $this->assertNull($deleted_game);

        $search = new Application_Model_Search('Application_Model_Game');
        $this->assertNull($search->fetch(GameTest::GAME2_ID));
    }

    public function testGamesByTeamAction()
    {
        $params = array('action'=> 'gamesByTeam',
                        'controller'=> 'Admin',
                        'team_id'=> TeamTest::OMSK_A_ID,
                        'category_id'=> CategoryTest::LEAGUE_ID);
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertContains('Омский Авангард',
        $this->getResponse()->getBody());
    }

    public function testUpdateTeamAction()
    {
        $params = array('action'=> 'updateTeam',
                        'controller'=> 'Admin',
                        'id'=> TeamTest::OMSK_A_ID);
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->request->setMethod('POST');
        $this->request->setPost(
            array(
                 'title'=> 'Команда А',
                 'logo'=> 'om_av.gif',
                 'sport_id'=> '1',
                 'datetime'=> '2012',
                 'city'=> 'Омск',
                 'arena_title'=> 'Арена-Омск',
                 'coach_name'=> 'Raimo Summanen (FIN)',
                 'email'=> 'avangardsn@mail.ru',
                 'website_url'=> 'http://www.hawk.ru',
                 'type'=> 'k',
                 'category_id'=> CategoryTest::LEAGUE_ID
            ));
        $this->dispatch($url);

        // assertions
        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $updated_team = Application_Model_Team::fetch(TeamTest::OMSK_A_ID);
        $this->assertEquals('Команда А', $updated_team->title);
        $this->assertEquals('КХОЛ', $updated_team->category()->title);
        $this->assertRedirectTo($url);
    }

    public function test_create_team()
    {
        $params = array('action' => 'updateTeam',
                        'controller' => 'Admin');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);

        $this->request->setMethod('POST');
        $this->request->setPost(
            array(
                 'title'=> 'Омский Авангардт',
                 'logo'=> 'om_av.gif',
                 'sport_id'=> '1',
                 'category_id'=> CategoryTest::LEAGUE_ID,
                 'datetime'=> '2012',
                 'city'=> 'Омск',
                 'arena_title'=> 'Арена-Омск',
                 'coach_name'=> 'Raimo Summanen (FIN)',
                 'email'=> 'avangardsn@mail.ru',
                 'website_url'=> 'http://www.hawk.ru',
                 'type'=> 'k'
            ));
        $this->dispatch($url);

        // assertions
//        print_r($this->getResponse()->getBody());
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertRedirectTo('/admin/dashboard');

        $this->assertEquals(3, count(Application_Model_Team::fetch_all(CategoryTest::HOCKEY_ID, null)));
    }

    public function testDeleteTeamAction()
    {
        $params = array('action'=> 'deleteTeam',
                        'controller'=> 'Admin',
                        'id'=> TeamTest::OMSK_2_ID);
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public function testGamesByCategoryAction()
    {
        $params = array('action' => 'gamesByCategory',
                        'controller' => 'Admin',
                        'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public function testTeamsToLeagueAction()
    {
        $params = array('action' => 'teamsToLeague', 'controller' => 'Admin',
                        'league_id' => CategoryTest::LEAGUE_ID);
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public function testPlayersAction()
    {
        $params = array('action' => 'players', 'controller' => 'Admin', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public function testDeletePlayerAction()
    {
        $params = array('action' => 'deletePlayer', 'controller' => 'Admin', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
    }

    public function testPlayerTransferAction()
    {
        $params = array('action' => 'playerTransfer', 'controller' => 'Admin', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertQueryContentContains(
            'div#view-content p',
            'View script for controller <b>' . $params['controller'] . '</b> and script/action name <b>' . $params['action'] . '</b>'
            );
    }

    public function testUpdateTransferAction()
    {
        $params = array('action' => 'updateTransfer', 'controller' => 'Admin', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertQueryContentContains(
            'div#view-content p',
            'View script for controller <b>' . $params['controller'] . '</b> and script/action name <b>' . $params['action'] . '</b>'
            );
    }

    public function testImportPlayersAction()
    {
        $params = array('action' => 'importPlayers', 'controller' => 'Admin', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertQueryContentContains(
            'div#view-content p',
            'View script for controller <b>' . $params['controller'] . '</b> and script/action name <b>' . $params['action'] . '</b>'
            );
    }

    public function testGameMarksAction()
    {
        $params = array('action' => 'gameMarks', 'controller' => 'Admin', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertQueryContentContains(
            'div#view-content p',
            'View script for controller <b>' . $params['controller'] . '</b> and script/action name <b>' . $params['action'] . '</b>'
            );
    }

    public function testUpdateMarkAction()
    {
        $params = array('action' => 'updateMark', 'controller' => 'Admin', 'module' => 'default');
        $urlParams = $this->urlizeOptions($params);
        $url = $this->url($urlParams);
        $this->dispatch($url);
        
        // assertions
        $this->assertModule($urlParams['module']);
        $this->assertController($urlParams['controller']);
        $this->assertAction($urlParams['action']);
        $this->assertQueryContentContains(
            'div#view-content p',
            'View script for controller <b>' . $params['controller'] . '</b> and script/action name <b>' . $params['action'] . '</b>'
            );
    }


}















