<?php

class Application_Model_PageMapper extends Application_Model_Mapper {

    protected $_table_name = 'page';

    protected $_map = array(
        'id'=> 'id',
        'slug'=> 'stitle',
        'title'=> 'ml_title_1',
        'body'=> 'ml_message_1'
    );
}