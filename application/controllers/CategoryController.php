<?php

class CategoryController extends Zend_Controller_Action
{

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {

        $id = $this->_getParam('id');
        $this->view->category = Application_Model_Category::fetch($id);
    }
}

