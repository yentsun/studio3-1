var timer;

$(function(){
    $('#player_search').keyup(function(){
        var val = $(this).val();
        if(timer) {
            clearTimeout(timer);
            timer = null;
        }
        timer = setTimeout(
        function(){
            if (val.length >= 3) {
                $.post('/admin/ajax-query-players/query/'+val, function(response){
                    $('#players tbody').html(response);
                    $('.pagination').hide();
                });
            }
        },
        500);
    });
});