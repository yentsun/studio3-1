<?php

class UserTest extends PHPUnit_Framework_TestCase {

    private $_mapper = NULL;
    private $_adapter = NULL;
    const KYLE_ID = 241;

    function setUp() {

        $this->_mapper = new Application_Model_UserMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();
    }

    function tearDown() {

        $this->_adapter->rollBack();
    }

    public function test_register_valid_user() {

        $applicant_data = array(
            'email'=> 'ike@example.com',
            'first_name'=> 'Ike',
            'last_name'=> 'Broflowski',
            'phone'=> '+79990000001',
            'locale'=> 'ru_RU',
            'club'=> 'AK Bars',
            'job_title'=> 'scout',
            'sport_id'=> '1'
        );
        $new_user = Application_Model_User::register($applicant_data);
        $this->assertInstanceOf('Application_Model_User', $new_user);
        $this->assertEquals('ike@example.com', $new_user->email);
        $this->assertEquals('+79990000001', $new_user->phone);
    }

    public function test_register_invalid_user() {

        $applicant_data = array(
            'email'=> 'Ṫ8L帝+1R0ṲP', //wrong email
            'first_name'=> 'Kyle',
            'last_name'=> '',        //empty last name
            'phone'=> '+79990000000',
            'locale'=> 'ru_RU',
            'club'=> 'AK Bars',
            'job_title'=> 'scout',
            'sport_id'=> '1'
        );

        $this->setExpectedException('Application_Exception_EntityNotValid');
        $new_user = Application_Model_User::register($applicant_data);
    }

    public function test_register_existent_user() {

        $applicant_data = array(
            'email'=> 'user1@acme.com', //assuming this user already exist
            'first_name'=> 'Kyle',
            'last_name'=> 'Broflowski',
            'phone'=> '+79990000000',
            'locale'=> 'ru_RU',
            'club'=> 'AK Bars',
            'job_title'=> 'scout',
            'sport_id'=> '1'
        );
        $this->setExpectedException('Application_Exception_EntityNotValid');
        $new_user = Application_Model_User::register($applicant_data);
    }

    public function test_update_user() {

        $data = array(
            'first_name'=> 'Joseph',
            'rubbish_prop'=> '345gf' //should be ignored
        );
        $updated_user = Application_Model_User::update($data, 241);
        $this->assertInstanceOf('Application_Model_User', $updated_user);
        $this->assertEquals('Joseph', $updated_user->first_name);
    }

    public function test_new_password() {

        $old_kyle = Application_Model_User::fetch(self::KYLE_ID);
        Application_Model_User::set_new_password(self::KYLE_ID);
        $new_kyle = Application_Model_User::fetch(self::KYLE_ID);
        $this->assertNotEquals($old_kyle->password_hash,
                               $new_kyle->password_hash);
    }

    public function test_get_status() {

        $kyle = Application_Model_User::fetch(self::KYLE_ID);
        $this->assertEquals('client', $kyle->get_status());
    }

    public function test_fetch_all() {

        $all = Application_Model_User::fetch_all();
        $this->assertEquals(2, count($all));
        foreach ($all as $user) {
            $this->assertInstanceOf('Application_Model_User', $user);
        }
    }
}