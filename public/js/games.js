var timer;

$(function(){
    $('#game_search').keyup(function(){
        var val = $(this).val();
        if(timer) {
            clearTimeout(timer);
            timer = null;
        }
        timer = setTimeout(
        function(){
            if (val.length >= 3) {
                $.post('/admin/ajax-query-games/query/'+val, function(response){
                    $('#games tbody').html(response);
                });
            }
        },
        500);
    });
});