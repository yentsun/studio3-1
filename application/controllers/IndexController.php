<?php

class IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {

        if ($this->view->user)
            $this->_forward('dashboard', 'user');
    }

    public function setLocaleAction() {

        $locale = $this->_getParam('locale');
        $uri = $this->getRequest()->getServer('HTTP_REFERER');
        $session = new Zend_Session_Namespace('visit');
        $session->locale = $locale;
        $this->_redirect($uri, array('prependBase'=>false));
    }
}