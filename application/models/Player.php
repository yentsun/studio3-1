<?php

class Application_Model_Player extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_PlayerMapper';
    const DATE_FORMAT = 'dd.MM.YYYY';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>true),
        'name'=> array(),
        'datetime'=> array(array('Date', 'YYYY-MM-dd'), 'allowEmpty'=>true),
        'height'=> array('Int', 'allowEmpty'=>true),
        'weight'=> array('Int', 'allowEmpty'=>true),
        'hand'=> array('Int', 'allowEmpty'=>true),
        'sport_id'=> 'Int',
        'country_id'=> 'Int',
        'line_id'=> array('Int', 'allowEmpty'=>true),
        'photo'=> array('allowEmpty'=>true),
        'promo_video'=> array('allowEmpty'=>true),
        'row'=> array('allowEmpty'=>true)
    );

    protected $_filters = array(
        'number'=> 'Int',
        'datetime'=> array(
            array('LocalizedToNormalized',
                  array('date_format'=> self::DATE_FORMAT)),
            array('NormalizedToLocalized',
                  array('date_format'=> Zend_Date::ISO_8601))
        )
    );

    protected static $_hand = array(
        'r'=> 1,
        'l'=> 2
    );

    public function age() {

        Zend_Date::setOptions(array('format_type'=>'php'));
        $today = new Zend_Date();
        $birth_date = new Zend_Date($this->datetime);
        $age = $today->toString('Y') - $birth_date->toString('Y');

        // Edit: Added julian date adjustments for leap years
        $adjust_julian = strcmp($today->toString('L'),
                               $birth_date->toString('L'));
        if ($birth_date->toString('m') > 2) {
            $birth_date->addDay($adjust_julian);
        }

        if ($today->toString('z') < $birth_date->toString('z')) {
            $age--;
        }
        Zend_Date::setOptions(array('format_type' => 'iso'));
        return $age;
    }

    public function get_hand_title() {

        $hand = array(
            1=> 'Правша',
            2=> 'Левша'
        );
        return $hand[$this->hand];
    }

    public static function fetch($id_or_name) {

        $mapper = new Application_Model_PlayerMapper();
        $row = $mapper->fetch_all(null, null, null, null, null, null,
                                  null, false, $id_or_name);
        return $row
                ? new self(array_merge($mapper->row_to_array($row)))
                : null;
    }

    public static function fetch_all($sport_id=null, $page=1, $save_row=true,
                                     $user_id=null, $stats=false,
                                     $games=array(), $line_id=null,
                                     $order_by=null, $dir=null) {

        $mapper = new Application_Model_PlayerMapper();

        $per_page = null;
        $offset = null;

        if ($page) {
            $per_page = Zend_Registry::get('settings')->pagination->per_page;
            $offset = $per_page * ($page - 1);
            $total_count = self::count_all($sport_id);

            //pagination
            $paginator = Zend_Paginator::factory((int) $total_count);
            $paginator->setPageRange(
                Zend_Registry::get('settings')->pagination->range);
            Zend_Paginator::setDefaultScrollingStyle('Elastic');
            Zend_View_Helper_PaginationControl::setDefaultViewPartial(
                'admin/_pagination.phtml');
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($per_page);
            self::$paginator = $paginator;
        }

        $rows = $mapper->fetch_all($sport_id, $offset, $per_page, $games, null,
                                   $order_by, $dir, $stats, null, null, $user_id,
                                   $line_id);
        $result_set = array();
        foreach ($rows as $row) {
            $player = new self($mapper->row_to_array($row));
            $result_set[$player->id] = $player;
        }
        return $result_set;
    }

    public static function fetch_all_by_team_id($team_id) {

        $mapper = new Application_Model_PlayerMapper();
        $rows = $mapper->fetch_all_by_team_id($team_id);
        $result_set = array();
        foreach ($rows as $row) {
            $player = new self($mapper->row_to_array($row));
            $player->row = $row;
            $result_set[$player->id] = $player;
        }
        return $result_set;
    }

    public function fetch_country() {

        return $this->country_id
            ? Application_Model_Country::fetch($this->country_id)
            : null;
    }

    public function fetch_line() {

        $mapper = new Application_Model_PlayerMapper();
        $rows = $mapper->fetch_all_lines($this->line_id);
        return $rows[0];
    }

    /**
     * Fetch player's current team based on his transfers. If there is a club
     * in current teams - return it first, if no - return any team instead
     * @return Application_Model_Team
     */
    public function fetch_current_team() {

        $transfers = $this->fetch_transfers(true);
        if (count($transfers) > 0) {
        foreach ($transfers as $transfer) {
            if ($transfer->row->team_type == 'k')
                return Application_Model_Team::fetch($transfer->team_id);
        }
            return Application_Model_Team::fetch($transfers[0]->team_id);
        } else
            return null;
    }

    public function fetch_transfers($current=false) {

        return Application_Model_Transfer::fetch_all($this->id, $current);
    }

    public function fetch_games($video_only=false) {

        return Application_Model_Game::fetch_all(null, null, null,
                                                $this->id, $this->line_id,
                                                 null, null, $video_only);
    }

    public static function count_all($sport_id) {

        $mapper = new Application_Model_PlayerMapper();
        return (int) $mapper->count_all($mapper->get_map_value('sport_id').'= ?',
                                        $sport_id);
    }

    public static function fetch_lines() {

        $mapper = new Application_Model_PlayerMapper();
        $rows = $mapper->fetch_all_lines();
        return $rows;
    }

    public function to_search_doc($id=null) {

        $id = $id ? $id : $this->id;
        $doc = new Zend_Search_Lucene_Document();
        $doc->addField(Zend_Search_Lucene_Field::Keyword('id', $id));
        $doc->addField(Zend_Search_Lucene_Field::text('name', $this->name,
                                                      'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::unIndexed('photo',
                $this->photo));
        $doc->addField(Zend_Search_Lucene_Field::unIndexed('country_image_file',
                                    $this->row->country_image_file));
        $doc->addField(Zend_Search_Lucene_Field::unIndexed('country_title',
                                    $this->row->country_title));
        $doc->addField(Zend_Search_Lucene_Field
            ::text('line_title', $this->fetch_line()->ml_title_1, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field
            ::text('team_title', $this->row->team_title, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field
            ::unIndexed('team_id', $this->row->team_id, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field
            ::unIndexed('team_logo', $this->row->team_logo, 'UTF-8'));
        return $doc;
    }

    public static function process_xls($file_path, $sport_id) {

        require_once('Excel/reader.php');
        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('utf-8');
        $data->read($file_path);

        $result_set = array();

        foreach ($data->sheets[0]['cells'] as $no=>$row) {
            if ($no != 1) {
                $country_id = Application_Model_Country::fetch(
                    null,
                    $row[11])->id;
                $mapper = new Application_Model_PlayerMapper();
                $line_id = $mapper->fetch_line_id($row[17]);
                $date = new Zend_Date($row[3], 'dd/MM/YYYY');
                $player_data = array(
                    'name'=> $row[1],
                    'datetime'=> $date->toString(self::DATE_FORMAT),
                    'height'=> $row[8],
                    'weight'=> $row[9],
                    'hand'=> self::$_hand[strtolower($row[10])],
                    'sport_id'=> $sport_id,
                    'country_id'=> $country_id,
                    'number'=> $row[7],
                    'line_id'=> $line_id
                );

                //create player
                $new_player_id = self::create($player_data, false);

                //add initial transfer record
                $entry_date = new Zend_Date($row[5], 'dd/MM/YYYY');
                $team = Application_Model_Team::fetch(null, $row[6]);
                if ($team) {
                    Application_Model_Transfer::create(
                        array(
                             'player_id'=> $new_player_id,
                             'team_id'=> 'Int',
                             'number'=> $row[7],
                             'datetime'=> $entry_date->toString(
                                 self::DATE_FORMAT),
                        ));
                }
                Application_Model_Player::update($player_data,
                    $new_player_id);

                $result_set[$new_player_id] = self::fetch($new_player_id);
            }
        }

        return $result_set;
    }

    public function get_promo_file_info() {

        $result = array();
        $path_pieces = explode('/', $this->promo_video);
        $file_pieces = explode('.', array_pop($path_pieces));
        $result['directory_path'] = implode('/', $path_pieces);
        $result['ext'] = array_pop($file_pieces);
        $result['name'] = implode('.', $file_pieces);
        return $result;
    }
}