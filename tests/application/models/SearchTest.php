<?php

class SearchTest extends PHPUnit_Framework_TestCase {

    protected $_search = null;
    protected $_games_count = null;


    function setUp() {

        //add all games to index
        $this->_search = new Application_Model_Search('Application_Model_Game');
        $games = Application_Model_Game::fetch_all();
        $this->_games_count = count($games);
        foreach ($games as $game) {
            $this->_search->save_document($game->to_search_doc());
        }
    }

    function tearDown() {

        $this->_search->delete_index();
    }

    public function test_initial_index() {

        $this->assertEquals($this->_games_count,
                            $this->_search->get_index()->count());
    }

    public function test_add_document() {


    }

    public function test_delete_from_index() {

        $this->_search->delete(GameTest::GAME_ID);
        $this->assertEquals($this->_games_count-1,
                            $this->_search->get_index()->numDocs());
        $this->assertNull($this->_search->fetch(GameTest::GAME_ID));
        $this->assertNotNull($this->_search->fetch(GameTest::GAME2_ID));
    }

    public function test_query_by_date() {

        $hits = $this->_search->perform_search('16.09.2011');
        $this->assertEquals($hits[0]->getDocument()->id, GameTest::GAME2_ID);
    }

    public function test_query_by_team_title_no_quotes() {

        $hits = $this->_search->perform_search('Омский');
        $this->assertEquals(2, count($hits));
    }

    public function test_query_by_league_title_and_date() {

        $hits = $this->_search->perform_search('КХОЛ 16.09.2011');
        $this->assertEquals($hits[0]->getDocument()->id, GameTest::GAME2_ID);
    }
}