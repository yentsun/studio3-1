var FORM_ELEMENTS = $('form label, form input, form div, form select');

$(function(){
    if ($('textarea.tinymce').length) {
        $('textarea.tinymce').tinymce({

            // Location of TinyMCE script
            script_url : '/js/tiny_mce/tiny_mce.js',
            language : 'ru',

            // General options
            theme : "advanced",
            plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,fullscreen",
//        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,

            content_css : "/css/main.css"
        });
    }
    if (typeof(required) !== 'undefined') {
        FORM_ELEMENTS.each(function(){
            var name = $(this).attr('name');
            if (jQuery.inArray(name, required) !== -1) {
                $(this).addClass('required');
                $('label[for="'+name+'"]')
                    .not(':has(span.req)')
                    .append('<span class="req"> *</span>');
            }
        });
    }
    if (typeof(error_messages) !== 'undefined') {
        var repop_field_names = Object.keys(original_data);
        for (var key in repop_field_names) {
            var field_name = repop_field_names[key];
            $('[name='+field_name+']').val(original_data[field_name]);
        }
        var error_field_names = Object.keys(error_messages);
        for (var key in error_field_names) {
            var field_name = error_field_names[key];
            var field_in_question = $('[name='+field_name+']');
            field_in_question.addClass('error');
            var message = '';
            for (var message_type in error_messages[field_name]) {
                message = message + error_messages[field_name][message_type]+' ';
            }
            field_in_question
                .attr('title', message)
                .tooltip({placement:'right'});
        }
    }
    if ($('.chosen').length)
        $('.chosen').chosen({no_results_text: 'результатов не найдено'});
});

function confirm_delete(url) {

    var conf = confirm('Действительно удалить?');
    if (conf)
        window.location.href = url;
}