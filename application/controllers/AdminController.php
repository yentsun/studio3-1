<?php

class AdminController extends Zend_Controller_Action {

    public function init() {

        $this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
    }

    public function dashboardAction() {

        $this->view->headScript()
            ->appendFile('/js/bootstrap-collapse.js')
            ->appendFile('/js/jquery.jeditable.mini.js')
            ->appendFile('/js/jquery-ui-1.8.21.custom.min.js')
            ->appendFile('/js/jquery.cookie.js')
            ->appendFile('/js/dashboard.js');
        $this->view->tree = Application_Model_Category::get_tree(0);
    }

    public function addGamesAction() {

        $this->view->parent_id = $this->_getParam('parent_id');
        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/form_common.js')
        ;

        $this->view->tree = Application_Model_Category::get_tree(0);

        if ($this->_request->isPost()) {
            $upload = new Zend_File_Transfer_Adapter_Http();
            $new_file_name = time();
            $csv_path = APPLICATION_PATH.'/../data/csv';
            $upload->addFilter('Rename', $csv_path.'/'.$new_file_name);
            $upload->addValidator('Extension', false, array('csv', 'txt'));
            if ($upload->isValid()) {
                $upload->receive();
                $data = $this->_request->getPost();
                try {
                $games = Application_Model_Game::process_csv(
                    $csv_path.'/'.$new_file_name, $data['parent_id']);
                    if (count($games['ignored'])) {
                        foreach ($games['ignored'] as $id=>$game)
                            $games['ignored'][$id] = sprintf(
                                '%s %s - %s - %s', $game->datetime(),
                                $game->team_one()->title, $game->game_score,
                                $game->team_two()->title
                            );
                        $ignored_list = implode('<br>', $games['ignored']);
                    } else
                        $ignored_list = '';
                    $this->flashMessenger->addMessage(sprintf(
                        '<div class="alert alert-success">
                            Добавлено %d матчей<br>
                            Проигнорировано %d матчей<br>%s
                         </div>', count($games['added']),
                                  count($games['ignored']), $ignored_list));
                    $this->_redirect('/admin/dashboard');
                } catch (Application_Exception_TeamNotFound $e) {
                    $this->flashMessenger->addMessage(sprintf(
                        '<div class="alert alert-error">
                            Найдены матчи с участием отсутствующих в базе
                            команд (<strong>%s</strong>)
                        </div>', $e->getMessage()));
                } catch (Application_Exception_EntityNotValid $e) {
                    $messages = array();
                    foreach ($e->messages as $array)
                        $messages[] = implode(' ', $array);
                    $this->flashMessenger->addMessage(sprintf(
                            '<div class="alert alert-error">
                            Неверные данные (<strong>%s</strong>)
                        </div>', implode(' ', $messages)));
                }
            } else {
                $errors = $upload->getErrors();
                $this->flashMessenger->addMessage(sprintf(
                    '<div class="alert alert-error">
                        Ошибка при загрузке файла (%s)
                    </div>', implode(' ', $errors)));
            }
        }
    }

    public function updateCategoryAction() {

        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/form_common.js')
        ;

        $id = $this->_getParam('id');
        $parent_id = $this->_getParam('parent_id');
        $this->view->category = Application_Model_Category::fetch($id);
        $this->view->parent_id = $parent_id;
        $this->view->tree = Application_Model_Category::get_tree(0);

        if ($this->_request->isPost()) {

            $data = $this->_request->getPost();
            $upload = new Zend_File_Transfer_Adapter_Http();
            $new_file_name = time();
            $image_path = APPLICATION_PATH.'/../public/img/main_pic';
            $upload->addFilter('Rename', $image_path.'/p144_'.$new_file_name);
            $upload->addValidator('IsImage', false);
            try {
                if ($upload->isUploaded()) {
                    if ($upload->isValid()) {
                        $upload->receive();
                        $data['image_file'] = strval($new_file_name);
                    } else {
                        $errors = implode(' ', $upload->getErrors());
                        throw new Whyte_Exception_EntityNotValid(
                            "Ошибка при загрузке изображения ($errors)");
                    }
                }
                if ($id) {
                    $updated_category = Application_Model_Category::update($data, $id);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Соревнование "'.$updated_category ->title.'" обновлено</div>');
                    $this->_redirect('/admin/dashboard');
                } else {
                    Application_Model_Category::create($data);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Соревнование "'.$data['title'].'" создано</div>');
                    $this->_redirect('/admin/dashboard');
                }
            } catch (Application_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                        Форма заполнена неверно! '
                        .$e->getMessage()
                        .'</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
            }
        }
    }

    public function updatePlayerAction() {

        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/tiny_mce/jquery.tinymce.js')
            ->appendFile('/js/form_common.js')
        ;
        $this->view->required = Application_Model_Player::get_required();
        $id = $this->_getParam('id');
        if ($id) {
            $this->view->player = Application_Model_Player::fetch($id);
            $sport_id = $this->view->player->sport_id;
        } else {
            $sport_id = $this->_getParam('sport_id');
        }
        $this->view->sport = Application_Model_Category::fetch($sport_id);
        $this->view->countries = Application_Model_Country::fetch_all();
        $this->view->lines = Application_Model_Player::fetch_lines();

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            $upload = new Zend_File_Transfer_Adapter_Http();
            $new_file_name = time();
            $image_path = APPLICATION_PATH.'/../public/img/player_pic';
            $upload->addFilter('Rename', $image_path.'/p144_'.$new_file_name);
            $upload->addValidator('IsImage', false);
            try {
                if ($upload->isUploaded()) {
                    if ($upload->isValid()) {
                        $upload->receive();
                        $data['photo'] = strval($new_file_name);
                    } else {
                        $errors = implode(' ', $upload->getErrors());
                        throw new Whyte_Exception_EntityNotValid(
                            "Ошибка при загрузке изображения ($errors)");
                    }
                }
                if ($id) {
                    $updated_player =
                        Application_Model_Player::update($data, $id, true, false);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Игрок "'.$updated_player->name.'" обновлен</div>');
                    $this->_redirect($this->view->url());
                } else {
                    Application_Model_Player::create($data);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Игрок "'.$data['name'].'" создан</div>');
                    $this->_redirect('/admin/players/sport_id/'.$sport_id);
                }
            } catch (Whyte_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                        Форма заполнена неверно! '
                        .$e->getMessage()
                        .'</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
            }
        }
    }

    public function deleteCategoryAction() {

        $id = $this->_getParam('id');
        try {
            Application_Model_Category::delete($id);
            $this->flashMessenger->addMessage(
                '<div class="alert alert-success">
                Соревнование удалено</div>');
            $this->_redirect('/admin/dashboard');
        } catch (Exception $e) {
        }
    }

    public function recreateIndexAction() {

        $game_search = new Application_Model_Search('Application_Model_Game',
                                                     true);
        $all_games = Application_Model_Game::fetch_all();

        $team_search = new Application_Model_Search('Application_Model_Team',
                                                     true);
        $all_teams = Application_Model_Team::fetch_all();

        $player_search = new Application_Model_Search('Application_Model_Player',
                                                     true);
        $all_players = Application_Model_Player::fetch_all(null, 0);

        try {
            //add games
            foreach ($all_games as $game) {
                $game_search->save_document($game->to_search_doc(), false);
            }
            $this->flashMessenger->addMessage(sprintf(
                                            '<div class="alert alert-success">
                                            В индексе %d матчей</div>',
                                              $game_search->get_index()->count()));

            //add teams
            foreach ($all_teams as $team) {
                $team_search->save_document($team->to_search_doc(), false);
            }
            $this->flashMessenger->addMessage(sprintf(
                                            '<div class="alert alert-success">
                                            В индексе %d команд</div>',
                                              $team_search->get_index()->count()));

            //add players
            foreach ($all_players as $player) {
                $player_search->save_document($player->to_search_doc(), false);
            }
            $this->flashMessenger->addMessage(sprintf(
                    '<div class="alert alert-success">
                                            В индексе %d игроков</div>',
                    $player_search->get_index()->count()));

            $this->_redirect('/admin/dashboard');
        } catch (Exception $e) {
            $this->flashMessenger->addMessage(sprintf(
                '<div class="alert alert-error">
                        Ошибка во время индексирования: %s</div>', $e->getMessage()));
        }
    }

    public function ajaxQueryGamesAction() {

        $query = $this->_getParam('query');
        $this->view->games = Application_Model_Game::search($query);
        $this->_helper->layout->disableLayout();
    }

    public function ajaxQueryTeamsAction() {

        $query = $this->_getParam('query');
        $this->view->teams = Application_Model_Team::search($query);
        $this->_helper->layout->disableLayout();
    }

    public function ajaxQueryPlayersAction() {

        $query = $this->_getParam('query');
        $this->view->players = Application_Model_Player::search($query);
        $this->_helper->layout->disableLayout();
    }

    public function updateGameAction() {
        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/tiny_mce/jquery.tinymce.js')
            ->appendFile('/js/form_common.js')
        ;
        $this->view->required = Application_Model_Game::get_required();
        $id = $this->_getParam('id');
        if ($id) {
            $this->view->game = Application_Model_Game::fetch($id);
            $category_id = $this->view->game->category_id;
        } else {
            $category_id = $this->_getParam('category_id');
        }
        $this->view->category_id = $category_id;
        $this->view->tree = Application_Model_Category::get_tree(0);
        $category = Application_Model_Category::fetch($category_id);
        $this->view->teams = Application_Model_Team
                       ::fetch_all($category->get_parent_by_type('sport')->id, null);

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            try {
                if ($id) {
                    $updated_game = Application_Model_Game::update($data, $id);
                    $this->flashMessenger->addMessage(
                    '<div class="alert alert-success">
                        Матч #'.$updated_game->number.' обновлен</div>');
                    $this->_redirect($this->view->url());
                } else {
                    $new_id = Application_Model_Game::create($data);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Матч '.$new_id.' создан</div>');
                    $this->_redirect('/admin/dashboard');
                }
            } catch (Application_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                        Форма заполнена неверно!</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
            }
        }
    }

    public function deleteGameAction() {

        $id = $this->_getParam('id');
        try {
            Application_Model_Game::delete($id);
            $this->flashMessenger->addMessage(
                '<div class="alert alert-warning">
                        Матч '.$id.' удален</div>');
        } catch (Exception $e) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">
                        Ошибка удаления матча '.$id.'</div>');
        }
        $this->_redirect('/admin/dashboard');
    }

    public function gamesByTeamAction() {

        $team_id = $this->_getParam('team_id');
        $category_id = $this->_getParam('category_id');
        $this->view->team = Application_Model_Team::fetch($team_id);
        $this->view->category = Application_Model_Category::fetch($category_id);
    }

    public function updateTeamAction() {

        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/tiny_mce/jquery.tinymce.js')
            ->appendFile('/js/form_common.js')
        ;
        $this->view->required = Application_Model_Team::get_required();
        $id = $this->_getParam('id');
        if ($id) {
            $this->view->team = Application_Model_Team::fetch($id);
            $sport_id = $this->view->team->sport_id;
        } else {
            $sport_id = $this->_getParam('sport_id');
            $this->view->sport =
                Application_Model_Category::fetch($sport_id);
        }
        $this->view->tree =
            Application_Model_Category::get_tree($sport_id);
        $this->view->countries = Application_Model_Country::fetch_all();

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            $upload = new Zend_File_Transfer_Adapter_Http();
            $new_file_name = time();
            $image_path = APPLICATION_PATH.'/../public/img/team_pic';
            $upload->addFilter('Rename', $image_path.'/p144_'.$new_file_name);
            $upload->addValidator('IsImage', false);
            try {
                if ($upload->isUploaded()) {
                    if ($upload->isValid()) {
                        $upload->receive();
                        $data['logo'] = strval($new_file_name);
                    } else {
                        $errors = implode(' ', $upload->getErrors());
                        throw new Whyte_Exception_EntityNotValid(
                            "Ошибка при загрузке изображения ($errors)");
                    }
                }
                if ($id) {
                    $updated_team = Application_Model_Team::update($data, $id);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Команда "'.$updated_team->title.'" обновлена</div>');
                    $this->_redirect($this->view->url());
                } else {
                    Application_Model_Team::create($data);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Команда "'.$data['title'].'" создана</div>');
                    $this->_redirect('/admin/dashboard');
                }
            } catch (Whyte_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                        Форма заполнена неверно! '
                       .$e->getMessage()
                       .'</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
            }
        }
    }

    public function deleteTeamAction() {

        $id = $this->_getParam('id');
        try {
            Application_Model_Team::delete($id);
            $this->flashMessenger->addMessage(
                '<div class="alert alert-warning">
                        Команда '.$id.' удалена</div>');
        } catch (Exception $e) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">
                        Ошибка удаления команды '.$id.'</div>');
        }
        $this->_redirect('/admin/dashboard');
    }

    public function teamsAction() {

        $this->view->headScript()
            ->appendFile('/js/teams.js')
        ;

        $sport_id = $this->_getParam('sport_id');
        $page = $this->_getParam('page') ? $this->_getParam('page') : 1;
        $this->view->teams = Application_Model_Team::fetch_all($sport_id, $page);
        $this->view->countries = Application_Model_Country::fetch_all();
        $this->view->category = Application_Model_Category::fetch($sport_id);
        $this->view->paginator = Application_Model_Team::$paginator;
    }

    public function gamesByCategoryAction() {
        $this->view->headScript()
            ->appendFile('/js/games.js')
        ;

        $id = $this->_getParam('id');
        $page = $this->_getParam('page') ? $this->_getParam('page') : 1;
        $this->view->games = Application_Model_Game::fetch_all(null, $id, $page);
        $this->view->category = Application_Model_Category::fetch($id);
        $this->view->paginator = Application_Model_Game::$paginator;
    }

    public function ajaxUpdateCategoryAction() {

        if ($this->_request->isPost()) {

            $data = $this->_request->getPost();
            $new_category = null;

            if (isset($data['id_array'])) {
                foreach ($data['id_array'] as $order=>$id) {
                    Application_Model_Category
                    ::update(array('order_id'=>$order), $id);
                }
                $this->view->response = json_encode(array('status'=> 'ok'));
            } else {
                if (isset($data['id']) && $data['id'] == 0) {
                    // create new
                    unset($data['id']);
                    $new_id = Application_Model_Category::create($data);
                    if (is_int($new_id)) {
                        $new_category = Application_Model_Category::fetch($new_id);
                    }
                } elseif (isset($data['id'])) {
                    //update existing
                    $new_category = Application_Model_Category::update($data,
                        $data['id']);
                }
                if ($new_category instanceof Application_Model_Category)
                    $this->view->response = json_encode(
                        array(
                             'status'=> 'ok',
                             'category'=> $new_category->to_array()
                        ));
                else
                    $this->view->response = json_encode(
                        array('status'=>'error',
                              'messages'=>$new_category));
            }
        }
        $this->_helper->layout->disableLayout();
    }

    public function teamsToCategoryAction() {

        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/form_common.js')
        ;
        $category_id = $this->_getParam('id');
        $category = Application_Model_Category::fetch($category_id);
        $this->view->selected_ids = Application_Model_Team
                            ::fetch_all_by_category_id($category_id);
        $this->view->teams = Application_Model_Team
                            ::fetch_all($category
                                            ->get_parent_by_type('sport')
                                            ->id, null);
        $this->view->category = $category;

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            if (isset($data['team_ids'])) {
                try {
                    Application_Model_Team::update_category_records(
                        $data['team_ids'], $category_id);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                            Команды сезона "'.$category->title.'" обновлены</div>');
                } catch (Exception $e) {
                     $this->flashMessenger->addMessage(
                        '<div class="alert alert-error">
                            Произошла ошибка ('.$e->getMessage().')</div>');
                }
            }
            $this->_redirect($this->view->url());
        }
    }

    public function playersAction() {
        $this->view->headScript()
            ->appendFile('/js/players.js')
        ;

        $sport_id = $this->_getParam('sport_id');
        $page = $this->_getParam('page') ? $this->_getParam('page') : 1;
        $this->view->players = Application_Model_Player::fetch_all($sport_id, $page);
        $this->view->sport = Application_Model_Category::fetch($sport_id);
        $this->view->paginator = Application_Model_Team::$paginator;
    }

    public function deletePlayerAction() {
        $id = $this->_getParam('id');
        try {
            Application_Model_Player::delete($id);
            $this->flashMessenger->addMessage(
                '<div class="alert alert-warning">
                        Игрок '.$id.' удален</div>');
        } catch (Exception $e) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">
                        Ошибка удаления игрока '.$id.'</div>');
        }
        $this->_redirect('/admin/dashboard');
    }

    public function playerTransferAction() {

        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/form_common.js');

        $id = $this->_getParam('id');
        $this->view->player = Application_Model_Player::fetch($id, true);
    }

    public function updateTransferAction() {

        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/form_common.js')
        ;
        $this->view->required = Application_Model_Team::get_required();
        $id = $this->_getParam('id');
        $player_id = $this->_getParam('player_id');
        $this->view->player = Application_Model_Player::fetch($player_id, true);
        $this->view->teams = Application_Model_Team
                                 ::fetch_all($this->view->player->sport_id, null);
        $this->view->transfer = $id
            ? Application_Model_Transfer::fetch($id)
            : Application_Model_Transfer::dummy();

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            $data['player_id'] = $player_id;
            try {
                if ($id) {
                    $updated_transfer = Application_Model_Transfer
                                            ::update($data, $id);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Трансфер #'.$updated_transfer->id.' обновлен</div>');
                    $this->_redirect('/admin/player-transfer/id/'
                                     .$player_id);
                } else {
                    $new_id = Application_Model_Transfer::create($data);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Трансфер '.$new_id.' создан</div>');
                    $this->_redirect('/admin/player-transfer/id/'
                                     .$player_id);
                }
            } catch (Whyte_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                        Форма заполнена неверно!</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
            }
        }
    }

    public function deleteTransferAction() {
        $id = $this->_getParam('id');
        $player_id = $this->_getParam('player_id');
        try {
            Application_Model_Transfer::delete($id);
            $this->flashMessenger->addMessage(
                '<div class="alert alert-warning">
                        Трансфер '.$id.' удален</div>');
        } catch (Exception $e) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">
                        Ошибка удаления трансфера '.$id.'</div>');
        }
        $this->_redirect('/admin/player-transfer/id/'
                         .$player_id);
    }

    public function importPlayersAction() {

        $this->view->sport_id = $this->_getParam('sport_id');

        if ($this->_request->isPost()) {
            $upload = new Zend_File_Transfer_Adapter_Http();
            $new_file_name = time();
            $xls_path = APPLICATION_PATH.'/../data/xls';
            $upload->addFilter('Rename', $xls_path.'/'.$new_file_name);
            $upload->addValidator('Extension', false, array('xls'));
            if ($upload->isValid()) {
                $upload->receive();
                $data = $this->_request->getPost();
                try {
                    $players = Application_Model_Player::process_xls(
                        $xls_path.'/'.$new_file_name, $this->view->sport_id);
                    $this->flashMessenger->addMessage(sprintf(
                            '<div class="alert alert-success">
                            Добавлено %d игроков
                         </div>', count($players)));
                    $this->_redirect('/admin/dashboard');
                } catch (Application_Exception_EntityNotValid $e) {
                    $messages = array();
                    foreach ($e->messages as $array)
                        $messages[] = implode(' ', $array);
                    $this->flashMessenger->addMessage(sprintf(
                            '<div class="alert alert-error">
                            Неверные данные (<strong>%s</strong>)
                        </div>', implode(' ', $messages)));
                }
            } else {
                $errors = $upload->getErrors();
                $this->flashMessenger->addMessage(sprintf(
                        '<div class="alert alert-error">
                        Ошибка при загрузке файла (%s)
                    </div>', implode(' ', $errors)));
            }
        }
    }

    public function gameMarksAction() {

        $game_id = $this->_getParam('id');
        $this->view->game = Application_Model_Game::fetch($game_id);
    }

    public function updateMarkAction() {

        $this->view->headScript()
            ->appendFile('/js/form_common.js')
        ;

        $id = $this->_getParam('id');
        if ($id) {
            $mark = Application_Model_GameMark::fetch($id);
            $game_id = $mark->game_id;
        } else {
            $mark = Application_Model_GameMark::dummy();
            $game_id = $this->_getParam('game_id');
        }
        $this->view->mark = $mark;
        $this->view->game = Application_Model_Game::fetch($game_id);
        $this->view->required = Application_Model_GameMark::get_required();

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            $data['game_id'] = $game_id;
            try {
                if ($id) {
                    $updated_mark = Application_Model_GameMark
                                    ::update($data, $id);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Метка "'.$updated_mark->title.'" обновлена</div>');
                } else {
                    Application_Model_GameMark::create($data);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Метка "'.$data['title'].' '
                                .$data['time_protocol'].'" создана</div>');
                }
                $this->_redirect('/admin/game-marks/id/'
                        .$game_id);
            } catch (Application_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                        Форма заполнена неверно!</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
            }
        }
    }

    public function deleteGameMarkAction() {

        $id = $this->_getParam('id');
        $game_id = $this->_getParam('game_id');

        try {
            Application_Model_GameMark::delete($id);
            $this->flashMessenger->addMessage(
                '<div class="alert alert-warning">
                        Метка '.$id.' удалена</div>');
        } catch (Exception $e) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">
                        Ошибка удаления метки '.$id.'</div>');
        }
        $this->_redirect('/admin/game-marks/id/'.$game_id);
    }

    public function lineupAction() {

        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/form_common.js')
        ;
        $game_id = $this->_getParam('id');
        $game = Application_Model_Game::fetch($game_id);
        $this->view->game = $game;
        $this->view->players = Application_Model_Player
               ::fetch_all($game->category()->get_parent_by_type('sport')->id,
                           null);

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            if (isset($data['player_ids'])) {
                try {
                    $game->update_roster($data['player_ids']);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                            Составы матча "'.$game->number.'" обновлены</div>');
                } catch (Exception $e) {
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-error">
                            Произошла ошибка ('.$e->getMessage().')</div>');
                }
            }
            $this->_redirect($this->view->url());
        }
    }

    public function playerStatsAction() {

        $game_id = $this->_getParam('id');
        $game = Application_Model_Game::fetch($game_id);
        $this->view->game = $game;
    }

    public function updateStatAction() {

        $this->view->headScript()
            ->appendFile('/js/form_common.js')
        ;

        $id = $this->_getParam('id');
        $player_line = $this->_getParam('player_line');
        if ($id) {
            $stat = Application_Model_PlayerGameStat::fetch($id, $player_line);
            $game_id = $stat->game_id;
        } else {
            $stat = Application_Model_PlayerGameStat::dummy();
            $game_id = $this->_getParam('game_id');
        }
        $this->view->stat = $stat;
        $this->view->player_line = $player_line;
        $this->view->game = Application_Model_Game::fetch($game_id);
        $this->view->required = Application_Model_PlayerGameStat
                                ::get_required();

        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            try {
                if ($id) {
                    Application_Model_PlayerGameStat
                                            ::update($data, $id, $player_line);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Запись обновлена</div>');
                } else {
                    Application_Model_PlayerGameStat
                                                 ::create($data, $player_line);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Запись создана</div>');
                }
                $this->_redirect('/admin/player-stats/id/'
                        .$game_id);
            } catch (Application_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                        Форма заполнена неверно!</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
            }
        }

    }

    public function deleteStatAction() {

        $id = $this->_getParam('id');
        $player_line = $this->_getParam('player_line');
        $game_id = $this->_getParam('game_id');

        try {
            Application_Model_PlayerGameStat::delete($id, $player_line);
            $this->flashMessenger->addMessage(
                '<div class="alert alert-warning">Запись удалена</div>');
        } catch (Exception $e) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">Ошибка удаления записи</div>');
        }
        $this->_redirect('/admin/player-stats/id/'.$game_id);
    }

    public function importGameStatsAction() {

        if ($this->_request->isPost()) {
            $upload = new Zend_File_Transfer_Adapter_Http();
            $new_file_name = time();
            $xls_path = APPLICATION_PATH.'/../data/xls';
            $upload->addFilter('Rename', $xls_path.'/'.$new_file_name);
            $upload->addValidator('Extension', false, array('xls'));
            if ($upload->isValid()) {
                $upload->receive();
                $mapper = new Application_Model_GameMapper();
                $adapter = $mapper->get_gateway()->getAdapter();
                $adapter->beginTransaction();
                try {
                    $result = Application_Model_Game::process_xls(
                        $xls_path.'/'.$new_file_name);
                    $adapter->commit();
                    $this->flashMessenger->addMessage(sprintf(
                         '<div class="alert alert-success">
                             <strong>Импорт завершен</strong>
                             <ul>
                                <li>%d зап. статистики нападающих</li>
                                <li>%d зап. статистики защитников</li>
                                <li>%d зап. статистики вратарей</li>
                                <li>%d зап. о голах</li>
                                <li>%d зап. об удалениях</li>
                             </ul>
                         </div>',
                         count($result['forward_stats']),
                         count($result['back_stats']),
                         count($result['goalie_stats']),
                         count($result['goal_stats']),
                         count($result['penalty_stats'])
                        )
                    );
                    $this->_redirect('/admin/dashboard');
                } catch (Application_Exception_EntityNotValid $e) {
                    $messages = array();
                    foreach ($e->messages as $array)
                        $messages[] = implode(' ', $array);
                    $this->flashMessenger->addMessage(sprintf(
                            '<div class="alert alert-error">
                            Неверные данные (<strong>%s</strong>)
                        </div>', implode(' ', $messages)));
                    $adapter->rollBack();
                } catch (Application_Exception_GameNotFound $e) {
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-error">'
                            .$e->getMessage().
                        '</div>');
                    $adapter->rollBack();
                } catch (Application_Exception_PlayerNotFound $e) {
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-error">'
                            .$e->getMessage().
                            '</div>');
                    $adapter->rollBack();
                }
            } else {
                $errors = $upload->getErrors();
                $this->flashMessenger->addMessage(sprintf(
                        '<div class="alert alert-error">
                        Ошибка при загрузке файла (%s)
                    </div>', implode(' ', $errors)));
            }
        }
    }

    public function manageUsersAction() {

        $this->view->users = Application_Model_User::fetch_all();
    }

    public function updateUserAction() {

        $this->view->headLink()
            ->appendStylesheet('/js/chosen/chosen/chosen.css');
        $this->view->headScript()
            ->appendFile('/js/chosen/chosen/chosen.jquery.js')
            ->appendFile('/js/form_common.js')
        ;
        $id = $this->_getParam('id');
        $this->view->altered_user = $id ? Application_Model_User::fetch($id) :
                                          Application_Model_User::dummy();
        $this->view->categories = Application_Model_Category::fetch_all();
        if ($this->_request->isPost()) {
            $data = $this->_request->getPost();
            try {
                if ($id) {
                    $updated_user =
                        Application_Model_User::update($data, $id);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Пользователь #'.$updated_user->id.' обновлен</div>');
                    $this->_redirect('/admin/manage-users');
                } else {
                    $new = Application_Model_User::register($data);
                    $this->flashMessenger->addMessage(
                        '<div class="alert alert-success">
                        Пользователь '.$new->id.' зарегистрирован</div>');
                    $this->_redirect('/admin/manage-users');
                }
            } catch (Whyte_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                        Форма заполнена неверно!</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
            }
        }
    }

    public function deleteUserAction() {

        $email = $this->_getParam('email');

        try {
            Application_Model_User::delete($email);
            $this->flashMessenger->addMessage(
                '<div class="alert alert-warning">Пользователь удален</div>');
        } catch (Exception $e) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">Ошибка удаления пользователя</div>');
        }
        $this->_redirect('/admin/manage-users');
    }
}
