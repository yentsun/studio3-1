<?php

class Application_Model_User extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_UserMapper';

    protected $_properties = array(
        'id'=> array('Int', array('GreaterThan', 0)),
        'email'=> array('EmailAddress', 'presence'=>'required'),
        'password_hash'=> array('Hex', array('StringLength', 40, 40),
                                'presence'=> 'required'),
        'first_name'=> array(),
        'last_name'=> array(),
        'phone'=> array(array('StringLength', 7, 18), 'presence'=>'required'),
        'locale'=> array(),
        'registration_date'=> array(array('Date', 'YYYY-MM-dd')),
        'club'=> array('allowEmpty'=> true),
        'job_title'=> array('allowEmpty'=> true),
        'agency'=> array('allowEmpty'=> true),
        'end_date'=> array(array('Date', 'YYYY-MM-dd')),
        'sport_id'=> 'Int',
        'status_id'=> 'Int' // 0 - inactive; 1 - normal; 2 - admin
    );

    private static $_status_map = array(
        '0'=> 'inactive',
        '1'=> 'client',
        '2'=> 'administrator'
    );

    public function get_status() {

        return self::$_status_map[$this->status_id];
    }

    public static function get_status_map() {

        return self::$_status_map;
    }

    /**
     * Register a new user and return either errors array or a valid user
     *
     * @static
     * @param array $applicant_data
     * @return mixed
     * @throws Application_Exception_EntityNotValid
     */
    public static function register(array $applicant_data) {

        $applicant = new Application_Model_Applicant($applicant_data);
        if ($applicant->has_errors()) {
            $exception = new Whyte_Exception_EntityNotValid(
                'Форма заполнена неверно!');
            $exception->messages = $applicant->_errors;
            $exception->original_data = $applicant_data;
            throw $exception;
        }

        //set properties that make a user
        $new_user = new self($applicant->to_array());
        $password = self::generate_password();
        $new_user->password_hash = self::digest_password($password);
        $new_user->status_id = 1;
        $now = Zend_Date::now();
        $new_user->registration_date = $now->toString(Zend_Date::ISO_8601);
        //TODO add a value from settings, not just one month
        $new_user->end_date = $now->addMonth(1)->toString(Zend_Date::ISO_8601);

        $mapper = new Application_Model_UserMapper();
        $user_id = $mapper->add($new_user);
        $user = self::fetch($user_id);
        Application_Model_Mailer::send_welcome_message($user, $password);
        Application_Model_Mailer::send_new_user_notification($user);
        return $user;
    }

    /**
     * Generate a readable password
     * @static
     * @return string
     */
    public static function generate_password() {

        require_once 'Text_Password-1.1.1/Password.php';
        $tp = new Text_Password();
        return $tp->create();
    }

    /**
     * Return hash for a password
     * @static
     * @param $password
     * @return string
     */
    public static function digest_password($password) {

        return sha1($password);
    }

    /**
     * Generate new password and attach it to user
     * @static
     * @param $user_id
     */
    public static function set_new_password($user_id) {

        $new_password = self::generate_password();
        //TODO send new password
        self::update(array('password_hash'=>
                           self::digest_password($new_password)),
                     $user_id);
    }

    /**
     * Try to authenticate with data array and return true if successful
     * @static
     * @param array $data
     * @return bool
     */
    public static function login(array $data) {

        if ($data['email'] && $data['password']) {
            $auth_adapter = self::_get_auth_adapter($data);
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($auth_adapter);
            if ($result->isValid()) {
                $max_session_time = 3600 * 24 * 365;
                Zend_Session::rememberMe($max_session_time);
                $user = Application_Model_User::get_auth_user();
                Application_Model_LoginRecord::close_expired($user->id, $max_session_time);
                Application_Model_LoginRecord
                    ::create(array('id'=>Zend_Session::getId(),
                                   'user_id'=>$user->id,
                                   'ip'=>self::get_ip(),
                                   'start_datetime'=>null));
                $other_ip_logins = Application_Model_LoginRecord
                                ::fetch_all($user->id, true, self::get_ip());
                if (count($other_ip_logins) > 0) {
                    Application_Model_Mailer::send_ip_notification($user,
                                                                   $other_ip_logins);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Get auth adapter based on Zend_Auth_Adapter_DbTable
     * @static
     * @param $data
     * @return \Zend_Auth_Adapter_DbTable
     */
    private static function _get_auth_adapter(array $data) {

        $mapper = new Application_Model_UserMapper();
        $map = $mapper->get_map();
        $auth_adapter = new Zend_Auth_Adapter_DbTable(
            Zend_Db_Table::getDefaultAdapter());
        $auth_adapter
            ->setTableName($mapper->get_table_name())
            ->setIdentityColumn($map['email'])
            ->setCredentialColumn($map['password_hash'])
            ->setCredentialTreatment('SHA1(?)')
            ->setIdentity($data['email'])
            ->setCredential($data['password'])
        ;
        return $auth_adapter;
    }

    public static function get_auth_user() {

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
            return self::fetch($identity);
        } else
            return null;
    }

    public static function delete($email) {

        $mapper = new Application_Model_UserMapper();
        $mapper->delete('email', $email);
    }

    public static function fetch($id_or_email) {

        $mapper = new Application_Model_UserMapper();
        if ($row=$mapper->get('id', (int) $id_or_email))
            return new self($row);
        elseif ($row=$mapper->get('email', $id_or_email))
            return new self($row);
        return null;
    }

    public static function logout() {

        Application_Model_LoginRecord::close(Zend_Session::getId());
        Zend_Session::forgetMe();
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
    }

    public static function get_ip() {

        $ip = null;
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else
            $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }

    public function fetch_IPs() {

        $records = Application_Model_LoginRecord::fetch_all($this->id);
        $unique_IP_records = array();
        foreach ($records as $record) {
                $unique_IP_records[$record->ip] = $record;
        }
        return $unique_IP_records;
    }

    public function fetch_open_IPs() {

        $records = Application_Model_LoginRecord::fetch_all($this->id, true);
        $unique_IPs = array();
        foreach ($records as $record) {
            if (!in_array($record->ip, $unique_IPs))
                $unique_IPs[] = $record->ip;
        }
        return $unique_IPs;
    }
}