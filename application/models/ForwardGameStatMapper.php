<?php

class Application_Model_ForwardGameStatMapper
                               extends Application_Model_PlayerGameStatMapper {

    protected $_table_name = 'stat_player_1';

    protected $_map = array(
        'id'=> 'id',
        'player_id'=> 'playerid',
        'game_id'=> 'parentid',
        'games_played'=> 'value_29__i',
        'goals_authored'=> 'value_30__i',
        'assists'=> 'value_31__i',
        'points'=> 'value_32__i',
        'plus_minus'=> 'value_33__i',
        'penalty_time'=> 'value_34__i',
        'score_equal'=> 'value_35__i',
        'score_grater'=> 'value_36__i',
        'score_less'=> 'value_37__i',
        'score_overtime'=> 'value_38__i',
        'score_win'=> 'value_39__i',
        'score_win_bullets'=> 'value_40__i',
        'attempts'=> 'value_41__i',
        'performance'=> 'value_42__f',
        'face_offs'=> 'value_43__i',
        'face_offs_won'=> 'value_44__i',
        'face_off_performance'=> 'value_45__f',
        'field_time'=> 'value_46__t',
        'shifts'=> 'value_47__i'
    );
}