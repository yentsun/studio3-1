<?php

class GroupTest extends PHPUnit_Framework_TestCase {

    private $_mapper = null;
    private $_adapter = null;
    const GROUP1_ID = 20;

    function setUp() {

        $this->_mapper = new Application_Model_GroupMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();
    }

    function tearDown() {

        $this->_adapter->rollBack();
    }

    public function test_create_group() {

        $group_data = array(
            'title'=> 'Дивизион F',
            'parent_id'=> '1'
        );
        $new_group_id = Application_Model_Group::create($group_data);
        $this->assertInternalType('int', $new_group_id);
        $new_group = Application_Model_Group::fetch($new_group_id);
        $this->assertEquals('Дивизион F', $new_group->title);
        $this->assertEquals(1, $new_group->parent_id);
    }

    public function test_update_group() {

        $data = array(
            'title'=> 'Дивизион G',
            'rubbish_prop'=> '345gf' //should be ignored
        );
        $updated_group = Application_Model_Group::update($data,
                                                          self::GROUP1_ID);
        $this->assertInstanceOf('Application_Model_Group', $updated_group);
        $this->assertEquals('Дивизион G', $updated_group->title);
    }

    public function test_delete_group() {

        Application_Model_Group::delete(self::GROUP1_ID);
        $deleted_group = Application_Model_Group::fetch(self::GROUP1_ID);
        $this->assertNull($deleted_group);
    }
}