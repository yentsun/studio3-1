<?php

class Application_Model_Applicant extends Application_Model_User {

    protected function _pre_data_population() {

        $mapper = new Application_Model_UserMapper;
        $user_properties = $this->_properties;
        $this->_properties = array_merge($user_properties, array(
            'email'=> array('EmailAddress',
                             new Zend_Validate_Db_NoRecordExists(
                                 $mapper->get_table_name(),
                                 $mapper->get_map_value('email')),
                             'presence' => 'required'),
            'id'=> array('allowEmpty'=>true)
        ));
        unset($this->_properties['password_hash']);
        unset($this->_properties['status_id']);
        unset($this->_properties['registration_date']);
        unset($this->_properties['end_date']);
    }
}

