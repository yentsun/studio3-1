<?php

class PlayerTest extends PHPUnit_Framework_TestCase {

    private $_mapper = null;
    private $_adapter = null;
    const PLAYER_ID = 5450;

    function setUp() {

        $this->_mapper = new Application_Model_PlayerMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();
    }

    function tearDown() {

        $this->_adapter->rollBack();
    }

    public function test_create_player() {

        $player_data = array(
            'name'=> 'БЕРДНИКОВ Роман',
            'datetime'=> '18.07.1992',
            'height'=> '182',
            'weight'=> '85',
            'hand'=> '2',
            'country_id'=> '1',
            'line'=> '1',
            'photo'=> 'berd.jpg'
        );
        $new_player_id = Application_Model_Player::create($player_data);
        $this->assertInternalType('int', $new_player_id);
        $new_player = Application_Model_Player::fetch($new_player_id);
        $this->assertEquals('БЕРДНИКОВ Роман', $new_player->name);
        $this->assertEquals(182, $new_player->height);
    }

    public function test_update_player() {

        $data = array(
            'height'=> '188',
            'weight'=> '87',
            'rubbish_prop'=> '345gf' //should be ignored
        );
        $updated_player = Application_Model_Player::update($data,
                                                          self::PLAYER_ID);
        $this->assertInstanceOf('Application_Model_Player', $updated_player);
        $this->assertEquals(188, $updated_player->height);
        $this->assertEquals(87, $updated_player->weight);
    }

    public function test_delete_player() {

        Application_Model_Player::delete(self::PLAYER_ID);
        $deleted_player = Application_Model_Player::fetch(self::PLAYER_ID);
        $this->assertNull($deleted_player);
    }

    public function test_count_players() {

        $this->assertEquals(1, Application_Model_Player
                                    ::count_all(CategoryTest::HOCKEY_ID));
    }

    public function test_fetch_all_players() {

        $players = Application_Model_Player::fetch_all(1);
        foreach ($players as $player) {
            $this->assertInstanceOf('Application_Model_Player', $player);
            $this->assertInstanceOf('Application_Model_Category',
                                    $player->country());
            $this->assertNotNull($player->line_title);
            if ($player->id == self::PLAYER_ID) {
                $this->assertEquals('Россия', $player->country()->title);
                $this->assertEquals('Омский Авангард', $player->team()->title);
            }
        }
    }

    public function test_process_xls() {

        $result = Application_Model_Player::process_xls(
            dirname(__FILE__).'/../../data/Players-u18-1.xls',
            CategoryTest::HOCKEY_ID);
        $this->assertEquals(213, count($result));
    }
}