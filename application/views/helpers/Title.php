<?php

class Application_View_Helper_Title extends Zend_View_Helper_Abstract {

    public $view;

    public function setView(Zend_View_Interface $view) {

        $this->view = $view;
    }

    /**
     * Set $string as the title placeholder value and return it
     * @param $string
     * @return mixed
     */
    public function title($string) {

        $this->view->placeholder('title')->set($string);
        return $this->view->placeholder('title');
    }
}