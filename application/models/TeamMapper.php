<?php

class Application_Model_TeamMapper extends Application_Model_Mapper {

    protected $_table_name = 'team';
    protected $_team_cat_table_name = 'team_main';

    protected $_map = array(
        'id'=> 'id',
        'title'=> 'ml_title_1',
        'title_en'=> 'ml_title_2',
        'logo'=> 'pic',
        'sport_id'=> 'parentid',
        'country_id'=> 'morecountryid',
        'datetime'=> 'date',
        'city'=> 'town',
        'arena_title'=> 'ml_arena_1',
        'arena_capacity'=> 'mesta',
        'coach_name'=> 'ml_trener_1',
        'email'=> 'ml_mail_1',
        'website_url'=> 'ml_site_1',
        'type'=> 'type',
        'iihf_rating'=> 'iihf_rating'
    );

    public function get($property, $value) {

        $select = $this->_select_joined_with_category();
        $properties = is_array($property) ? $property : (array) $property;
        $first_prop = array_shift($properties);
        $select
            ->where($this->_table_name.'.'.$this->_map[$first_prop].' = ?',
                    $value);
        foreach ($properties as $prop)
            $select
                ->orWhere($this->_table_name.'.'.$this->_map[$prop].' = ?',
                          $value);
        $row = $this->_gateway->fetchRow($select);
        if ($row)
            return $this->row_to_array($row, array('category_id'=>'category_id'));
        else
            return null;
    }

    public function fetch_all($sport_id=null, $offset=0, $limit=0,
                               array $ids=null, $user_id=null) {

        $select = $this->_select_joined_with_category()
            ->limit($limit, $offset)
        ;
        if ($sport_id)
            $select = $select->where(
                $this->_table_name.'.'.$this->_map['sport_id'].'= ?',
                $sport_id);
        if ($ids)
            $select = $select->where(
                $this->_table_name.'.'.$this->_map['id'].' IN (?)',
                $ids);
        if ($user_id) {
            $fav_mapper = new Application_Model_FavouriteMapper();
            $select
                ->join(array('fav'=>$fav_mapper->get_table_name()),
                      $this->_table_name.'.'.$this->_map['id'].' = '
                     .'fav.'.$fav_mapper->get_map_value('object_id'),
                       array('id'=>$this->_table_name.'.'.$this->_map['id'],
                             'fav_id'=>'fav.'.$fav_mapper->get_map_value('id')))
                ->where('fav.'.$fav_mapper->get_map_value('user_id').' = ?',
                        $user_id)
                ->where('fav.'.$fav_mapper->get_map_value('type').' = ?',
                         2)
            ;
        }
        return $this->_gateway->fetchAll($select);
    }

    public function fetch_all_by_game_category(array $cat_ids, $offset=0,
                                               $limit=0) {

        $game_mapper = new Application_Model_GameMapper();
        $select = $this->_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_table_name)
            ->join(array('game'=>'match'),
                   $this->_table_name.'.'.$this->_map['id'].' = game.'
                       .$game_mapper->get_map_value('team_one_id').' OR '
                       .$this->_table_name.'.'.$this->_map['id'].' = game.'
                       .$game_mapper->get_map_value('team_two_id'), array())
            ->where('game.'.$game_mapper->get_map_value('category_id')
                   .' IN (?) ', $cat_ids)
            ->order($this->_map['title'])
            ->limit($limit, $offset)
            ;
        return $this->_gateway->fetchAll($select);
    }

    private function _select_joined_with_category() {

        return $this->_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_table_name)
            ->joinLeft(array('tm'=>$this->_team_cat_table_name),
                   $this->_table_name.'.id = tm.teamid')
            ->columns(array('id'=>$this->_table_name.'.id'))
            ->columns(array('category_id'=>'tm.parentid'))
            ->columns(array('parentid'=>'team.parentid'))
            ->joinLeft(array('c'=>'country'),
                      'team.'.$this->_map['country_id'].' = c.id',
                       array('country_title'=>'c.ml_title_1',
                             'country_image_file'=>'c.pic'))
            ->order($this->_table_name.'.'.$this->_map['title'].' ASC')
            ;
    }

    public function fetch_teams_league_id($team_id) {

        $select = $this->_select_joined_with_category()
            ->where('tm.teamid = ?', $team_id);
        $row = $this->_gateway->fetchRow($select);
        if ($row)
            return $row->league_id;
        else
            return NULL;
    }

    public function fetch_all_category_records($category_id) {

        $team_cat_gateway = new Zend_Db_Table($this->_team_cat_table_name);
        $select = $team_cat_gateway->select()
            ->where('parentid = ?', $category_id);
        return $team_cat_gateway->fetchAll($select);
    }

    public function insert_category_record($team_id, $league_id) {

        $team_cat_gateway = new Zend_Db_Table($this->_team_cat_table_name);
        $team_cat_gateway->insert(array('parentid'=>$league_id,
                                        'teamid'=>$team_id));
    }

    public function delete_category_records($league_id) {

        $team_cat_gateway = new Zend_Db_Table($this->_team_cat_table_name);
        $where = $team_cat_gateway
            ->getAdapter()
            ->quoteInto('parentid = ?', $league_id);
        $team_cat_gateway->delete($where);
    }
}