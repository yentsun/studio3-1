<?php

class PageTest extends PHPUnit_Framework_TestCase {

    private $_mapper = NULL;
    private $_adapter = NULL;
    const PAGE_ID = 'benefits';

    function setUp() {

        $this->_mapper = new Application_Model_PageMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();
    }

    function tearDown() {

        $this->_adapter->rollBack();
    }

    public function test_create_page() {

        $data = array(
            'slug'=> 'benefits2',
            'title'=> 'The Benefits You Get',
            'body'=> 'You will get many benefits while working with us'
        );
        $new_page_id = Application_Model_Page::create($data);
        $this->assertInternalType('int', $new_page_id);
        $new_page = Application_Model_Page::fetch('benefits2');
        $this->assertEquals('The Benefits You Get', $new_page->title);
        $this->assertEquals('You will get many benefits while working with us',
                            $new_page->body);
    }

    public function test_update_page() {

        $data = array(
            'title'=> "The Benefits You Don't Get",
            'rubbish_prop'=> '345gf' //should be ignored
        );
        $updated_page = Application_Model_Page::update($data, self::PAGE_ID);
        $this->assertInstanceOf('Application_Model_Page', $updated_page);
        $this->assertEquals("The Benefits You Don't Get",
                            $updated_page->title);
    }

    public function test_delete_page() {

        Application_Model_Page::delete(self::PAGE_ID);
        $this->assertNull(Application_Model_Page::fetch(self::PAGE_ID));
    }
}