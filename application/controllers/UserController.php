<?PHP

class UserController extends Zend_Controller_Action {

    public function init() {

        $this->flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->view->headScript()
            ->appendFile('/js/form_common.js');
    }

    public function indexAction() {
        // action body
    }

    public function registerAction() {

        $this->view->required = Application_Model_User::get_required();
        if ($this->_request->isPost() && $data=$this->_request->getPost()) {
            try {
                Application_Model_User::register($data);
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-success">
                        Вы успешно зарегистрировались и авторизовались!</div>');
                $this->_redirect('/user/register-success');
            } catch (Whyte_Exception_EntityNotValid $e) {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">'.$e->getMessage().'</div>');
                $this->view->errors = $e->messages;
                $this->view->original_data = $e->original_data;
//                $this->_redirect('/user/register');
            }
        }
    }

    public function registerSuccessAction() {
        // action body
    }

    public function loginAction() {

        if ($this->_request->isPost()) {
            $login = Application_Model_User::login($this->_request->getPost());
            if ($login) {
                $this->flashMessenger->addMessage('ok');
                $this->_redirect('/');
            } else {
                $this->flashMessenger->addMessage(
                    '<div class="alert alert-error">
                     Авторизация не удалась!</div>');
            }
        }
    }

    public function logoutAction() {

        Application_Model_User::logout();
        $this->flashMessenger->addMessage(sprintf(
                '<div class="alert alert-success">%s!</div>',
                $this->view->translate('Всего хорошего, ждем вас снова')));
        $this->_redirect('/');
    }

    public function recoverAction() {

        if ($this->_request->isPost()) {
            $login = Application_Model_User::login($this->_request->getPost());
            $this->flashMessenger->addMessage(
                '<div class="alert alert-error">
                     Авторизация не удалась!</div>');
        }
    }

    public function dashboardAction() {

        $this->view->countries = Application_Model_Category::fetch_all(1);
    }

    public function favouritesAction() {

        $user = Application_Model_User::get_auth_user();
        $this->view->teams = Application_Model_Favourite
                                 ::fetch_all(2, $user->id);
        $this->view->games = Application_Model_Favourite
                                 ::fetch_all(3, $user->id);
        $this->view->players = Application_Model_Favourite
                                   ::fetch_all(1, $user->id);
    }

    public function addFavouriteAction() {

        $object_id = $this->_getParam('id');
        $type = $this->_getParam('type');
        $user = Application_Model_User::get_auth_user();
        $back_url = $url = $this->getRequest()->getServer('HTTP_REFERER');
        try {
            Application_Model_Favourite::create(
                array('object_id'=> $object_id,
                      'type'=> $type,
                    'user_id'=> $user->id));
        }
        catch (Exception $e) {}
        $this->flashMessenger->addMessage(
               '<div class="alert alert-success">'.
                     $this->view->translate('Элемент добавлен в избранное')
               .'</div>');
        $this->_redirect($back_url);
    }

    public function deleteFavouriteAction() {

        $id = $this->_getParam('id');
        $user = Application_Model_User::get_auth_user();
        if (Application_Model_Favourite::delete($id, $user->id)) {
            $this->flashMessenger->addMessage(
                '<div class="alert alert-success">
                     Элемент удален из избранного</div>');
            $this->_redirect('/user/favourites');
        }
    }
}