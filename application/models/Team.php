<?php

class Application_Model_Team extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_TeamMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>True),
        'title'=> array(),
        'title_en'=> array(),
        'logo'=> array('allowEmpty'=>True),
        'sport_id'=> array('Int', 'allowEmpty'=>True),
        'country_id'=> array('Int', 'allowEmpty'=>True),
        'category_id'=> array('Int', 'allowEmpty'=>True),
        'datetime'=> array(array('Date', 'yyyy-MM-dd'),
                           'allowEmpty'=>True),
        'city'=> array('allowEmpty'=>True),
        'arena_title'=> array('allowEmpty'=>true),
        'arena_capacity'=> array('allowEmpty'=>true),
        'coach_name'=> array('allowEmpty'=>True),
        'email'=> array(array('EmailAddress', array('domain'=>false)),
                        'allowEmpty'=>True),
        'website_url'=> array('allowEmpty'=>True),
        'type'=> array(),
        'iihf_rating'=> array('Int', 'allowEmpty'=>True),
        'row'=> array('allowEmpty'=>True)
    );

    protected $_filters = array(
        'number'=> 'Int',
    );

    protected function _pre_data_population() {

        $this->_filters = array_merge($this->_filters,
                                      array('datetime'=> new Application_Filter_YearToISO()));
    }

    public static function fetch($id=null, $title=null) {

        $mapper = new Application_Model_TeamMapper();
        if ($id) {
            $property_name = 'id';
            $value = $id;
        } elseif ($title) {
            $property_name = array('title', 'title_en');
            $value = $title;
        } else
            throw new Exception('Нужно указать id или title');
        $data_array = $mapper->get($property_name, $value);
        if ($data_array) {
            $team = new self($data_array);
            return $team;
        }
        else
            return null;
    }

    public static function fetch_all_by_category_id($category_id) {

        $mapper = new Application_Model_TeamMapper();
        $rows = $mapper->fetch_all_category_records($category_id);
        $result_set = array();
        foreach ($rows as $row) {
            $result_set[] = $row->teamid;
        }
        return $result_set;
    }

    public static function update_category_records(array $team_ids,
                                                         $category_id) {

        $mapper = new Application_Model_TeamMapper();
        $mapper->delete_category_records($category_id);
        foreach ($team_ids as $team_id) {
            $mapper->insert_category_record($team_id, $category_id);
        }
    }

    public function category() {

        if (!isset($this->_fetched['category'])) {
            if (isset($this->row->category_title)) {
                $this->_fetched['category'] = new Application_Model_Category(
                    array(
                         'id'=> $this->row->category_id,
                         'title'=> $this->row->category_title,
                         'image_file'=> $this->row->category_image_file
                    ));
            } elseif ($this->category_id) {
                $this->_fetched['category'] = Application_Model_Category
                ::fetch($this->category_id);
            } else {
                 $this->_fetched['category'] = null;
            }
        }
        return $this->_fetched['category'];
    }

    public function country() {

        //TODO rewrite with joins
        $league = $this->category();
        if (!isset($this->_fetched['country']))
            $this->_fetched['country'] = Application_Model_Category
                                            ::fetch($league->parent_id);
        return $this->_fetched['country'];
    }

    public function fetch_games($category_id=null, $limit=null) {

        return Application_Model_Game::fetch_all($this->id, $category_id,
                                                 null, null, null, $limit);
    }

    public function fetch_players() {

        return Application_Model_Player::fetch_all_by_team_id($this->id);
    }

    public static function fetch_all($sport_id=null, $page=1, array $ids=null,
                                     $user_id=null) {

        $mapper = new Application_Model_TeamMapper();

        $per_page = null;
        $offset = null;

        if ($page) {
            $per_page = Zend_Registry::get('settings')->pagination->per_page;
            $offset = $per_page * ($page - 1);
            $total_count = self::count_all($sport_id);

            //pagination
            $paginator = Zend_Paginator::factory((int) $total_count);
            $paginator->setPageRange(
                Zend_Registry::get('settings')->pagination->range);
            Zend_Paginator::setDefaultScrollingStyle('Elastic');
            Zend_View_Helper_PaginationControl::setDefaultViewPartial(
                'admin/_pagination.phtml');
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($per_page);
            self::$paginator = $paginator;
        }

        $rows = $mapper->fetch_all($sport_id, $offset, $per_page, $ids, $user_id);
        $result_set = array();
        foreach ($rows as $row) {
            $team = new self($mapper->row_to_array($row, array('category_id'=>'category_id')));
            $team->row = $row;
            $result_set[$team->id] = $team;
        }
        return $result_set;
    }

    public static function count_all($sport_id) {

        $mapper = new Application_Model_TeamMapper();
        return (int) $mapper->count_all($mapper->get_map_value('sport_id').'= ?',
                                        $sport_id);
    }

    public function to_search_doc($id=null) {

        $id = $id ? $id : $this->id;
        $doc = new Zend_Search_Lucene_Document();
        $doc->addField(Zend_Search_Lucene_Field::Keyword('id', $id));
        $doc->addField(Zend_Search_Lucene_Field
                                       ::text('title', $this->title, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::unIndexed('logo',
                                                           $this->logo));
        $cat_title =  $this->category() ? $this->category()->title : '';
        $doc->addField(Zend_Search_Lucene_Field::text('category_title',
                                             $cat_title, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::text('arena_title',
                                             $this->arena_title, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::text('city',
                                                      $this->city, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::text('coach_name',
                                                   $this->coach_name, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::Keyword('datetime', $this->datetime(null, null, null, false)));

        return $doc;
    }

    public function count_videos() {

        $game_mapper = new Application_Model_GameMapper();
        return $game_mapper->fetch_all($this->id, null, null, null, null,
                                        null, true, true);

    }
}