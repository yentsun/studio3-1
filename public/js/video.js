var SECONDS = 'data-seconds';
var type_map = {0: 'inverse',
                1: 'warning',
                3: 'primary'};

$(function(){
    var video = _V_('main_video');
    var on_play = function(){
        spawn_markers($('.vjs-progress-control'), this);
    };
    video.ready(function(){
        this.addEvent('loadedmetadata', on_play);
    });
});

function spawn_markers(cont, video) {
    var duration = video.duration();
    var markers_cont = $('<div id="markers" />');
    markers_cont.appendTo(cont);
    $('#marks .item').each(function(){
        var seconds = $(this).attr(SECONDS);
        $(this).bind('mouseenter mouseleave', function(){
            $('.vjs-controls').toggleClass('vjs-fade-out')
                              .toggleClass('vjs-fade-in');
            $('#m'+seconds).toggleClass('above');
            $('#m'+seconds+' .popup').toggle();
        });
        var type = $(this).attr('data-type');
        var percent = (seconds / duration * 100);
        var popup = $('<div class="popup btn btn-mini btn-'+type_map[type]+'" />');
        popup.html($(this).html());
        var marker = $('<a id="m'+seconds+'" data-seconds="'+seconds+'"' +
                            'onclick="play_from(this)" ' +
                            'style="left:'+percent+'%" ' +
                            'class="marker '+type_map[type]+'">' +
                       '</a>');
        marker.bind('mouseenter mouseleave', function() {
            $(this).find('.popup').toggle();
        });
        popup.appendTo(marker);
        marker.appendTo(markers_cont);
    });

}

function play_from(cont) {

    var seconds = $(cont).attr(SECONDS);
    _V_('main_video').ready(function(){
        this.pause();
        this.currentTime(seconds);
        this.play();
    });
}