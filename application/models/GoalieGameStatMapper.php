<?php

class Application_Model_GoalieGameStatMapper
    extends Application_Model_PlayerGameStatMapper {

    protected $_table_name = 'stat_player_3';

    protected $_map = array(
        'id'=> 'id',
        'player_id'=> 'playerid',
        'game_id'=> 'parentid',
        'games_played'=> 'value_15__i',
        'goals_authored'=> 'value_24__i',
        'games_won'=> 'value_16__i',
        'games_lost'=> 'value_17__i',
        'bullet_games'=> 'value_18__i',
        'assists'=> 'value_25__i',
        'penalty_time'=> 'value_27__i',
        'attempts'=> 'value_19__i',
        'field_time'=> 'value_28__t',
        'score_missed'=> 'value_20__i',
        'saves'=> 'value_21__i',
        'saving_performance'=> 'value_22__f',
        'saving_ratio'=> 'value_23__f',
        'dry_games'=> 'value_26__i'
    );
}