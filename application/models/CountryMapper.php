<?php

class Application_Model_CountryMapper extends Application_Model_Mapper {

    protected $_table_name = 'country';

    protected $_map = array(
        'id'=> 'id',
        'title'=> 'ml_title_1',
        'image_file'=> 'pic',
        'order_id'=> 'orderid'
    );

    public function fetch_all() {

        $select = $this->_gateway
            ->select()
            ->order($this->_map['order_id']);
        return $this->_gateway->fetchAll($select);
    }
}