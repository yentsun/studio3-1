<?php

class Application_Plugin_Common extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        $layout = Zend_Layout::getMvcInstance();
        $view = $layout->getView();
        $front = Zend_Controller_Front::getInstance();

        //cache manager
        $cache_manager = $front
            ->getParam('bootstrap')
            ->getResource('cachemanager');
        Zend_Registry::set('cache_manager', $cache_manager);

        //flash messages
        $flash_messenger = Zend_Controller_Action_HelperBroker
                            ::getStaticHelper('FlashMessenger');
        $view->messenger = $flash_messenger;

        // set locale if required
        $visit = new Zend_Session_Namespace('visit');
        if (isset($visit->locale)) {
            if (Zend_Locale::isLocale($visit->locale)) {
                $locale = new Zend_Locale($visit->locale);
                Zend_Registry::set(
                    Zend_Application_Resource_Locale::DEFAULT_REGISTRY_KEY,
                    $locale
                );
                Zend_Registry::get('Zend_Translate')->setLocale($locale);
            }
        }
    }
}