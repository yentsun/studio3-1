<?php

class PlayerController extends Zend_Controller_Action {

    public function init() {

        $id = $this->_getParam('id');
        $this->view->player = Application_Model_Player::fetch($id);
    }

    public function indexAction() {

        $this->view->headScript()
            ->appendFile('/js/jquery.dataTables.min.js')
            ->appendFile('/js/team-index.js')
        ;
        $this->view->transfers = $this->view->player->fetch_transfers();
        $this->view->games = $this->view->player->fetch_games();
        $team = $this->view->player->fetch_current_team();
        $this->view->team = $team ? $team : Application_Model_Team::dummy();
    }

    public function videoAction() {

        $this->view->transfers = $this->view->player->fetch_transfers();
        $this->view->games = $this->view->player->fetch_games(true);
        $this->view->team = $this->view->player->fetch_current_team();
    }

    public function streamPromoAction() {

        $format = $this->_getParam('format', 'mp4');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $file_info = $this->view->player->get_promo_file_info();
        $this->getResponse()
                ->setHeader('Content-type', 'video/'.$format)
                ->setHeader('X-Accel-Redirect',
                            '/media'.$file_info['directory_path'].'/'
                            .$file_info['name'].'.'.$format)
                ->setHeader('Content-length',
                             abs(filesize(APPLICATION_PATH.'/../data/media'
                            .$file_info['directory_path'].'/'
                            .$file_info['name'].'.'.$format)))
        ;
    }
}