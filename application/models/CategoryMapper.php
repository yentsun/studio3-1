<?php

class Application_Model_CategoryMapper extends Application_Model_Mapper {

    protected $_table_name = 'main';

    protected $_map = array(
        'id'=> 'id',
        'title'=> 'ml_title_1',
        'image_file'=> 'pic',
        'parent_id'=> 'parentid',
        'order_id'=> 'orderid',
        'status_id'=> 'item_end'
    );

    public function fetch_all(array $filters=null) {

        $select = $this->_gateway
            ->select()
            ->order($this->_map['order_id'])
        ;
        if ($filters) {
            foreach ($filters as $prop_name=>$value)
                    $select = $select->where(
                        $this->_map[$prop_name].' = ?', $value);
        }
        return $this->_gateway->fetchAll($select);
    }
}