<?php

abstract class Application_Model_Entity extends Whyte_Model_Entity {

    /**
     * If entity has 'datetime' property, return Zend_Date->toString() in given
     * format
     * @param string $format
     * @return null|string
     */
    public function datetime($format=Zend_Date::DATE_MEDIUM,
                             $property_name='datetime',
                             $input_format=null, $fix_mode=true) {

        if (isset($this->_properties[$property_name]))
            if ($this->_properties[$property_name]) {
                if ($this->$property_name != '0000-00-00') {
                    //fix needed for Zend_Date - strtotime first
                    $time_string = $fix_mode ?
                        strtotime($this->$property_name) :
                        $this->$property_name;
                    $date = new Zend_Date($time_string, $input_format);
                    return $date->toString($format);
                }
            }
        return null;
    }

    /**
     * Clear 'functions' cache with entity class name as tag
     * @static
     * @param $class_name
     */
    protected static function _clear_cache($class_name) {

        $cache_manager = Zend_Registry::get('cache_manager');
        $cache = $cache_manager->getCache('functions');
        $cache->clean(
            Zend_Cache::CLEANING_MODE_MATCHING_TAG,
            array($class_name)
        );
    }

    /**
     * If entity has 'to_search_doc' method, perform addition to search index
     * @param null $id
     */
    protected function _add_to_search_index($id=null) {

        if (method_exists($this, 'to_search_doc')) {
            $search = new Application_Model_Search(get_called_class());
            $search->save_document($this->to_search_doc($id));
        }
    }

    protected function _remove_from_search_index($id) {

        $search = new Application_Model_Search(get_called_class());
        $search->delete($id);
    }

    public static function search($query) {

        $search = new Application_Model_Search(get_called_class());
        return $search->perform_search($query);
    }
}