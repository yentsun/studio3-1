<?php

class Application_Model_Mailer {

    /**
     * Send the message and deal with error if any
     * @static
     * @param       $template_file
     * @param array $data
     * @param       $to
     * @param       $subject
     */
    private static function _send($template_file, array $data=array(),
                                  $to, $subject) {

        $layout = new Zend_Layout();
        $layout->setLayout('email');
        $layout->setLayoutPath(APPLICATION_PATH.'/layouts/scripts/');
        $template = new Zend_View();
        $template->setScriptPath(APPLICATION_PATH.'/views/scripts/email/');
        $config = Zend_Registry::get('settings');
        $template->assign('host', $config->webhost);
        foreach ($data as $name=>$value)
            $template->assign($name, $value);
        $layout->host = $config->webhost;
        $layout->content = $template->render($template_file);
        $body = $layout->render();

        $mail = new Zend_Mail('utf-8');
//        try {
            $mail
                ->setBodyHtml($body)
                ->addTo($to)
                ->setSubject($subject)
                ->send();
//        } catch (Zend_Mail_Exception $e) {
//            //TODO somehow process the error
//        }
    }

    public static function send_welcome_message(Application_Model_User $user,
                                                $password) {
        $data = array(
            'user'=> $user,
            'password'=> $password
        );
        self::_send('welcome.phtml', $data, $user->email,
                    'добро пожаловать');
    }

    public static function send_new_user_notification(
        Application_Model_User $user) {

        $settings = Zend_Registry::get('settings');
        $data = array(
            'user'=> $user
        );
        self::_send('new-user-notification.phtml', $data,
                    $settings->email->admin,
                    'зарегистрирован новый пользователь');
    }

    public static function send_ip_notification(
        Application_Model_User $user, array $sessions) {

        $settings = Zend_Registry::get('settings');
        $data = array(
            'user'=> $user,
            'sessions'=> $sessions
        );
        self::_send('ip-notification.phtml', $data,
                    $settings->email->admin,
                    'пользователь зашел с разных IP-адресов');
    }
}

