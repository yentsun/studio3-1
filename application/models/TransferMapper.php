<?php

class Application_Model_TransferMapper extends Application_Model_Mapper {

    protected $_table_name = 'player_team';

    protected $_map = array(
        'id'=> 'id',
        'player_id'=> 'parentid',
        'team_id'=> 'teamid',
        'number'=> 'number',
        'datetime'=> 'date',
        'datetime_end'=> 'date_end',
    );

    public function fetch_all($player_id, $current=false) {

        $team_mapper = new Application_Model_TeamMapper();
        $team_map = $team_mapper->get_map();

        $select = $this->_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_table_name)
            ->order($this->_map['datetime'].' DESC')
            ->join(array('t'=>'team'), $this->_table_name.'.teamid = t.id',
                   array('team_title'=>'t.'.$team_map['title'],
                         'team_logo'=>'t.'.$team_map['logo'],
                         'team_type'=>'t.'.$team_map['type']))
        ;
        $select = $select
            ->where($this->_table_name.'.'
                   .$this->_map['player_id'].' = ?', $player_id);
        if ($current)
            $select
                ->where($this->_table_name.'.'
                       .$this->_map['datetime_end'].' = "0000-00-00"');
        return $this->_gateway->fetchAll($select);
    }
}