<?php

class Application_Model_PlayerGameStat extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_PlayerGameStatMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>true),
        'player_id'=> array('Int'),
        'game_id'=> array('Int'),
        'games_played'=> array('Int', 'allowEmpty'=>true),
        'games_won'=> array('Int', 'allowEmpty'=>true),
        'games_lost'=> array('Int', 'allowEmpty'=>true),
        'goals_authored'=> array('Int', 'allowEmpty'=>true),
        'assists'=> array('Int', 'allowEmpty'=>true),
        'points'=> array('Int', 'allowEmpty'=>true),
        'plus_minus'=> array('Int', 'allowEmpty'=>true),
        'penalty_time'=> array('Int', 'allowEmpty'=>true),
        'score_equal'=> array('Int', 'allowEmpty'=>true),
        'score_grater'=> array('Int', 'allowEmpty'=>true),
        'score_less'=> array('Int', 'allowEmpty'=>true),
        'score_overtime'=> array('Int', 'allowEmpty'=>true),
        'score_win'=> array('Int', 'allowEmpty'=>true),
        'score_win_bullets'=> array('Int', 'allowEmpty'=>true),
        'attempts'=> array('Int', 'allowEmpty'=>true),
        'performance'=> array(array('Float', 'locale'=>'en'),
                              'allowEmpty'=>true),
        'face_offs'=> array('Int', 'allowEmpty'=>true),
        'face_offs_won'=> array('Int', 'allowEmpty'=>true),
        'face_off_performance'=> array(array('Float', 'locale'=>'en'),
                                       'allowEmpty'=>true),
        'field_time'=> array(array('Date', 'HH:mm:ss')),
        'shifts'=> array('Int', 'allowEmpty'=>true),
        'bullet_games'=> array('Int', 'allowEmpty'=>true),
        'score_missed'=> array('Int', 'allowEmpty'=>true),
        'saves'=> array('Int', 'allowEmpty'=>true),
        'saving_performance'=> array(array('Float', 'locale'=>'en'),
                                     'allowEmpty'=>true),
        'saving_ratio'=> array(array('Float', 'locale'=>'en'),
                               'allowEmpty'=>true),
        'dry_games'=> array('Int', 'allowEmpty'=>true),
        'row'=> array('allowEmpty'=>true)
    );

    protected function _pre_data_population() {

        $this->_filters = array_merge(
            $this->_filters,
            array('field_time'=> new Application_Filter_FixTime()));
    }

    public static function fetch($id, $player_line) {

        $mapper = self::get_mapper($player_line);
        $data_array = $mapper->get('id', (int) $id);
        if ($data_array)
            return new self($data_array);
        else
            return null;
    }

    public static function get_mapper($player_line) {

        if (in_array($player_line, array('forward', 1)))
            return new Application_Model_ForwardGameStatMapper();
        if (in_array($player_line, array('back', 2)))
            return new Application_Model_BackGameStatMapper();
        if (in_array($player_line, array('goalie', 3)))
            return new Application_Model_GoalieGameStatMapper();
        return null;
    }

    public static function fetch_all($game_id, $player_line, $player_id=null) {

        $mapper = self::get_mapper($player_line);
        $rows = $mapper->fetch_all($game_id, $player_id);
        $result_set = array();
        foreach ($rows as $row) {
            $stat = new self($mapper->row_to_array($row));
            $stat->row = $row;
            $result_set[$stat->id] = $stat;
        }
        return $result_set;
    }

    public static function update(array $data, $id, $player_line) {

        $stat = self::fetch($id, $player_line);
        foreach ($data as $name=>$value) {
            if (array_key_exists($name, $stat->_validators) && $name != 'id')
                $stat->$name = $value;
        }
        if ($stat->has_errors()) {
            $exception = new Application_Exception_EntityNotValid(
                'Неверные данные!');
            $exception->messages = $stat->_errors;
            $exception->original_data = $data;
            throw $exception;
        } else {
            $mapper = self::get_mapper($player_line);
            $mapper->update($stat, 'id');
            return self::fetch($stat->id, $player_line);
        }
    }

    public static function create(array $data, $player_line) {

        $stat = new self($data);
        if ($stat->has_errors()){
            $array_of_errors = array();
            foreach ($stat->_errors as $key=>$array)
                $array_of_errors[] = implode(' ', $array);
            $exception = new Application_Exception_EntityNotValid(sprintf(
                'Неверные данные (%s)', implode(' ', $array_of_errors)));
            $exception->messages = $stat->_errors;
            $exception->original_data = $data;
            throw $exception;
        }
        $mapper = self::get_mapper($player_line);
        $new_id = $mapper->add($stat);
        return $new_id;
    }

    public static function delete($id, $player_line) {

        if ($id) {
            $mapper = self::get_mapper($player_line);
            $rows = $mapper->delete('id', $id);
            if (!$rows)
                throw new Zend_Exception('Now rows were affected');
        } else
            throw new Zend_Exception('Id must be set');
    }
}