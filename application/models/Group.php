<?php

class Application_Model_Group extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_GroupMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>true),
        'title'=> array(),
        'parent_id'=> 'Int'
    );
}