<?php

class Application_Model_LoginRecord extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_LoginRecordMapper';

    protected $_properties = array(
        'id'=> array('allowEmpty'=>true),
        'ip'=> array(),
        'user_id'=> 'Int',
        'start_datetime'=> array(array('Date', 'YYYY-MM-dd H:i:s')),
        'end_datetime'=> array(array('Date', 'YYYY-MM-dd H:i:s'),
                               'allowEmpty'=>true)
    );

    /**
     * Fetch login records by user id
     * @param null $user_id
     * @param bool $open
     * @param null $ip
     * @return array
     */
    public static function fetch_all($user_id, $open=false, $ip=null) {

        $mapper = new Application_Model_LoginRecordMapper();
        $filters = array('user_id'=>$user_id);
        if ($open)
            $filters['end_datetime'] = null;
        if ($ip)
            $filters_not = array('ip'=>$ip);
        else
            $filters_not = array();
        $rows = $mapper->fetch_all(null, $filters, $filters_not,
                                  'start_datetime');
        $records = array();
        foreach ($rows as $row)
            $records[] = new self($mapper->row_to_array($row));
        return $records;
    }

    public static function close($id) {

        $record = self::fetch($id);
        $now = new DateTime();
        $record->end_datetime = $now->format('Y-m-d H:i:s');
        $mapper = new Application_Model_LoginRecordMapper();
        if ($record instanceof Whyte_Model_Entity)
            $mapper->update($record, 'id');
    }

    public static function close_expired($user_id, $max_time) {

        $now = new DateTime();
        $interval = new DateInterval(sprintf('PT%dS', $max_time));
        $records = self::fetch_all($user_id, true);
        $mapper = new Application_Model_LoginRecordMapper();
        foreach ($records as $record) {
            $start = new DateTime($record->start_datetime);
            $expiry_time = $start->add($interval);
            if ($now > $expiry_time) {
                $record->end_datetime = $expiry_time;
                $mapper->update($record, 'id');
            }
        }
    }
}