<?php

class SearchController extends Zend_Controller_Action
{

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {

        if ($this->_request->isPost()) {
            $query_data = $this->_request->getPost();
            $query = $query_data['query'];
            $this->view->query = $query;
            $this->view->games = Application_Model_Game::search($query);
            $this->view->teams = Application_Model_Team::search($query);
            $this->view->players = Application_Model_Player::search($query);
        }
    }
}