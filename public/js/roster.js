$(function(){
});

function filter_players(position, cont) {
    var item = $('.items .item');
    $(cont).addClass('selected').parent('li')
           .siblings().find('a').removeClass('selected');
    if (position == 'all') {
        item.show();
    } else {
        item.hide();
        $('.items .item[data-pos-id="'+position+'"]').show();
    }
}