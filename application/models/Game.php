<?php

class Application_Model_Game extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_GameMapper';
    protected $_marks = null;
    const DATETIME_FORMAT = 'dd.MM.YYYY HH:mm';
    const GAME_START = '00:00:00';
    const PERIOD2_START = '00:20:00';
    const PERIOD3_START = '00:40:00';
    const OT_START = '01:00:00';
    const BULLET_TIME = '01:05:00';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>true),
        'datetime'=> array(array('Date', Zend_Date::ISO_8601)),
        'arena_title'=> array('allowEmpty'=>true),
        'team_one_id'=> array('Int'),
        'team_two_id'=> array('Int'),
        'category_id'=> array('Int'),
        'team_one'=> array('allowEmpty'=>true),
        'team_two'=> array('allowEmpty'=>true),
        'video_path'=> array('allowEmpty'=>true),
        'video_hd'=> array('allowEmpty'=>true),
        'review_path'=> array('allowEmpty'=>true),
        'number'=> array(),
        'game_score'=> array('allowEmpty'=>true),
        'protocol_html'=> array('allowEmpty'=>true),
        'scoreboard_html'=> array('allowEmpty'=>true),
        'video_time'=> array(array('Date', Zend_Date::ISO_8601),
                            'allowEmpty'=>true),
        'row'=> array('allowEmpty'=>true)
    );

    protected function _pre_data_population() {

        $team_mapper = new Application_Model_TeamMapper();
        $team_exist_validator = new Zend_Validate_Db_RecordExists(
            $team_mapper->get_table_name(),
            $team_mapper->get_map_value('id'));
        $this->_properties['team_one_id'] = array_merge(
            $this->_properties['team_one_id'], array($team_exist_validator));
        $this->_properties['team_two_id'] = array_merge(
            $this->_properties['team_two_id'], array($team_exist_validator));
    }

    protected $_filters = array(
        'number'=> 'Int',
        'datetime'=> array(
            array('LocalizedToNormalized',
                  array('date_format'=> self::DATETIME_FORMAT)),
            array('NormalizedToLocalized',
                  array('date_format'=> Zend_Date::ISO_8601))
        )
    );

    public static function check_existence($team_one_id, $team_two_id, $date) {

        $mapper = new Application_Model_GameMapper();
        $date = new Zend_Date($date, 'dd.MM.YYYY');
        $row = $mapper->fetch_by_composite_key((int) $team_one_id,
                                               (int) $team_two_id,
                                               $date->toString('YYYY-MM-dd'));

        return $row ? new self($mapper->row_to_array($row)) : false;
    }

    public static function fetch_all($team_id=null, $category_id=null,
                                     $page=null, $player_id=null,
                                     $line_id=null, $limit=null, $user_id=null,
                                     $video_only=false) {

        $per_page = null;
        $offset = null;

        //get all children of the category
        if ($category_id)
            $cat_ids =
                Application_Model_Category::get_children_flat($category_id);
        else
            $cat_ids = array();

        if ($page) {
            $per_page = Zend_Registry::get('settings')->pagination->per_page;
            $offset = $per_page * ($page - 1);
            $total_count = self::count_all($cat_ids);

            //pagination
            $paginator = Zend_Paginator::factory((int) $total_count);
            $paginator->setPageRange(
                Zend_Registry::get('settings')->pagination->range);
            Zend_Paginator::setDefaultScrollingStyle('Elastic');
            Zend_View_Helper_PaginationControl::setDefaultViewPartial(
                'admin/_pagination.phtml');
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($per_page);
            self::$paginator = $paginator;
        }
        $limit = $per_page ? $per_page : $limit;
        $mapper = new Application_Model_GameMapper();
        $rows = $mapper->fetch_all($team_id, $cat_ids, $offset, $limit,
                                   $player_id, $line_id, $video_only, false,
                                   $user_id);
        $result_set = array();
        foreach ($rows as $row) {
            $game = new self($mapper->row_to_array($row));
            $game->row = $row;
            $result_set[$game->id] = $game;
        }

        return $result_set;
    }

    public static function count_all(array $category_ids) {

        $mapper = new Application_Model_GameMapper();
        return $mapper->count_all(
            $mapper->get_map_value('category_id').' IN (?)',
            $category_ids);
    }

    public function category() {

        if (!isset($this->_fetched['category'])) {
            if (isset($this->row->category_id)) {
                $this->_fetched['category'] = new Application_Model_Category(
                    array(
                         'id'=> $this->row->category_id,
                         'title'=> $this->row->category_title,
                         'parent_id'=> $this->row->category_parent_id
                    ));
            } else {
                $this->_fetched['category'] = Application_Model_Category
                                                ::fetch($this->category_id);
            }
        }
        return $this->_fetched['category'];
    }

    public function team_one() {

        if (!isset($this->_fetched['team_one'])) {
            if (isset($this->row->team_one_id)) {
                $this->_fetched['team_one'] = new Application_Model_Team(
                    array(
                         'id'=> $this->row->team_one_id,
                         'title'=> $this->row->team_one_title
                    ));
            } else {
                $this->_fetched['team_one'] = Application_Model_Team
                                                ::fetch($this->team_one_id);
            }
        }
        return $this->_fetched['team_one'];
    }

    public function team_two() {

        if (!isset($this->_fetched['team_two'])) {
            if (isset($this->row->team_two_id)) {
                $this->_fetched['team_two'] = new Application_Model_Team(
                    array(
                         'id'=> $this->row->team_two_id,
                         'title'=> $this->row->team_two_title
                    ));
            } else {
                $this->_fetched['team_two'] = Application_Model_Team
                                                ::fetch($this->team_two_id);
            }
        }
        return $this->_fetched['team_two'];
    }

    public function fetch_players($team_id=null, $order_by=null, $dir=null) {

        if (!isset($this->_fetched['players_'.$team_id])) {
            $player_mapper = new Application_Model_PlayerMapper();
            $rows = $player_mapper->fetch_all(null, 0, 0, $this->id, $team_id,
                                             $order_by, $dir, false, null,
                                            !$team_id);
            $result_set = array();
            foreach ($rows as $row) {
                $player = new Application_Model_Player(
                    $player_mapper->row_to_array($row));
                $player->row = $row;
                $result_set[$player->id] = $player;
            }
            $this->_fetched['players'.$team_id] = $result_set;
        }
        return $this->_fetched['players'.$team_id];
    }

    /**
     * Return one of the game's team by player id
     * @param $player_id
     * @return null | Application_Model_Team
     */
    public function get_team($player_id) {

        $team_one = $this->team_one();
        $team_two = $this->team_two();
        if (in_array($player_id,
                      array_keys($this->fetch_players($team_one->id))))
            return $team_one;
        elseif (in_array($player_id,
                          array_keys($this->fetch_players($team_two->id))))
            return $team_two;
        return null;
    }

    public function winner() {

        $score_pieces = explode(':', $this->game_score);
        if (count($score_pieces) == 2) {
            $team_one_score = (int) $score_pieces[0];
            $team_two_score = (int) $score_pieces[1];
            if ($team_one_score > $team_two_score)
                return $this->team_one();
            if ($team_two_score > $team_one_score)
                return $this->team_two();
        }
        return null;
    }

    public function marks() {

        if (!$this->_marks)
            $this->_marks = Application_Model_GameMark::fetch_all($this->id);
        return $this->_marks;
    }

    public function has_overtime() {

        return strstr($this->game_score, 'OT');
    }

    public function has_bullets() {

        return strstr($this->game_score, 'P');
    }

    public function goals() {

        $period2_start = new DateTime(self::PERIOD2_START);
        $period3_start = new DateTime(self::PERIOD3_START);
        $OT_start = new DateTime(self::OT_START);
        $bullet_time = new DateTime(self::BULLET_TIME);
        $result = array(1=>array(1=>0, 2=>0, 3=>0, 4=>0, 5=>0),
                        2=>array(1=>0, 2=>0, 3=>0, 4=>0, 5=>0));
        $marks = $this->marks();
        foreach ($marks as $mark) {

            //count goals
            if ($mark->type == 3) {
                $time = new DateTime($mark->time_protocol);
                if ($time  < $period2_start) {
                    if ($mark->team_id == $this->team_one()->id)
                        $result[1][1]++;
                    else
                        $result[2][1]++;
                }
                if ($time >= $period2_start && $time < $period3_start) {
                    if ($mark->team_id == $this->team_one()->id)
                        $result[1][2]++;
                    else
                        $result[2][2]++;
                }
                if ($time >= $period3_start && $time < $OT_start) {
                    if ($mark->team_id == $this->team_one()->id)
                        $result[1][3]++;
                    else
                        $result[2][3]++;
                }
                if ($time >= $OT_start && $time < $bullet_time) {
                    if ($mark->team_id == $this->team_one()->id)
                        $result[1][4]++;
                    else
                        $result[2][4]++;
                }
                if ($time == $bullet_time) {
                    if ($mark->team_id == $this->team_one()->id)
                        $result[1][5]++;
                    else
                        $result[2][5]++;
                }
            }
        }
        return $result;
    }

    public function fetch_stats($player_line) {

        return Application_Model_PlayerGameStat
            ::fetch_all($this->id, $player_line);
    }

    public function update_roster(array $player_ids) {

        $mapper = new Application_Model_PlayerMapper();
        $mapper->delete_game_records($this->id);
        foreach ($player_ids as $player_id)
            $mapper->insert_game_record($player_id, $this->id);
    }

    public static function process_csv($file_path, $category_id) {

        $CSV_DELIMITER = ',';
        $result_set = array();
        $ignored = array();

        if (($handle = self::fopen_utf8($file_path)) !== false) {
            while (($string = fgets($handle, 1000)) !== false) {
                $row = explode($CSV_DELIMITER, $string);
                foreach ($row as $key=>$val) $row[$key] = trim($val);
                $data = array();
                list(
                    $data['number'],
                    $data['time'],
                    $data['date'],
                    $data['team_one_title'],
                    $data['team_two_title'],
                    $data['game_score']
                ) = $row;

                //normalize/add data
                $pieces = explode(':', $data['time']);
                $data['datetime'] = sprintf('%s %s:%s ', $data['date'],
                                            $pieces[0], $pieces[1]);
                $data['category_id'] = $category_id;

                //fetch teams
                try {
                    $team_one = Application_Model_Team::fetch(null,
                                                      $data['team_one_title']);
                    $team_two = Application_Model_Team::fetch(null,
                                                      $data['team_two_title']);
                } catch (Exception $e) {

                }
                if (!$team_one)
                    throw new Application_Exception_TeamNotFound(
                        sprintf('Ряд №%d, команда "%s"',
                                $data['number'],
                                $data['team_one_title']));
                if (!$team_two)
                    throw new Application_Exception_TeamNotFound(
                        sprintf('Ряд №%d, команда "%s"',
                                $data['number'],
                                $data['team_two_title']));
                $data['team_one_id'] = $team_one->id;
                $data['team_two_id'] = $team_two->id;
                $data['arena_title'] = $team_one->arena_title;

                $existent_game = self::check_existence($data['team_one_id'],
                                                  $data['team_two_id'],
                                                  $data['date']);

                if (!$existent_game) {
                    $new_game_id = self::create($data);
                    if (is_int($new_game_id))
                        $result_set[$new_game_id] = self::fetch($new_game_id);
                } else {
                    $ignored[$existent_game->id] = $existent_game;
                }
            }
            fclose($handle);
        }
        return array('added'=>$result_set, 'ignored'=>$ignored);
    }

    public function to_search_doc($id=null) {

        $id = $id ? $id : $this->id;
        $doc = new Zend_Search_Lucene_Document();
        $doc->addField(Zend_Search_Lucene_Field::Keyword('id', $id));
        $doc->addField(Zend_Search_Lucene_Field::text(
                'category_title',
                implode(', ', $this->category()->parents(true, 2)).', '
               .$this->category()->title, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::text(
                'team_one_title',
                $this->team_one()->title, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::text(
                'team_two_title',
                $this->team_two()->title, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::Keyword(
                'number',
                $this->number));
        $doc->addField(Zend_Search_Lucene_Field::text(
                'game_score',
                $this->game_score, 'UTF-8'));
        $doc->addField(Zend_Search_Lucene_Field::text(
                'datetime',
                $this->datetime(self::DATETIME_FORMAT, null, null, false),
                'UTF-8'));
        return $doc;
    }

    public static function process_xls($file_path) {

        require_once('Excel/reader.php');
        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('utf-8');
        $data->read($file_path);

        $result = array();

        // get game data
        $date = trim($data->sheets[0]['cells'][1][1]);
        $team_one_title = trim($data->sheets[0]['cells'][2][1]);
        $team_two_title = trim($data->sheets[0]['cells'][2][2]);
        $team_one = Application_Model_Team::fetch(null, $team_one_title);
        $team_two = Application_Model_Team::fetch(null, $team_two_title);
        if (!$team_one)
            throw new Application_Exception_TeamNotFound($team_one_title);
        if (!$team_two)
            throw new Application_Exception_TeamNotFound($team_two_title);

        $game = self::check_existence($team_one->id, $team_two->id, $date);
        if (!$game)
            throw new Application_Exception_GameNotFound(
                sprintf('Игра "%s" - "%s", %s не найдена',  $team_one->title,
                        $team_two->title, $date));

        //remove title rows
        foreach ($data->sheets as $no=>$sheet)
            unset($data->sheets[$no]['cells'][1]);

        // process goals
        foreach ($data->sheets[1]['cells'] as $no=>$row) {
            $pieces = explode(' ', $row[6]);
            $player_name = implode(' ', array($pieces[1], $pieces[2]));
            $player = Application_Model_Player::fetch($player_name);
            if (!$player)
                throw new Application_Exception_PlayerNotFound(
                    sprintf('Игрок "%s" не найден',  $player_name));
            $goal_mark_data = array(
                'game_id'=> $game->id,
                'team_id'=> $game->get_team($player->id)->id,
                'time_video'=> null,
                'time_protocol'=>$row[3],
                'type'=> 3,
                'title'=> $row[4],
                'description_line1'=> $row[6],
                'description_line2'=> null,
                'player_id'=> $player->id
            );
            $result['goal_stats'][] = $goal_mark_data;
            Application_Model_GameMark::create($goal_mark_data);
        }

        // process penalties
        unset($data->sheets[3]['cells'][2]);
        foreach ($data->sheets[3]['cells'] as $no=>$row) {
            $penalty_mark_data = null;
            if (self::_is_time_cell($row[6])) {
                $pieces = explode(' ', $row[7]);
                $player_name = implode(' ', array($pieces[1], $pieces[2]));
                $player = Application_Model_Player::fetch($player_name);
                if (!$player)
                    throw new Application_Exception_PlayerNotFound(
                        sprintf('Игрок "%s" не найден', $player_name));
                $penalty_mark_data = array(
                    'game_id'=> $game->id,
                    'team_id'=> $game->get_team($player->id)->id,
                    'time_video'=> null,
                    'time_protocol'=>$row[6],
                    'type'=> 1,
                    'title'=> $row[8],
                    'description_line1'=> $row[9],
                    'description_line2'=> null,
                    'player_id'=> $player->id
                );
            }
            if (self::_is_time_cell($row[1])) {
                $pieces = explode(' ', $row[2]);
                $player_name = implode(' ', array($pieces[1], $pieces[2]));
                $player = Application_Model_Player::fetch($player_name);
                if (!$player)
                    throw new Application_Exception_PlayerNotFound(
                        sprintf('Игрок "%s" не найден', $player_name));
                $penalty_mark_data = array(
                    'game_id'=> $game->id,
                    'team_id'=> $game->get_team($player->id)->id,
                    'time_video'=> null,
                    'time_protocol'=>$row[1],
                    'type'=> 1,
                    'title'=> $row[3],
                    'description_line1'=> $row[4],
                    'description_line2'=> null,
                    'player_id'=> $player->id
                );
            }
            if ($penalty_mark_data) {
                $result['penalty_stats'][] = $penalty_mark_data;
                Application_Model_GameMark::create($penalty_mark_data);
            }
        }

        // process goalies stats
        $goalie_rows = array_merge($data->sheets[4]['cells'],
                                   $data->sheets[7]['cells']);
        foreach ($goalie_rows as $no=>$row) {
                $row = self::_filter_null($row);
                $player_name = $row[2];
                $player = Application_Model_Player::fetch($player_name);
                if (!$player)
                    throw new Application_Exception_PlayerNotFound(
                        sprintf('Игрок "%s" не найден',  $player_name));
                $goalie_stat_data = array(
                    'player_id'=> $player->id,
                    'game_id'=> $game->id,
                    'games_played'=> $row[3],
                    'games_won'=> $row[4],
                    'games_lost'=> $row[5],
                    'bullet_games'=> $row[6],
                    'assists'=> $row[13],
                    'penalty_time'=> $row[15],
                    'attempts'=> $row[7],
                    'field_time'=> $row[16],
                    'score_missed'=> $row[8],
                    'saves'=> $row[9],
                    'saving_performance'=> $row[10],
                    'saving_ratio'=> $row[11],
                    'goals_authored'=> $row[12],
                    'dry_games'=> $row[14]
                );
                $result['goalie_stats'][] = $goalie_stat_data;
                Application_Model_PlayerGameStat::create($goalie_stat_data,
                                                         'goalie');
        }

        // process backs stats
        foreach (array_merge($data->sheets[5]['cells'],
                             $data->sheets[8]['cells'])
            as $no=>$row) {
                $row = self::_filter_null($row);
                $player_name = $row[2];
                $player = Application_Model_Player::fetch($player_name);
                if (!$player)
                    throw new Application_Exception_PlayerNotFound(
                        sprintf('Игрок "%s" не найден',  $player_name));
                $back_stat_data = array(
                    'player_id'=> $player->id,
                    'game_id'=> $game->id,
                    'games_played'=> $row[3],
                    'goals_authored'=> $row[4],
                    'assists'=> $row[5],
                    'points'=> $row[6],
                    'plus_minus'=> $row[7],
                    'penalty_time'=> $row[8],
                    'score_equal'=> $row[9],
                    'score_grater'=> $row[10],
                    'score_less'=> $row[11],
                    'score_overtime'=> $row[12],
                    'score_win'=> $row[13],
                    'score_win_bullets'=> $row[14],
                    'attempts'=> $row[15],
                    'performance'=> $row[16],
                    'face_offs'=> $row[17],
                    'face_offs_won'=> $row[18],
                    'face_off_performance'=> $row[19],
                    'field_time'=> $row[20],
                    'shifts'=> $row[21]
                );
                $result['back_stats'][] = $back_stat_data;
                Application_Model_PlayerGameStat::create($back_stat_data,
                                                         'back');
        }

        // process forwards stats
        foreach (array_merge($data->sheets[6]['cells'],
                             $data->sheets[9]['cells'])
            as $no=>$row) {
                $row = self::_filter_null($row);
                $player_name = $row[2];
                $player = Application_Model_Player::fetch($player_name);
                if (!$player)
                    throw new Application_Exception_PlayerNotFound(
                        sprintf('Игрок "%s" не найден',  $player_name));
                $forward_stat_data = array(
                    'player_id'=> $player->id,
                    'game_id'=> $game->id,
                    'games_played'=> $row[3],
                    'goals_authored'=> $row[4],
                    'assists'=> $row[5],
                    'points'=> $row[6],
                    'plus_minus'=> $row[7],
                    'penalty_time'=> $row[8],
                    'score_equal'=> $row[9],
                    'score_grater'=> $row[10],
                    'score_less'=> $row[11],
                    'score_overtime'=> $row[12],
                    'score_win'=> $row[13],
                    'score_win_bullets'=> $row[14],
                    'attempts'=> $row[15],
                    'performance'=> $row[16],
                    'face_offs'=> $row[17],
                    'face_offs_won'=> $row[18],
                    'face_off_performance'=> $row[19],
                    'field_time'=> $row[20],
                    'shifts'=> $row[21]
                );
                $result['forward_stats'][] = $forward_stat_data;
                Application_Model_PlayerGameStat::create($forward_stat_data,
                                                         'forward');
        }
        return $result;
    }

    /**
     * Convert stream to utf-8.
     * Taken from
     * http://www.practicalweb.co.uk/blog/08/05/18/reading-unicode-excel-file-php
     * @static
     * @param $filename
     * @return resource
     */
    public static function fopen_utf8($filename){

        $encoding = null;
        $handle = fopen($filename, 'r');
        $bom = fread($handle, 2);
        rewind($handle);
        if ($bom === chr(0xff).chr(0xfe) || $bom === chr(0xfe).chr(0xff)) {
            // UTF16 Byte Order Mark present
            $encoding = 'UTF-16';
        } else {
            $file_sample = fread($handle, 1000).'e'; //read first 1000 bytes
            // + e is a workaround for mb_string bug
            rewind($handle);
            $encoding = mb_detect_encoding($file_sample , 'UTF-8, UTF-16');
        }
        if ($encoding){
            stream_filter_append($handle, 'convert.iconv.'.$encoding.'/UTF-8');
        }
        return $handle;
    }

    private static function _filter_null(array $row) {

        foreach ($row as $no=>$cell) {
            if ($cell == '-')
                $row[$no] = null;
        }
        return $row;
    }

    private static function _is_time_cell($cell_contents) {

        return preg_match('/^([0-9]{2}:[0-9]{2})/', $cell_contents);
    }

    public function get_videofile_info() {

        $result = array();
        $path_pieces = explode('/', $this->video_path);
        $file_pieces = explode('.', array_pop($path_pieces));
        $result['directory_path'] = implode('/', $path_pieces);
        $result['ext'] = array_pop($file_pieces);
        $result['name'] = implode('.', $file_pieces);
        return $result;
    }

    public function get_videofile_path() {

        return APPLICATION_PATH.'/../data/media'.$this->video_path;
    }

    public function has_webm_copy() {

        $info = $this->get_videofile_info();
        return file_exists(APPLICATION_PATH.'/../data/media/'.$info['directory_path'].'/'.$info['name'].'.webm');
    }
}