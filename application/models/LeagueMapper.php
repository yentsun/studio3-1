<?php

class Application_Model_LeagueMapper extends Application_Model_Mapper {

    protected $_table_name = 'main';

    protected $_map = array(
        'id'=> 'id',
        'title'=> 'ml_title_1',
        'logo'=> 'pic',
        'country_id'=> 'parentid',
        'order_id'=> 'orderid'
    );

    public function fetch_all(array $filters) {

        $select = $this->_gateway
            ->select()
            ->setIntegrityCheck(false)
            ->order($this->_map['order_id'])
        ;
        foreach ($filters as $prop_name=>$value)
            $select = $select->where(
                $this->_map[$prop_name].' = ?', $value);
        return $this->_gateway->fetchAll($select);
    }
}

