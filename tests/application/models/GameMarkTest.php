<?php

class GameMarkTest extends PHPUnit_Framework_TestCase {

    private $_mapper = null;
    private $_adapter = null;
    const MARK_ID = 13616;

    function setUp() {

        $this->_mapper = new Application_Model_GameMarkMapper();
        $this->_adapter = $this->_mapper->get_gateway()->getAdapter();
        $this->_adapter->beginTransaction();
    }

    function tearDown() {

        $this->_adapter->rollBack();
    }

    public function test_create_gamemark() {

        $gamemark_data = array(
            'game_id'=> '24114',
            'team_id'=> '22',
            'time'=> '00:45:00',
            'type'=> '3',
            'title'=> '0:1',
            'player_id'=> '3375'
        );
        $new_gamemark_id = Application_Model_GameMark::create($gamemark_data);
        $this->assertInternalType('int', $new_gamemark_id);
        $new_gamemark = Application_Model_GameMark::fetch($new_gamemark_id);
        $this->assertEquals('00:45:00', $new_gamemark->time);
        $this->assertEquals('0:1', $new_gamemark->title);
    }

    public function test_update_gamemark() {

        $data = array(
            'time'=> '00:47:00',
            'rubbish_prop'=> '345gf' //should be ignored
        );
        $updated_gamemark = Application_Model_GameMark::update($data,
                                                          self::MARK_ID);
        $this->assertInstanceOf('Application_Model_GameMark', $updated_gamemark);
        $this->assertEquals('00:47:00', $updated_gamemark->time);
    }

    public function test_delete_gamemark() {

        Application_Model_GameMark::delete(self::MARK_ID);
        $deleted_gamemark = Application_Model_GameMark::fetch(self::MARK_ID);
        $this->assertNull($deleted_gamemark);
    }
}