<?php

class Application_Model_Category extends Application_Model_Entity {

    protected $_mapper_class = 'Application_Model_CategoryMapper';

    protected $_properties = array(
        'id'=> array('Int', 'allowEmpty'=>True),
        'title'=> array(),
        'image_file'=> array('allowEmpty'=>True),
        'parent_id'=> 'Int',
        'order_id'=> array('Int', 'default'=>0),
        'status_id'=> array('Int', 'default'=>0),
        'videos_count'=> array('Int', 'allowEmpty'=>True)
    );

    private $_status_map = array(
        0=> 'in progress',
        1=> 'finished'
    );

    private $_type_map = array(
        0=> 'sport',
        1=> 'country',
        2=> 'league',
        3=> 'season',
        4=> 'stage',
    );

    public function get_status() {

        return $this->_status_map[$this->status_id];
    }

    public function get_type() {

        $depth = count($this->parents());
        return $this->_type_map[$depth];
    }

    public function get_parent_by_type($type) {

        foreach ($this->parents() as $parent) {
            if ($parent->get_type() == $type)
                return $parent;
        }
        return null;
    }

    public static function fetch_all($parent_id=null) {

        $mapper = new Application_Model_CategoryMapper();
        $criteria = $parent_id ? array('parent_id'=>$parent_id) : null;
        $rows = $mapper->fetch_all($criteria);
        $result_set = array();
        foreach ($rows as $row) {
            $category = new self($mapper->row_to_array($row));
            $result_set[$category->id] = $category;
        }
        return $result_set;
    }

    public function fetch_children() {

        return self::fetch_all($this->id);
    }

    public function parent() {

        return self::fetch($this->parent_id);
    }

    public function parents($flat=false, $offset=0) {

        $cache_manager = Zend_Registry::get('cache_manager');
        $cache = $cache_manager->getCache('functions');
        $flat_set = $cache->call(__CLASS__.'::fetch_all', array(0),
                                 array(__CLASS__, 'flat_set'));
        $parents = array();
        $cat = $this;
        while (isset($flat_set[$cat->parent_id])) {
            $parent = $flat_set[$cat->parent_id];
            $parents[] = $flat ? $parent->title : $parent;
            $cat = $flat_set[$parent->id];
        }
        $parents = array_slice(array_reverse($parents), $offset, null, true);
        return $parents;
    }

    /**
     * Hierarchy tree of 'category' entities
     * @static
     * @param $parent_id
     * @return array
     */
    public static function fetch_tree($parent_id=0) {

        $flat_set = Application_Model_Category::fetch_all();
        return self::_get_children($flat_set, $parent_id);
    }

    /**
     * Cached version of the expensive fetch_tree method
     * @static
     * @param int $parent_id
     * @return mixed
     */
    public static function get_tree($parent_id=0) {

        $cache_manager = Zend_Registry::get('cache_manager');
        $cache = $cache_manager->getCache('functions');
        return $cache->call(__CLASS__.'::fetch_tree', array($parent_id),
                            array(__CLASS__, 'tree'));
    }

    /**
     * Build hierarchy tree recursively based on flat set of categories
     * @static
     * @param $flat_set
     * @param $parent_id
     * @return array
     */
    private static function _get_children($flat_set, $parent_id, $ids_only=false) {

        $result = array();
        if (!empty($flat_set)) {
            foreach ($flat_set as $cat) {
                if ($cat->parent_id == $parent_id) {
                    unset($flat_set[$cat->id]);
                    if (!$ids_only) {
                        $result[$cat->id] = array(
                            'cat'=> $cat,
                            'type'=> $cat->get_type(),
                            'children'=> self::_get_children($flat_set, $cat->id)
                        );
                    } else {
                        $result = array_merge($result,
                                    self::_get_children($flat_set, $cat->id, true));
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get ids of all children if a given category recursively as a flat array
     * @static
     * @param $category_id
     * @return array
     */
    public static function get_children_flat($category_id) {

        $tree = self::get_tree($category_id);
        $flat_array = array($category_id=>null);
        array_walk_recursive($tree,
                      function($a) use (&$flat_array) {
                          if ($a instanceof Application_Model_Category)
                              $flat_array[$a->id] = $a;

                      });
        return array_keys($flat_array);
    }

    public function count_videos() {

        $game_mapper = new Application_Model_GameMapper();
        $categories = self::get_children_flat($this->id);
        return $game_mapper->fetch_all(null, $categories,
                                       null, null, null, null, true, true);

    }

    /**
     * Get the first (active) of all shown seasons of a league
     */
    public function get_first_season() {

        $seasons = self::fetch_all($this->id);
        return array_shift($seasons);
    }
}