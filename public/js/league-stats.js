$(function(){
    var forwards_cont = $('#forwards');
    var defenders_cont = $('#defenders');
    var goalies_cont = $('#goalies');
    var forwards = init_datatable(forwards_cont.find('table'), league_id, 1, null);
    var defenders = init_datatable(defenders_cont.find('table'), league_id, 2, null);
    var goalies = init_datatable(goalies_cont.find('table'), league_id, 3, null);

    forwards_cont.find('.seasons').clone().prependTo(defenders_cont);
    forwards_cont.find('.seasons').clone().prependTo(goalies_cont);
    $(forwards_cont.find('.seasons')).change(function() {
        forwards.fnDestroy();
        forwards = init_datatable(forwards_cont.find('table'), league_id,
                                  1, $(this).val());
    });
    $(defenders_cont.find('.seasons')).change(function() {
        defenders.fnDestroy();
        defenders = init_datatable(defenders_cont.find('table'), league_id,
                                  2, $(this).val());
    });
    $(goalies_cont.find('.seasons')).change(function() {
        goalies.fnDestroy();
        goalies = init_datatable(goalies_cont.find('table'), league_id,
                                  3, $(this).val());
    });
});

/* Default class modification */
$.extend( $.fn.dataTableExt.oStdClasses, {
    "sSortAsc": "header headerSortDown",
    "sSortDesc": "header headerSortUp",
    "sSortable": "header"
} );

/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings ) {
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
}

/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };

            $(nPaging).addClass('pagination').append(
                '<ul>'+
                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                    '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
        },

        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }
            else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }

            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // Remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                // Add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                        .insertBefore( $('li:last', an[i])[0] )
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                            fnDraw( oSettings );
                        } );
                }

                // Add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                    $('li:first', an[i]).addClass('disabled');
                } else {
                    $('li:first', an[i]).removeClass('disabled');
                }

                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                    $('li:last', an[i]).addClass('disabled');
                } else {
                    $('li:last', an[i]).removeClass('disabled');
                }
            }
        }
    }
} );

$.fn.dataTableExt.oApi.fnSortNeutral = function ( oSettings )
{
    /* Remove any current sorting */
    oSettings.aaSorting = [];

    /* Sort display arrays so we get them in numerical order */
    oSettings.aiDisplay.sort( function (x,y) {
        return x-y;
    } );
    oSettings.aiDisplayMaster.sort( function (x,y) {
        return x-y;
    } );

    /* Redraw */
    oSettings.oApi._fnReDraw( oSettings );
};

function select_from_unique(json, target) {
    /*Get column rows and make a select from their unique values*/
    var unique_elements = [];
    for (var key in json.aaData) {
        //strip img tag
        var text = $.trim(json.aaData[key][2]).replace(/(<([^>]+)>)/ig,"");
        if ($.inArray(text, unique_elements) === -1)
            unique_elements.push(text);
    }
    var club_string = $('table:eq(0)').find('td.n3').text();
    //refresh the table
    target.html('');
    target.append('<option>'+club_string+'</option>');
    $.each(unique_elements, function() {
        var option = $('<option />').attr('value', this.valueOf())
                                    .text(this.valueOf());
        target.append(option);
    });
}

function init_datatable(table, league_id, line_id, cat_id) {

    var cat_string = '';
    if (cat_id != null)
        cat_string = '/category_id/'+cat_id;

    return table.dataTable({
        "aoColumnDefs": [
            {"sClass": "left team", "aTargets":[2]},
            {"sClass": "left", "aTargets":[1]}
        ],
        "aaSorting": [],
        "sPaginationType": "bootstrap",
        "bProcessing": true,
        "sAjaxSource": '/league/ajax-stats/id/'+league_id+
                       '/line_id/'+line_id+cat_string,
        "oLanguage": {
            "sLengthMenu": table_lang['sLengthMenu'],
            "sZeroRecords": table_lang['sZeroRecords'],
            "sProcessing": table_lang['sProcessing'],
            "sSearch": table_lang['sSearch'],
            "oPaginate": {
                "sFirst":    table_lang['sFirst'],
                "sPrevious": table_lang['sPrevious'],
                "sNext":     table_lang['sNext'],
                "sLast":     table_lang['sLast']
            }
        },
        "fnInitComplete": function(oSettings, json) {
            var select = table.parents('.person1').find('select.teams');
            select_from_unique(json, select);
            select.change(function(){
                table.fnFilter(this.value, 2);
            });
        }
    })
}