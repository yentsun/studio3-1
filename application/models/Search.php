<?php

class Application_Model_Search {

    private $_index = null;
    const INITIAL_PATH = '/../data/search_index';

    public function __construct($index_name, $force_new=false) {

        Zend_Search_Lucene_Analysis_Analyzer::setDefault(new
          Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive);

        $this->_index_name = $index_name;
        if ($force_new)
            $this->delete_index();
        $this->_index = $this->_open_index($index_name);
    }

    private static function _index_path($name) {

        return APPLICATION_PATH
              .self::INITIAL_PATH.'/'
              .APPLICATION_ENV.'/'
              .$name;
    }

    private function _open_index($name) {

        $path = self::_index_path($name);
        $index = null;
        try {
            $index =  Zend_Search_Lucene::open($path);
        } catch (Zend_Search_Lucene_Exception $e) {
            $message = $e->getMessage();
            if ($message == "Index doesn't exists in the specified directory.") {
                $index = Zend_Search_Lucene::create($path);
            }
        }
        return $index;
    }

    public function delete_index() {

        $path = self::_index_path($this->_index_name);
        self::_destroy_dir($path);
    }

    public function get_index() {

        return $this->_index;
    }

    /**
     * Add or update document
     * @param Zend_Search_Lucene_Document $doc
     */
    public function save_document(Zend_Search_Lucene_Document $doc,
                                  $check_existence=true) {

        if ($check_existence) {
            $entity_id = $doc->getFieldValue('id');
            $this->delete($entity_id);
        }
        $this->_index->addDocument($doc);
    }

    public function fetch($id) {

        $query = sprintf('id:%d', $id);
        $hits = $this->_index->find($query);
        if (!empty($hits))
            return $hits[0];
        else
            return null;
    }

    public function delete($id) {

        $doc = $this->fetch($id);
        if ($doc) {
            $this->_index->delete($doc->id);
//            $this->_index->optimize();
        }
    }

    public function perform_search($query) {

        $words = explode(' ', $query);
        $patterns = array();
        foreach ($words as $word) {
                $patterns[] = $word;
        }
        Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding('UTF-8');
        $query = Zend_Search_Lucene_Search_QueryParser
                     ::parse(implode(' AND ', $patterns));
        return $this->_index->find($query);
    }

    /**
     * http://www.php.net/manual/ru/function.explode.php#104007
     * @static
     * @param array  $delimiters
     * @param string $string
     * @return array
     */
    public static function multi_explode($delimiters=array(), $string='') {

        $main_delim = $delimiters[count($delimiters)-1];
        array_pop($delimiters);
        foreach($delimiters as $delimiter) {
            $string= str_replace($delimiter, $main_delim, $string);
        }
        $result = explode($main_delim, $string);
        return $result;
    }

    /**
     * http://stackoverflow.com/a/1407382/216042
     * @static
     * @param $dir
     * @return bool
     */
    private static function _destroy_dir($dir) {

        $DS = '/';
        if (!is_dir($dir) || is_link($dir)) return unlink($dir);
        foreach (scandir($dir) as $file) {
            if ($file == '.' || $file == '..') continue;
            if (!self::_destroy_dir($dir.$DS.$file)) {
                chmod($dir.$DS.$file, 0777);
                if (!self::_destroy_dir($dir.$DS.$file)) return false;
            };
        }
        return rmdir($dir);
    }
}